<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    if(\Illuminate\Support\Facades\Auth::check())
    {
        if(\Illuminate\Support\Facades\Auth::user()->type==1)
        {
            return redirect(route('admin'));
        }else
        {
            return redirect(route('accounts'));
        }
    }
    return redirect(route('login'));

})->name('home');

Route::get('login','LoginController@login')->name('login');
Route::post('login','LoginController@authenticate')->name('login');
Route::post('logout','LoginController@logout')->name('logout');
Route::get('add-dso','DSOController@add')->name('add-dso')->middleware('auth','user_type');


Route::get('admin','AdminController@admin')->name('admin')->middleware('auth','user_type');
Route::post('admin-trx','AdminController@addNewPersonalAccounts')->name('admin-trx')->middleware('auth','user_type');
Route::get('admin-trx-diary','AdminController@getAllPersonalAccounts')->name('admin-trx-diary')->middleware('auth','user_type');
Route::get('admin-trx-delete/{id}','AdminController@deletePersonalTRX')->name('admin-trx-delete')->middleware('auth','user_type');
Route::get('admin-trx-edit/{id}','AdminController@editViewPersonalTRX')->name('admin-trx-edit')->middleware('auth','user_type');
Route::post('admin-trx-diary-update/{id}','AdminController@updatePersonalTRX')->name('admin-trx-diary-update')->middleware('auth','user_type');
Route::post('reset','AdminController@resetDB')->name('resetDB')->middleware('auth','user_type');
Route::get('reset-password','AdminController@resetPassword')->name('reset-password')->middleware('auth');
Route::post('reset-password','AdminController@newResetPassword')->name('reset-password')->middleware('auth');
Route::post('add-user','UserController@addNewUser')->name('add-user')->middleware('auth','user_type');
Route::post('remove_user','UserController@removeUser')->name('remove_user')->middleware('auth','user_type');


Route::get('dso-list','DSOController@dsoList')->name('dso-list')->middleware('auth');
Route::post('addDso','DSOController@addDso')->name('addDso')->middleware('auth');
Route::post('delete-dso','DSOController@deletedso')->name('delete-dso')->middleware('auth');
Route::post('get-dso','DSOController@getDso')->name('get-dso');
Route::post('update-dso','DSOController@updateDso')->name('update-dso')->middleware('auth');
Route::get('users','UserController@getUsers')->name('users')->middleware('auth');


Route::get('accounts','AccountsController@accounts')->name('accounts')->middleware('auth');
Route::post('get-wallet-no','AccountsController@getWalletsNos')->name('get-wallet-no')->middleware('auth');
Route::post('process-opening-cash','AccountsController@processOpeningCash')->name('process-opening-cash')->middleware('auth');
Route::post('process-closing-cash','AccountsController@processClosingCash')->name('process-closing-cash')->middleware('auth');
Route::post('payment-summery','AccountsController@getPaymentSummery')->name('payment-summery')->middleware('auth');
Route::post('add-income','AccountsController@addNewIncome')->name('add-income')->middleware('auth');
Route::post('addNewIncomeType','AccountsController@addNewIncomeType')->name('addNewIncomeType')->middleware('auth');
Route::post('removeIncomeType','AccountsController@removeType')->name('removeIncomeType')->middleware('auth');
Route::post('newInvest','AccountsController@newInvest')->name('newInvest')->middleware('auth','user_type');
Route::post('opening-transaction','AccountsController@openingTransaction')->name('opening-transaction')->middleware('auth');
Route::post('get-user-transaction-due-amount','AccountsController@getUserTransactionDueAmount')->name('get-user-transaction-due-amount')->middleware('auth');
Route::get('edit/{id}','SalaryController@editSalary')->name('edit-salary')->middleware('auth');
Route::post('update-salary','SalaryController@updateSalary')->name('update-salary')->middleware('auth');
Route::post('delete-salary/{id}','SalaryController@deleteSalary')->name('delete-salary')->middleware('auth');

Route::post('get-all-opening-transaction','AccountsController@getAllDailyOpeningTransactionsByUser')->name('get-all-opening-transaction')->middleware('auth');
Route::post('get-all-closing-transaction','AccountsController@getAllDailyClosingransactionsByUser')->name('get-all-closing-transaction')->middleware('auth');
Route::post('get-all-investments-today','AccountsController@getallInvestmentsToday')->name('get-all-investments-today')->middleware('auth');
Route::post('get-all-openings-today','AccountsController@getAllOpeningsToday')->name('get-all-openings-today')->middleware('auth');
Route::post('get-all-expense-given-today','AccountsController@getallExpenseAmountGivenToday')->name('get-all-expense-given-today')->middleware('auth');

Route::post('update_total_opening_amount','AccountsController@updateAmount')->name('update_total_opening_amount')->middleware('auth');
Route::post('update_total_opening_expense_amount','ExpenseController@updateAmount')->name('update_total_opening_expense_amount')->middleware('auth');
Route::post('delete_row','AccountsController@deleteRow')->name('delete_row')->middleware('auth');
Route::post('delete_expense_row','ExpenseController@deleteAmount')->name('delete_expense_row')->middleware('auth');


Route::post('give-amount','AccountsController@giveNewPersonalTransaction')->name('give-amount')->middleware('auth');
Route::post('receive-amount','AccountsController@receiveNewPersonalTransaction')->name('receive-amount')->middleware('auth');
Route::post('get-all-received-amounts','AccountsController@allReceivedPersonalTransaction')->name('get-all-received-amounts')->middleware('auth');
Route::post('get-all-given-amounts','AccountsController@allGivenPersonalTransaction')->name('get-all-given-amounts')->middleware('auth');

Route::get('income-edit/{id}','AccountsController@incomeEdit')->name('income-edit')->middleware('auth');
Route::post('update-income','AccountsController@updateIncome')->name('update-income')->middleware('auth');
Route::post('delete-income','AccountsController@deleteIncome')->name('delete-income')->middleware('auth');


Route::post('add-salary','SalaryController@addNewSalary')->name('add-salary')->middleware('auth');

Route::get('add-expense','ExpenseController@addNew')->name('newexpense')->middleware('auth');
Route::get('exp-lists','ExpenseController@getExpenseLists')->name('exp-lists')->middleware('auth');
Route::post('expense-enlist','ExpenseController@enlistExpense')->name('expense-enlist')->middleware('auth');
Route::post('addNewType','ExpenseController@addNewType')->name('addNewType')->middleware('auth');
Route::post('removeType','ExpenseController@removeType')->name('removeType')->middleware('auth');
Route::post('updateAmount','ExpenseController@updateAmount')->name('updateAmount')->middleware('auth');
Route::post('deleteAmount','ExpenseController@deleteAmount')->name('deleteAmount')->middleware('auth');


Route::get('add-new','FileController@add')->name('add-file')->middleware('auth','user_type');
Route::post('add-new','FileController@storeFile')->name('add-file')->middleware('auth');
Route::post('delete-file','FileController@deletefile')->name('delete-file')->middleware('auth','user_type');
Route::get('download/{file}','FileController@downloadFile')->name('download-file')->middleware('auth');


Route::get('dso-daily-transaction-report','ReportController@getDsoDailyTransactionReport')->name('dso-daily-transaction-report')->middleware('auth');
Route::get('dso-daily-transaction-report-view','ReportController@viewDsoDailyTransactionReport')->name('dso-daily-transaction-report-view')->middleware('auth');
Route::get('dso-daily-transaction-report-short','ReportController@dailyDSOShortReport')->name('dso-daily-transaction-report-short')->middleware('auth');
Route::get('dso-daily-transaction-short-report-view','ReportController@dailyDSOShortReportView')->name('dso-daily-transaction-short-report-view')->middleware('auth');

Route::get('salary-report','ReportController@salaryReport')->name('salary-report')->middleware('auth');

Route::get('salary-report-view','ReportController@getSalaryReportStatements')->name('salary-report-view')->middleware('auth');



Route::get('kpi-add','ManagerController@KPI')->name('kpi-add')->middleware('auth');
Route::post('kpi-add','ManagerController@addTarget')->name('kpi-add')->middleware('auth');
Route::get('target-list','ManagerController@getTargetList')->name('target-list')->middleware('auth');
Route::get('daily-target-achievement','ManagerController@dailyAchievementSet')->name('daily-target-achievement')->middleware('auth');
Route::post('daily-target-achievement','ManagerController@addTargetAchievement')->name('daily-target-achievement')->middleware('auth');
Route::get('daily-target-achievement-list','ManagerController@addTargetAchievementList')->name('daily-target-achievement-list')->middleware('auth');
Route::get('daily-target-achievement-edit/{id}','ManagerController@achievementEdit')->name('daily-target-achievement-edit')->middleware('auth');
Route::post('daily-target-achievement-update','ManagerController@achievementUpdate')->name('daily-target-achievement-update')->middleware('auth');
Route::post('daily-target-achievement-delete','ManagerController@achievementDelete')->name('daily-target-achievement-delete')->middleware('auth');
Route::get('daily-target-achievement-report','ManagerController@getAllTargetAchievementReport')->name('daily-target-achievement-report')->middleware('auth');
Route::get('achievement-report','ManagerController@reportPage')->name('achievement-report')->middleware('auth');