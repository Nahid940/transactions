<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'get-wallet-no',
        'payment-summery',
        'delete-dso',
        'removeType',
        'delete-file',
        'removeIncomeType',
        'get-dso',
        'get-all-opening-transaction',
        'get-all-closing-transaction',
        'update_total_opening_amount',
        'delete_row',
        'get-all-investments-today',
        'get-all-openings-today',
        'get-all-received-amounts',
        'get-all-given-amounts',
        'get-user-transaction-due-amount',
        'get-all-expense-given-today',
        'deleteAmount',
        'updateAmount',
        'delete_expense_row',
        'update_total_opening_expense_amount',
        'admin-trx-diary-update',
        'kpi-add',
        'daily-target-achievement',
        'daily-target-achievement-delete'
    ];
}
