<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManagerController extends Controller
{
    //

    public function KPI()
    {
        $dsos=DB::table('users')
            ->select('id','name','contact')
            ->where('is_deleted',0)
            ->where('username','=',null)
            ->get();
        return view('kpi.target-add')->with(['dsos'=>$dsos]);
    }

    public function addTarget(Request $request)
    {
        $exist=DB::table('dso_target')
            ->where('target_month',date('m'))
            ->where('dso_id',$request->dso_id)
            ->where('wallet_no',$request->wallet_no)
            ->first();
        if(empty($exist))
        {
            DB::table('dso_target')->insert([
                'target_month'=>date('m'),
                'target_year'=>date('Y'),
                'entry_date'=>date('Y-m-d'),
                'dso_id'=>$request->dso_id,
                'wallet_no'=>$request->wallet_no,
                'status'=>1,
                'transaction_target'=>$request->transaction_target==''?0:$request->transaction_target,
                'registration_target'=>$request->registration_target==''?0:$request->registration_target,
                'strike_target'=>$request->strike_target==''?0:$request->strike_target,
                'minimum_balance'=>$request->minimum_balance==''?0:$request->minimum_balance
            ]);

            return redirect()->back()->with('target-add-success','Data Saved Successfully!!');
        }else
        {
            return redirect()->back()->with('target-add-error','Target has already been set for this DSO!!');
        }
    }

    public function getTargetList(Request $request)
    {
        if($request->month!='')
        {
            $lists=DB::table('dso_target')->where('target_month',date('m'))
                ->select('name','dso_target.entry_date','transaction_target',
                    'registration_target','strike_target','minimum_balance')
                ->join('users','users.id','=','dso_target.dso_id')
                ->where('target_month',$request->month)
                ->get();
            return view('kpi.target-list')->with('lists',$lists);

        }else
        {
            $lists=DB::table('dso_target')->where('target_month',date('m'))
                ->select('name','dso_target.entry_date','transaction_target',
                    'registration_target','strike_target','minimum_balance')
                ->join('users','users.id','=','dso_target.dso_id')
                ->get();
            return view('kpi.target-list')->with('lists',$lists);
        }
    }

    public function dailyAchievementSet()
    {
        $dsos=DB::table('users')
            ->select('id','name','contact')
            ->where('is_deleted',0)
            ->where('username','=',null)
            ->get();
        return view('kpi.achievement-set')->with('dsos',$dsos);
    }


    public function addTargetAchievement(Request $request)
    {
        $exist=DB::table('target_achievements')
            ->where('entry_date',date('Y-m-d'))
            ->where('dso_id',$request->dso_id)
            ->where('wallet_no',$request->wallet_no)
            ->first();
        if(empty($exist))
        {
            DB::table('target_achievements')->insert([
                'dso_id'=>$request->dso_id,
                'wallet_no'=>$request->wallet_no,
                'transaction'=>$request->transaction==''?0:$request->transaction,
                'registration'=>$request->registration==''?0:$request->registration,
                'strike'=>$request->strike==''?0:$request->strike,
                'minimum_balance'=>$request->minimum_balance==''?0:$request->minimum_balance,
                'entry_date'=>date('Y-m-d'),
                'target_month'=>date('m'),
                'entry_year'=>date('Y'),
            ]);
            return redirect()->back()->with('acv-add-success','Data Added Successfully!');
        }else
        {
            return redirect()->back()->with('acv-add-error','Data Already Added !');
        }

    }

    public function addTargetAchievementList(Request $request)
    {
        if($request->date!='')
        {
            $lists=DB::table('target_achievements')->where('target_month',date('m'))
                ->select('achievementID','name','target_achievements.entry_date','transaction',
                    'registration','strike','minimum_balance')
                ->join('users','users.id','=','target_achievements.dso_id')
                ->where('target_achievements.entry_date',$request->date)
                ->get();
            return view('kpi.achievement-list')->with('lists',$lists);

        }else
        {
            $lists=DB::table('target_achievements')->where('target_month',date('m'))
                ->select('achievementID','name','target_achievements.entry_date','transaction',
                    'registration','strike','minimum_balance')
                ->join('users','users.id','=','target_achievements.dso_id')
                ->where('target_achievements.entry_date',date('Y-m-d'))
                ->get();
            return view('kpi.achievement-list')->with('lists',$lists);
        }
    }

    public function achievementEdit(Request $request)
    {
        $dsos=DB::table('users')
            ->select('id','name','contact')
            ->where('is_deleted',0)
            ->where('username','=',null)
            ->get();
        $list=DB::table('target_achievements')
            ->select('achievementID','name','target_achievements.entry_date','transaction',
                'registration','strike','minimum_balance','target_achievements.dso_id as dso_id','wallet_no')
            ->join('users','users.id','=','target_achievements.dso_id')
            ->where('achievementID',$request->id)
            ->first();

        return view('kpi.achievement-edit')->with(['list'=>$list,'dsos'=>$dsos]);
    }

    public function achievementUpdate(Request $request)
    {
        DB::table('target_achievements')
            ->where('achievementID',$request->achievementID)
            ->update([
                'target_month'=>date('m',strtotime($request->entry_date)),
                'entry_year'=>date('Y',strtotime($request->entry_date)),
                'entry_date'=>$request->entry_date,
                'registration'=>$request->registration,
                'strike'=>$request->strike,
                'minimum_balance'=>$request->minimum_balance,
                'dso_id'=>$request->dso_id,
                'wallet_no'=>$request->wallet_no,
            ]);

        return redirect()->back()->with('edit-success','Data Updated Successfully');
    }

    public function achievementDelete(Request $request)
    {
        DB::table('target_achievements')->where('achievementID',$request->id)->delete();
        return redirect()->back()->with('edit-success','Data Deleted Successfully');
    }


    public function reportPage()
    {
        $dsos=DB::table('users')
            ->select('id','name','contact')
            ->where('is_deleted',0)
            ->where('username','=',null)
            ->get();
        return view('kpi.achievement-report')->with('dsos',$dsos);
    }


    public function getAllTargetAchievementReport(Request $request)
    {
        $lists=DB::table('dso_target')
            ->where('target_month',$request->target_month)
            ->where('dso_id',$request->dso_id)
            ->select('name','dso_id','dso_target.entry_date','transaction_target',
                'registration_target','strike_target','minimum_balance','target_month')
            ->join('users','users.id','=','dso_target.dso_id')
        ->get();

        $achievement_list=[];
        foreach ($lists as $list)
        {

            $data=DB::table('target_achievements')
                ->select('achievementID','target_achievements.entry_date','transaction',
                    'registration','strike','minimum_balance','target_achievements.dso_id as dso_id','wallet_no')
                ->where('target_month',$list->target_month)
                ->where('dso_id',$list->dso_id)
                ->get();
            
            $achievement_list[]=[
              'dso_name'=>$list->name,
              'achievement_list'=>$data,
            ];
        }
        return view('kpi.target-report')->with('lists',$achievement_list);
    }
}
