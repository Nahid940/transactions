<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    //
    public function admin()
    {
        $dsos=User::with('wallets')
            ->where('is_deleted',0)
            ->where('type',0)
            ->get();
        $users=User::select('id','name','contact','username')->where('username','!=','')->get();



        return view('admin')->with(['dsos'=>$dsos,'users'=>$users]);
    }

    public function resetDB(Request $request)
    {
        if($request->password!='')
        {
            if(Hash::check($request->password,Auth::user()->password))
            {
                DB::table('expenses')->delete();
                DB::table('expense_types')->delete();
                DB::table('incomes')->delete();
                DB::table('income_types')->delete();
                DB::table('salary')->delete();
                DB::table('transactions')->delete();
                DB::table('investments')->delete();
                DB::table('personal_transactions')->delete();
//                DB::table('users_wallet')->delete();
                DB::table('cash_monitor')->delete();

                return redirect()->back()->with('success','Database reset is successful !!');
            }
            else
            {
                return redirect()->back()->with('error','Invalid User !!');
            }

        }else
        {
            return redirect()->back()->with('error','Password required !!');
        }
    }

    public function resetPassword()
    {
        return view('password');
    }

    public function newResetPassword(Request $request)
    {
        DB::table('users')->where('id',Auth::user()->id)->update(['password'=>bcrypt($request->password)]);
        return redirect()->back()->with('success','Password updated !!');
    }


    public function addNewPersonalAccounts(Request $request)
    {
        DB::table('personal_transaction_diary')->insert([
            'amount'=>$request->amount,
            'entry_date'=>$request->date,
            'remark'=>$request->remark,
            'reference'=>$request->reference,
            'type'=>$request->type,
        ]);

        return redirect()->back()->with('trx-admin-success','Data saved successfully!!');
    }

    public function getAllPersonalAccounts()
    {
        $lists=DB::table('personal_transaction_diary')->select('*')->orderby('id','desc')->paginate(30);
        return view('personal_transaction_diary')->with('lists',$lists);
    }

    public function deletePersonalTRX(Request $request)
    {
        DB::table('personal_transaction_diary')->where('id',$request->id)->delete();
        return redirect()->back()->with('trx-delete','Data removed successfully !!');
    }

    public function editViewPersonalTRX(Request $request)
    {
        $data=DB::table('personal_transaction_diary')->select('*')->where('id',$request->id)->first();
        return view('personal-trx-edit')->with('data',$data);
    }

    public function updatePersonalTRX(Request $request)
    {
        DB::table('personal_transaction_diary')
            ->where('id',$request->id)
            ->update([
                'amount'=>$request->amount,
                'entry_date'=>$request->date,
                'remark'=>$request->remark,
                'reference'=>$request->reference,
                'type'=>$request->type,
            ]);
        return redirect()->back()->with('trx-update','Data updated successfully !!');
    }
}
