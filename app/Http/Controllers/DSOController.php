<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DSOController extends Controller
{
    //
    public function add()
    {

    }

    public function dsoList()
    {
        return view('dso/list');
    }


    public function addDso(Request $request)
    {
        $id=DB::table('users')->insertGetId([
            'name'=>$request->name,
            'contact'=>$request->contact,
            'type'=>0,
            'entry_date'=>date('Y-m-d'),
            'entry_user'=>1,
        ]);
        $this->addWalletNo($request,$id);

        return redirect()->back()->with('success','Data added successfully');
    }

    public function addWalletNo($request,$id)
    {
        for($i=0;$i<sizeof($request->wallet_no);$i++)
        {
            DB::table('users_wallet')->insert([
                'dso_id'=>$id,
                'wallet_no'=>$request->wallet_no[$i],
            ]);
        }
    }

    public function deletedso(Request $request)
    {
        DB::table('users')
            ->where('id',$request->id)
            ->update(['is_deleted'=>1]);
    }

    public function getDso(Request $request)
    {
        $dso=User::with('wallets')->where('id',$request->id)->first();
        return view('ajax-files.dso-form-element')->with(['dso'=>$dso]);
    }

    public function updateDso(Request $request)
    {
        DB::table('users_wallet')->where('dso_id',$request->id)->delete();

        DB::table('users')
            ->where('id',$request->id)
            ->update(['name'=>$request->name]);
        $size=(is_null($request->wallet_no)?0:sizeof($request->wallet_no));

        if($size>=1)
        {
            for($i=0;$i<$size;$i++)
            {
                if($request->wallet_no[$i]!='')
                {
                    DB::table('users_wallet')->insert([
                        'dso_id'=>$request->id,
                        'wallet_no'=>$request->wallet_no[$i],
                    ]);
                }
            }
        }


        return redirect()->back()->with('success','Data updated successfully');

    }
}
