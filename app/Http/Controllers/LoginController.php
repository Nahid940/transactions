<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //

    public function login()
    {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password,'is_deleted'=>0])) {
            // The user is active, not suspended, and exists.
//            if()
            //$type=User::where('username',$request->username)->select('type')->first();
            if(Auth::user()->type==1)
            {
                return redirect(route('admin'));
            }else
            {
                return redirect(route('accounts'));
            }
        }else
        {
            return redirect(route('login'))->with('error','Invalid Credentials!!');
        }
    }


    public function logout()
    {
        Auth::logout();
        return redirect(route('login'));
    }
}
