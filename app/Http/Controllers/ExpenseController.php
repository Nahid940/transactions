<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExpenseController extends Controller
{
    //

    public function addNew()
    {
        $users=DB::table('users')
            ->select('id','name','contact')
            ->where('is_deleted',0)
            ->get();

        $expense_types=DB::table('expense_types')->select('id','type_name')->get();



        return view('expense.add-new')->with(['users'=>$users,'expense_types'=>$expense_types]);
    }

    public function getExpenseLists()
    {
        return view('expense.lists')->with('expenses',$this->getAllExpenses());
    }

    public function enlistExpense(Request $request)
    {
        if(sizeof($request->expense_type)>=1)
        {
            if($request->post_from=='account')
            {
                if($request->reference_person=='')
                {
                    return  redirect(route('accounts'))->with([
                        'reference_person_error'=>'Please Select DSO!!'
                    ]);
                }

                for ($i=0;$i<sizeof($request->expense_type);$i++)
                {
                    if($request->expense_type[$i]!='' && $request->amount[$i]!='')
                    {
                        DB::table('expenses')
                            ->insert([
                                'title'=>$request->title,
                                'memo'=>$request->memo,
                                'unit'=>$request->unit,
                                'expense_id'=>$request->expense_type[$i],
                                'total_amount'=>$request->amount[$i],
                                'entry_user'=>1,
                                'entry_date'=>date('Y-m-d'),
                                'reference_person'=>$request->reference_person,
                                'created_at'=>date('Y-m-d h:i:s a'),
                                'type'=>($request->expense_type[$i]==10000?30:$request->type),//fixed asset for 30
                            ]);
                    }
                }

                return redirect(route('accounts'))->with('expense_success','Data stored successfully!');
            }else
            {

                for ($i=0;$i<sizeof($request->expense_type);$i++)
                {
                    if($request->expense_type[$i]!='' && $request->amount[$i]!='')
                    {
                        DB::table('expenses')
                            ->insert([
                                'title'=>$request->title,
                                'memo'=>$request->memo,
                                'unit'=>$request->unit,
                                'expense_id'=>$request->expense_type[$i],
                                'total_amount'=>$request->amount[$i],
                                'entry_user'=>1,
                                'entry_date'=>date('Y-m-d'),
                                'reference_person'=>$request->reference_person,
                                'created_at'=>date('Y-m-d h:i:s a'),
                                'type'=>($request->expense_type[$i]==10000?30:$request->type),
                            ]);
                    }
                }

                return redirect(route('exp-lists'))->with(['success','Expense added successfully !','expenses',$this->getAllExpenses()]);
            }
        }

        if($request->post_from=='account')
        {
            return redirect(route('accounts'));
        }else
        {
            return redirect(route('newexpense'))->with('error','Invalid submission!');
        }
    }

    public function getAllExpenses()
    {
        $expenses=DB::table('expenses')
            ->select('total_amount','expenses.entry_date','name','type_name')
            ->leftJoin('expense_types','expense_types.id','expenses.expense_id')
            ->leftJoin('users','users.id','expenses.reference_person')
            ->where('expenses.is_deleted',0)
            ->get();
        return $expenses;
    }

    public function addNewType(Request $request)
    {
        DB::table('expense_types')->insert([
            'type_name'=>$request->type_name,
            'created_by'=>1,
        ]);
        return redirect()->back()->with('type-success',"Data added successfully");
    }

    public function removeType(Request $request)
    {
        DB::table('expense_types')->where('id',$request->id)->delete();
    }


    public function updateAmount(Request $request)
    {

        DB::table('expenses')
            ->where('id',$request->id)
            ->update(['total_amount'=>$request->total_amount]);

    }

    public function deleteAmount(Request $request)
    {
        DB::table('expenses')
            ->where('id',$request->id)
            ->delete();
    }



}
