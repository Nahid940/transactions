<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //

    public function getDsoDailyTransactionReport()
    {
        $dsos=DB::table('users')->where('type',0)->where('is_deleted',0)->select('id','name')->get();
        return view('report.dso-daily-transaction-report')->with('dsos',$dsos);
    }

    public function viewDsoDailyTransactionReport(Request $request)
    {
        $dso=DB::table('users')->select('name')->where('id',$request->dso)->first();
        $transactions=DB::table('transactions')
            ->where('user_id',$request->dso)
            ->where('entry_date','=',$request->start_date)
            //->where('entry_date','<=',$request->end_date)
            ->select('total_amount','transaction_type','entry_date','wallet_no')
            ->get();
        $expenses=DB::table('expenses')
            ->where('reference_person',$request->dso)
            ->where('entry_date','=',$request->start_date)
            ->sum('total_amount');

        return view('report.dso-daily-transaction-details-view')->with([
            'dso'=>$dso,
            'transactions'=>$transactions,
            'expense'=>$expenses,
            'mtd'=>$this->getMTD($request->dso,$request->start_date,"<=")
        ]);
    }

    public function getMTD($id,$date,$range)
    {
        $total_given=DB::table('transactions')
            ->where('user_id',$id)
            ->where('entry_date',$range,$date)
            ->where(function($query){
                $query->where('transaction_type',2)
                    ->orWhere('transaction_type',3);
            })
            ->sum('total_amount');

        $total_return=DB::table('transactions')
            ->where('user_id',$id)
            ->where('entry_date',$range,$date)
            ->where(function($query){
                $query->where('transaction_type',5)
                    ->orWhere('transaction_type',6)
                    ->orWhere('transaction_type',7);
            })
            ->sum('total_amount');

//        ->orWhere('transaction_type',4)

        return $total_given-$total_return;

    }


    public function salaryReport()
    {
        $dsos=DB::table('users')->where('type',0)->where('is_deleted',0)->select('id','name')->get();
        return view('report.salary-report')->with('users',$dsos);
    }
    public function getSalaryReportStatements(Request $request)
    {
        if($request->u=='')
        {
            $lists=DB::table('salary')
                ->where('entry_month',$request->m)
                ->where('entry_year',$request->y)
                ->join('users','users.id','=','salary.user_id')
                ->select('name','basic','phone_bill','ta_da','incentive','other_incentive','salary.entry_date')
                ->get();
        }else
        {
            $lists=DB::table('salary')
                ->join('users','users.id','=','salary.user_id')
                ->where('entry_month',$request->m)
                ->where('entry_year',$request->y)
                ->where('user_id',$request->u)
                ->select('name','basic','phone_bill','ta_da','incentive','other_incentive','salary.entry_date')
                ->get();
        }
        return view('report.salary-report-view')->with('salary_lists',$lists);
    }


    public function dailyDSOShortReport()
    {
        $dsos=DB::table('users')->where('type',0)->where('is_deleted',0)->select('id','name')->get();
        return view('report.dso-transaction-short')->with('dsos',$dsos);
    }

    public function dailyDSOShortReportView(Request $request)
    {
        $dsos=DB::table('users')->where('type',0)->where('is_deleted',0)->select('id','name')->get();
        $lists=[];
        if($request->dso=='')
        {
            foreach ($dsos as $dso)
            {
                $transactions=DB::table('transactions')
                    ->select('transaction_type','total_amount')
                    ->where('user_id',$dso->id)
                    ->where('entry_date','=',$request->start_date)
                    ->get();

                $lists[]=[
                    'dso_name'=>$dso->name,
                    'transactions'=>$transactions,
                    'mtd_due'=>$this->getMTD($dso->id,$request->start_date,'<=')
                ];
            }

        }else
        {
            $dso_name=DB::table('users')->where('id',$request->dso)->select('name')->first();

            $transactions=DB::table('transactions')
                ->select('transaction_type','total_amount')
                ->where('user_id',$request->dso)
                ->where('entry_date','=',$request->start_date)
                ->get();

            $lists[]=[
                'dso_name'=>$dso_name->name,
                'transactions'=>$transactions,
                'mtd_due'=>$this->getMTD($request->dso,$request->start_date,'<=')
            ];

        }

        return view('report.dso-daily-transaction-report-view')->with(['lists'=>$lists]);
    }
}
