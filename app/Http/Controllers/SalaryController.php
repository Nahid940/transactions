<?php

namespace App\Http\Controllers;

use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SalaryController extends Controller
{

    public function addNewSalary(Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'user_id' => 'required',
            'basic' => 'required',
        ]);

        if($validator->fails())
        {
            return redirect(route('accounts'))
                ->with(['salary_error'=>$validator->errors(),'tab'=>'salary']);

        }else
        {

            if($request->others!='' && $request->others>0)
            {
                $basic=$request->basic+$request->others;

                DB::table('transactions')
                    ->insert([
                        'total_amount'=>$request->others,
                        'user_id'=>$request->user_id,
                        'transaction_type'=>40, // advance merged with salary
                        'entry_date'=>date('Y-m-d'),
                        'is_final'=>1,
                        'is_deleted'=>0,
                        'entry_user'=>Auth::user()->id,
                        'edited_by'=>0,
                        'created_at'=>date('Y-m-d h:i:s a'),
                        'wallet_no'=>0
                    ]);

            }else
            {
                $basic=$request->basic;
            }


            DB::table('salary')->insert([
                'user_id'=>$request->user_id,
                'basic'=>$basic,
                'phone_bill'=>$request->phone_bill,
                'ta_da'=>$request->ta_da,
                'incentive'=>$request->incentive,
                'other_incentive'=>$request->other_incentive,
                'entry_date'=>date('Y-m-d'),
                'entry_month'=>date('m'),
                'entry_year'=>date('Y'),
                'entry_user'=>Auth::user()->id,
                'transaction_type'=>$request->transaction_type,
            ]);
            return redirect(route('accounts'))
                ->with(['success'=>"Data stored successfully!",'tab'=>'salary']);
        }
    }


    public function editSalary(Request $request)
    {
        $users=DB::table('users')->select('id','name')->get();
        $list=DB::table('salary')->where('id',$request->id)->first();
        return view('accounts.salary-edit')->with(['list'=>$list,'users'=>$users,'id'=>$request->id]);
    }

    public function updateSalary(Request $request)
    {
        DB::table('salary')
            ->where('id',$request->row_id)
            ->update([
            'user_id'=>$request->user_id,
            'basic'=>$request->basic,
            'phone_bill'=>$request->phone_bill,
            'ta_da'=>$request->ta_da,
            'incentive'=>$request->incentive,
            'other_incentive'=>$request->other_incentive,
        ]);

        return redirect()->back()->with('success','Record updated successfully!');
    }

    public function deleteSalary(Request $request)
    {
        DB::table('salary')
            ->where('id',$request->id)->delete();
        return redirect(route('accounts'))->with(['success'=>'Record updated successfully!','tab'=>'salary']);
    }
}
