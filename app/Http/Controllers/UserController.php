<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //

    public function getUsers()
    {
        return view('users.users');
    }


    public function addNewUser(Request $request)
    {
        DB::table('users')->insert([
            'username'=>$request->username,
            'password'=>Hash::make($request->password),
            'name'=>$request->name,
            'contact'=>null,
            'address'=>null,
            'nid'=>null,
            'entry_date'=>date('Y-m-d'),
            'entry_user'=>Auth::user()->id,
            'type'=>$request->type
        ]);
        return redirect()->back()->with('success','User saved successfully!!');
    }


    public function removeUser(Request $request)
    {
        DB::table('users')->where('id',$request->user)->delete();
        return redirect()->back()->with('success','User removed successfully!!');
    }
}
