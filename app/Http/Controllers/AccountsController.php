<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use nahid940\NumberToWords\NumberToWords;

class AccountsController extends Controller
{
    public function accounts(Request $request)
    {
        $dsos=DB::table('users')
            ->select('id','name','contact')
            ->where('is_deleted',0)
            ->where('username','=',null)
            ->get();

        $users=DB::table('users')
            ->select('id','name','contact')
            ->where('is_deleted',0)
            ->get();

        $opening_cash_invest=$this->getIndividualInvestAmount(1); // invest cash or hand
        $opening_b2b_invest=$this->getIndividualInvestAmount(11); // invest b2b or hand
        $opening_bank_invest=$this->getIndividualInvestAmount(22); // invest bank

        $investAmount=$this->getTotalInvestAmount();

        $opening_cash=$this->getIndividualAmounts(2,'=',date('Y-m-d'));
        $opening_b2b=$this->getIndividualAmounts(3,'=',date('Y-m-d'));
        $opening_bank=0;

        $closing_cash=$this->getIndividualAmounts(5,'=',date('Y-m-d'));
        $closing_b2b=$this->getIndividualAmounts(6,'=',date('Y-m-d'));
        $closing_bank=$this->getIndividualAmounts(7,'=',date('Y-m-d'));



        $invests=DB::table('investments')
            ->select('title','total_amount','entry_date','transaction_type')
            ->where('is_deleted',0)
            ->orderby('investID','desc')
            ->paginate(10);

        $closing_cash_salary=$this->getSalaryAmountsByType(1,'=',date('Y-m-d'));
        $closing_b2b_salary=$this->getSalaryAmountsByType(2,'=',date('Y-m-d'));

        return view('accounts.account',
            ['dsos'=>$dsos,
                'opening_cash'=>$opening_cash,
                'opening_b2b'=>$opening_b2b,
                'opening_bank'=>$opening_bank,

                'closing_cash'=>($closing_cash-$closing_cash_salary),
                'closing_b2b'=>($closing_b2b-$closing_b2b_salary),
                'closing_bank'=>$closing_bank,

                'closing_cash_salary'=>$closing_cash_salary,
                'closing_b2b_salary'=>$closing_b2b_salary,

                'opening_cash_invest'=>$opening_cash_invest,
                'opening_b2b_invest'=>$opening_b2b_invest,
                'opening_bank_invest'=>$opening_bank_invest,

                'total_opening_transaction_at_hand'=>($this->getTotalPaymentByType(2,"<",date('Y-m-d'))),
                'total_closing_transaction_at_hand'=>$this->getTotalPaymentByType(5,"<",date('Y-m-d')),
                'total_opening_transaction_at_b2b'=>($this->getTotalPaymentByType(3,"<",date('Y-m-d'))),
                'total_closing_transaction_at_b2b'=>$this->getTotalPaymentByType(6,"<",date('Y-m-d')),

//            'total_opening_transaction_at_bank'=>$this->getTotalPaymentByType(3,"<",date('Y-m-d')),
                'total_closing_transaction_at_bank'=>$this->getTotalPaymentByType(7,"<",date('Y-m-d')),

                'todays_total_dso_expense'=>$this->getTodaysTotalExpesne(),
                'todays_office_expense'=>$this->getTodaysOtherExpesne(),
                'todays_total_dso_exp_amount'=>$this->getTodaysTotalDSOExpesne(),
                'total_expense'=>$this->getTotalExpesne('<'),

                'todays_fixed_assets'=>$this->getTotalFixedAssets('='),
                'total_fixed_assets'=>$this->getTotalFixedAssets('<'),

                'total_cash_salary'=>$this->getSalaryAmountsByType(1,'<',date('Y-m-d')),
                'total_b2b_salary'=>$this->getSalaryAmountsByType(2,'<',date('Y-m-d')),
                'todays_total_bank_advance'=>$this->getTotalPaymentByType(32,'=',date('Y-m-d')),


                'todays_invest'=>$this->getTodaysInvestAmount(),
                'total_invest'=>$investAmount,
                'total_due_collection'=>$this->getTotalPaymentByType(4,'<=',date('Y-m-d')),
                'todays_due_collection'=>$this->getTotalPaymentByType(4,'=',date('Y-m-d')),
                'todays_advance_collection'=>$this->getTodaysAdvanceCollection(),
                'total_advance_collection'=>$this->getMTDAdvanceCollectionAmount('<',date('Y-m-d')),

                'personal_advance_payment_given_open'=>$this->getTotalAdvancePaymentGiven('<',date('Y-m-d')),
                'personal_advance_payment_received_open'=>$this->getTotalAdvancePaymentReceived('<',date('Y-m-d')),
                'personal_advance_payment_given_close'=>$this->getTotalAdvancePaymentGiven('=',date('Y-m-d')),
                'personal_advance_payment_received_close'=>$this->getTotalAdvancePaymentReceived('=',date('Y-m-d')),
                'total_advance_merged_with_salary'=>$this-> getTotalMergedAdvanceAmount('<',date('Y-m-d')),
                'total_todays_advance_merged_with_salary'=>$this-> getTotalMergedAdvanceAmount('=',date('Y-m-d')),
                'income_types'=>$this->getAllIncomeTypes(),
                'users'=>$users,
                'salaries'=>$this->getSalary($request),
                'incomes'=>$this->getIncomes($request),
                'expense_types'=>$this->getAllExpenseTypes(),
                'invests'=>$invests,
                'total_salary_cost'=>$this->getTotalSalaryCost('<'),
//                'todays_salary_cost'=>$this->getTotalSalaryCost('='),
                'advance_bank_withdraw'=>$this->getBankAmount('=',32),
                'todays_bank_withdraw'=>$this->getBankAmount('=',31),
                'todays_bank_return'=>$this->getBankAmount('=',33),
                'total_bank_withdraw'=>$this->getBankAmount('<=',31),
                'total_bank_advance_withdraw'=>$this->getBankAmount('<=',32),
                'total_bank_advance_return'=>$this->getBankAmount('<=',33),
                'cash_monitor_amount'=>$this->getCashMonitorAmount()
            ]);
    }

    public function getDSOTotalAdvanceCollectionAmount($user_id,$range,$date)
    {

        $amount=DB::table('transactions')
            ->where('user_id',$user_id)
            ->where('entry_date',$range,$date)
            ->where('is_deleted',0)
            ->where(function ($query){
                $query
                    ->where('transaction_type',20)
                    ->orWhere('transaction_type',21)
                    ->orWhere('transaction_type',23);
            })
            ->sum('total_amount');

        $return_amount=DB::table('transactions')
            ->where('user_id',$user_id)
            ->where('entry_date',$range,$date)
            ->where('is_deleted',0)
            ->where('transaction_type',37)
            ->sum('total_amount');

        return $amount-$return_amount;
//        return $return_amount;
    }


    public function getMTDAdvanceCollectionAmount($range,$date)
    {
        $amount=DB::table('transactions')
            ->where('entry_date',$range,$date)
            ->where('is_deleted',0)
            ->where('transaction_type',20)
            ->orWhere('transaction_type',21)
            ->orWhere('transaction_type',23)
            ->sum('total_amount');

        $return_amount=DB::table('transactions')
            ->where('entry_date','<=',$date)
            ->where('is_deleted',0)
            ->where('transaction_type',37)
            ->sum('total_amount');

        return $amount-$return_amount;
    }




    public function getTotalAdvanceCollectionAmount($range,$date)
    {
        $amount=DB::table('transactions')
            ->where('entry_date',$range,$date)
            ->where('is_deleted',0)
            ->where('transaction_type',20)
            ->orWhere('transaction_type',21)
            ->orWhere('transaction_type',23)
            ->sum('total_amount');

        $return_amount=DB::table('transactions')
            ->where('entry_date',$range,$date)
            ->where('is_deleted',0)
            ->where('transaction_type',37)
            ->sum('total_amount');

        return $amount-$return_amount;
    }


    public function getTotalPaymentByType($type,$range,$date)
    {
        $amount=DB::table('transactions')
            ->where('entry_date',$range,$date)
            ->where('is_deleted',0)
            ->where('transaction_type',$type)
            ->sum('total_amount');
        return $amount;
    }

    public function getAllTotalClsoingPayment()
    {
        $opening_clsoing_amount=DB::table('transactions')
            ->where('entry_date',date('Y-m-d'))
            ->where('is_deleted',0)
            ->where(function ($query){
                $query
                    ->where('transaction_type',5)
                    ->orWhere('transaction_type',6)
                    ->orWhere('transaction_type',7);
            })
            ->sum('total_amount');
        return $opening_clsoing_amount;
    }


    public function getTotalInvestAmount()
    {
        $invest_amount=DB::table('transactions')
            ->where('entry_date','<',date('Y-m-d'))
            ->where('is_deleted',0)
            ->where(function ($query){
                $query
                    ->where('transaction_type',1) //invest @ cash
                    ->orWhere('transaction_type',11) //invest @ cash
                    ->orWhere('transaction_type',22); //invest @ bank
            })
            ->sum('total_amount');
        return $invest_amount;

    }


    public function getTodaysInvestAmount()
    {
        $invest_amount=DB::table('transactions')
            ->where('entry_date','=',date('Y-m-d')) //invest @ cash
            ->where(function ($query){
                $query
                    ->where('transaction_type',1) //invest @ cash
                    ->orWhere('transaction_type',11) //invest @ cash
                    ->orWhere('transaction_type',22); //invest @ bank
            })
            ->where('is_deleted',0)
            ->sum('total_amount');
        return $invest_amount;
    }

    public function getIndividualInvestAmount($type)
    {
        $invest_amount=DB::table('transactions')
            ->where('transaction_type',$type) //invest @ cash
            ->where('is_deleted',0)
            ->where('entry_date','<',date('Y-m-d'))
            ->sum('total_amount');
        return $invest_amount;
    }

    public function getSalary($request)
    {
        if(!empty($request->start) && !empty($request->end) && !empty($request->user))
        {
            $salaries=DB::table('salary')
                ->select('salary.id','name','basic','phone_bill','ta_da','incentive',
                    'other_incentive','salary.entry_date','entry_month')
                ->join('users','users.id','salary.user_id')
                ->where('salary.is_deleted',0)
                ->where('user_id',$request->user)
                ->where('salary.entry_date', '>=', $request->start)
                ->where('salary.entry_date', '<=', $request->end)
                ->orderby('salary.id','desc')
                ->paginate(20);
            return $salaries;

        }

        $salaries=DB::table('salary')
            ->select('salary.id','name','basic','phone_bill','ta_da','incentive',
                'other_incentive','salary.entry_date','entry_month')
            ->join('users','users.id','salary.user_id')
            ->where('salary.is_deleted',0)
            ->orderby('salary.id','desc')
            ->paginate(20);

        return $salaries;
    }

    public function getIncomes($request)
    {
        if(!empty($request->start) && !empty($request->end) && !empty($request->income_type_id))
        {
            $incomes=DB::table('incomes')
                ->select('income_id','type_name','total_amount','incomes.entry_date')
                ->join('income_types','income_types.income_type_id','incomes.income_type_id')
                ->where('incomes.income_type_id',$request->income_type_id)
                ->where('incomes.is_deleted',0)
                ->where('incomes.entry_date', '>=', $request->start)
                ->where('incomes.entry_date', '<=', $request->end)
                ->orderby('incomes.income_type_id','desc')
                ->paginate(20);
            return $incomes;

        }

        $incomes=DB::table('incomes')
            ->select('income_id','type_name','total_amount','incomes.entry_date')
            ->join('income_types','income_types.income_type_id','incomes.income_type_id')
            ->where('incomes.is_deleted',0)
            ->orderby('incomes.income_type_id','desc')
            ->paginate(20);

        return $incomes;
    }


    public function getTodaysAdvanceCollection()
    {
//        $total_opening_payment=$this->getAllTotalOpeningPayment();
//        $total_closing_payment=$this->getAllTotalClsoingPayment();
//        $total_due_collection=$this->getTodaysTotalDueCollection();
//        $amount=($total_closing_payment-$total_due_collection);
//        if($amount>$total_opening_payment)
//        {
//            return $amount-$total_opening_payment;
//        }

        $advance_collection=DB::table('transactions')
            ->where('entry_date',date('Y-m-d'))
            ->where(function ($query){
                $query
                    ->where('transaction_type',20) //advance cash
                    ->orWhere('transaction_type',21) //advance b2b
                    ->orWhere('transaction_type',23); //advance bank
            })
            ->sum('total_amount');
        return $advance_collection;
    }

//    public function getTotalAdvanceCollection()
//    {
////        $total_opening_payment=$this->getAllTotalOpeningPayment();
////        $total_closing_payment=$this->getAllTotalClsoingPayment();
////        $total_due_collection=$this->getTodaysTotalDueCollection();
////        $amount=($total_closing_payment-$total_due_collection);
////        if($amount>$total_opening_payment)
////        {
////            return $amount-$total_opening_payment;
////        }
//        $advance_collection=DB::table('transactions')
//            ->where('entry_date','<',date('Y-m-d'))
//            ->where('transaction_type',20) //advance cash
//            ->orWhere('transaction_type',21) //advance b2b
//            ->orWhere('transaction_type',23) //advance bank
//            ->sum('total_amount');
//        return $advance_collection;
//    }

    public function getAllIncomeTypes()
    {
        return $types=DB::table('income_types')->select('income_type_id','type_name')->where('is_deleted',0)->get();
    }


    public function getIndividualAmounts($type,$range,$date)
    {
        $amount=DB::table('transactions')
            ->where('transaction_type',$type)
            ->where('is_deleted',0)
            ->where('entry_date',$range,$date)
            ->sum('total_amount');
        return $amount;
    }

    public function getSalaryAmountsByType($type,$range,$date)
    {
        $amounts=DB::table('salary')
            ->where('transaction_type',$type)
            ->where('is_deleted',0)
            ->where('entry_date',$range,$date)
            ->select('basic','phone_bill','ta_da','incentive','other_incentive')
            ->get();
        $ttl=0;
        foreach ($amounts as $amount)
        {
            $ttl+=$amount->basic+$amount->phone_bill+$amount->ta_da+$amount->incentive+$amount->other_incentive;
        }

        return $ttl;
    }

    public function getAllTotalOpeningPayment()
    {
        $opening_given_amount=DB::table('transactions')
            ->where('entry_date',date('Y-m-d'))
            ->where('is_deleted',0)
            ->where(function ($query){
                $query
                    ->where('transaction_type',2)
                    ->orWhere('transaction_type',3);
            })
            ->sum('total_amount');
        return $opening_given_amount;
    }

//    public function getAllTotalClsoingPayment()
//    {
//        $opening_clsoing_amount=DB::table('transactions')
//            ->where('entry_date',date('Y-m-d'))
//            ->where('is_deleted',0)
//            ->where(function ($query){
//                $query
//                    ->where('transaction_type',5)
//                    ->orWhere('transaction_type',6)
//                    ->orWhere('transaction_type',7);
//            })
//            ->sum('total_amount');
//        return $opening_clsoing_amount;
//    }



    public function getOpeningBalanceAtHand()
    {

    }

    public function getWalletsNos(Request $request)
    {
        $dso_wallets=DB::table('users_wallet')
            ->select('wallet_no')
            ->where('dso_id',$request->id)
            ->get();
        return view('ajax-files.dsowallets',['dso_wallets'=>$dso_wallets]);
    }

    public function processOpeningCash(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'total_amount_cash' => 'required',
            'dso_id' => 'required',
            'wallet_no' => 'required',
        ]);
        if($validator->fails())
        {
            return redirect(route('accounts'))->with('errors',$validator->errors());
        }else{
            if($request->total_amount_cash!='' && $request->total_amount_b2b!='' && $request->opening_due_collection!='')
            {
                $this->addAmount($request,2,$request->total_amount_cash); //opening cash
                $this->addAmount($request,3,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,4,$request->opening_due_collection); // opening due collection
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }

            else if($request->total_amount_cash!='' && $request->total_amount_b2b!='' && $request->advance_payment!='')
            {
                $this->addAmount($request,2,$request->total_amount_cash); //opening cash
                $this->addAmount($request,3,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,37,$request->advance_payment); //advance return
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_cash!='' && $request->total_amount_b2b!='' )
            {
                $this->addAmount($request,2,$request->total_amount_cash); //opening cash
                $this->addAmount($request,3,$request->total_amount_b2b); //opening b2b
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_cash!='' && $request->opening_due_collection!='' )
            {
                $this->addAmount($request,2,$request->total_amount_cash); //opening cash
                $this->addAmount($request,4,$request->opening_due_collection); //opening due collection
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_b2b!='' && $request->opening_due_collection!='' )
            {
                $this->addAmount($request,3,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,4,$request->opening_due_collection); //opening due collection
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_cash!='' && $request->advance_payment!='')
            {
                $this->addAmount($request,2,$request->total_amount_cash); //opening cash
                $this->addAmount($request,37,$request->advance_payment); //opening cash
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_cash!='')
            {
                $this->addAmount($request,2,$request->total_amount_cash); //opening cash
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_b2b!='' && $request->advance_payment!='')
            {
                $this->addAmount($request,3,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,37,$request->advance_payment); //opening b2b
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_b2b!='')
            {
                $this->addAmount($request,3,$request->total_amount_b2b); //opening b2b
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->opening_due_collection!='')
            {
                $this->addAmount($request,4,$request->opening_due_collection); //opening due collection
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->opening_due_collection!='' && $request->advance_payment!='')
            {
                $this->addAmount($request,4,$request->opening_due_collection); //opening due collection
                $this->addAmount($request,37,$request->advance_payment); //advance payment
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }
            else if($request->advance_payment!='')
            {
                $this->addAmount($request,37,$request->advance_payment); //opening due collection
                return redirect(route('accounts'))->with('opening_cash_success','Data stored successfully !');
            }

            return redirect(route('accounts'));
        }
    }


    public function processClosingCash(Request $request)
    {

        $validator = Validator::make($request->all(), [
            //'total_amount_cash' => 'required',
            'dso_id' => 'required',
            'wallet_no' => 'required',
        ]);


        if($validator->fails())
        {
            return redirect(route('accounts'))->with('errors',$validator->errors());
        }else{


            if($request->advance_salary!='')
            {
                DB::table('salary')->insert([
                    'user_id'=>$request->dso_id,
                    'basic'=>$request->advance_salary,
                    'phone_bill'=>0,
                    'ta_da'=>0,
                    'incentive'=>0,
                    'other_incentive'=>0,
                    'entry_date'=>date('Y-m-d'),
                    'entry_month'=>date('m'),
                    'entry_year'=>date('Y'),
                    'entry_user'=>1,
                    'salary_type'=>0, //advance_salary
                ]);
            }



            if($request->total_amount_cash!=''
                && $request->total_amount_b2b!=''
                && $request->closing_cheque_payment!=''
                && $request->advance_collection_type!='' && $request->advance_collection_amount!=''
            )
            {
                $this->addAmount($request,5,$request->total_amount_cash); //opening cash
                $this->addAmount($request,6,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,7,$request->closing_cheque_payment); // closing cheque collection
                $this->addAmount($request,$request->advance_collection_type,$request->advance_collection_amount); // advance collection collection

                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_cash!=''
                && $request->total_amount_b2b!=''
                && $request->closing_cheque_payment!=''
            )
            {
                $this->addAmount($request,5,$request->total_amount_cash); //opening cash
                $this->addAmount($request,6,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,7,$request->closing_cheque_payment); // closing cheque collection
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }

            else if($request->total_amount_cash!='' && $request->total_amount_b2b!='' && $request->advance_collection_type!='' && $request->advance_collection_amount!='' )
            {
                $this->addAmount($request,5,$request->total_amount_cash); //opening cash
                $this->addAmount($request,6,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,$request->advance_collection_type,$request->advance_collection_amount);//advance collection
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_cash!='' && $request->total_amount_b2b!='' )
            {
                $this->addAmount($request,5,$request->total_amount_cash); //opening cash
                $this->addAmount($request,6,$request->total_amount_b2b); //opening b2b
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }

            else if($request->closing_cheque_payment!='' && $request->total_amount_b2b!='' && $request->advance_collection_type!='' && $request->advance_collection_amount!='' )
            {
                $this->addAmount($request,7,$request->closing_cheque_payment); //closing cheque collection
                $this->addAmount($request,6,$request->total_amount_b2b); //opening b2b
                $this->addAmount($request,$request->advance_collection_type,$request->advance_collection_amount);//advance collection
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }

            else if($request->closing_cheque_payment!='' && $request->total_amount_b2b!='' )
            {
                $this->addAmount($request,7,$request->closing_cheque_payment); //closing cheque collection
                $this->addAmount($request,6,$request->total_amount_b2b); //opening b2b
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }

            else if($request->total_amount_cash!='' && $request->closing_cheque_payment!='' && $request->advance_collection_type!='' && $request->advance_collection_amount!=''  )
            {
                $this->addAmount($request,5,$request->total_amount_cash); //opening cash
                $this->addAmount($request,7,$request->closing_cheque_payment); //closing cheque collection
                $this->addAmount($request,$request->advance_collection_type,$request->advance_collection_amount);//advance collection
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }

            else if($request->total_amount_cash!='' && $request->closing_cheque_payment!='' )
            {
                $this->addAmount($request,5,$request->total_amount_cash); //opening cash
                $this->addAmount($request,7,$request->closing_cheque_payment); //closing cheque collection
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }

            else if($request->advance_collection_type!='' && $request->advance_collection_amount!='')
            {
                $this->addAmount($request,$request->advance_collection_type,$request->advance_collection_amount);//advance collection
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_cash!='')
            {
                $this->addAmount($request,5,$request->total_amount_cash); //opening cash
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }
            else if($request->closing_cheque_payment!='')
            {
                $this->addAmount($request,7,$request->closing_cheque_payment); //opening cash
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }
            else if($request->total_amount_b2b!='')
            {
                $this->addAmount($request,6,$request->total_amount_b2b); //opening b2b
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }else if($request->opening_due_collection!='')
            {
                $this->addAmount($request,7,$request->closing_cheque_payment); //closing cheque collection
                return redirect(route('accounts'))->with('closing_cash_success','Data stored successfully !');
            }

            return redirect(route('accounts'));
        }
    }

    public function addAmount($request,$type,$amount)
    {
        return DB::table('transactions')
            ->insertGetId([
                'total_amount'=>$amount,
                'user_id'=>(isset($request->dso_id)?$request->dso_id:0),
                'transaction_type'=>$type,
                'entry_date'=>(date('Y-m-d')),
                'is_final'=>$request->is_final,
                'is_deleted'=>0,
                'entry_user'=>Auth::user()->id,
                'edited_by'=>0,
                'created_at'=>date('Y-m-d h:i:s a'),
                'wallet_no'=>$request->wallet_no
            ]);
    }

    public function getPaymentSummery(Request $request)
    {
        return view('ajax-files.dso-overview')
            ->with([
                'opening_cash_given_amount'=>$this->getTodaysTotalOpeningPayment($request->dso_id,2),
                'opening_b2b_given_amount'=>$this->getTodaysTotalOpeningPayment($request->dso_id,3),
                'closing_cash_amount'=>$this->getTodaysTotalClosingPayment($request->dso_id,5),
                'closing_b2b_amount'=>$this->getTodaysTotalClosingPayment($request->dso_id,6),
                'closing_bank_amount'=>$this->getTodaysTotalClosingPayment($request->dso_id,7),
                'opening_due_collection_amount'=>$this->getTodaysDueCollection($request->dso_id),
                'previous_due'=>$this->getPreviousDue($request->dso_id),
                'total_due_collection'=>$this->getTotalDueCollection($request->dso_id),
                'expense_amount'=>$this->getTodaysExpesne($request->dso_id),
                'dso_total_advance'=>$this->getDSOTotalAdvanceCollectionAmount($request->dso_id,'<=',date('Y-m-d')),
                'total_expense'=>$this->getDSOTotalExpesne($request->dso_id)
            ]);
    }

    public function getTodaysTotalOpeningPayment($user,$type)
    {
        $amount=DB::table('transactions')
            ->where('entry_date',date('Y-m-d'))
            ->where('user_id',$user)
            ->where('transaction_type',$type)
            ->sum('total_amount');
        return $amount;
    }


    public function getTodaysTotalClosingPayment($user,$type)
    {
        $amount=DB::table('transactions')
            ->where('entry_date',date('Y-m-d'))
            ->where('user_id',$user)
            ->where('transaction_type',$type)
            ->sum('total_amount');
        return $amount;
    }


    public function getTotalDueCollection($user)
    {
        $opening_due_collection_amount=DB::table('transactions')
            ->where('entry_date','<=',date('Y-m-d'))
            ->where('user_id',$user)
            ->where('transaction_type',4)
            ->sum('total_amount');
        return $opening_due_collection_amount;
    }

    public function getTodaysDueCollection($user)
    {
        $opening_due_collection_amount=DB::table('transactions')
            ->where('entry_date',date('Y-m-d'))
            ->where('user_id',$user)
            ->where('transaction_type',4)
            ->sum('total_amount');
        return $opening_due_collection_amount;
    }

    public function getTodaysTotalDueCollection()
    {
        $opening_due_collection_amount=DB::table('transactions')
            ->where('entry_date',date('Y-m-d'))
            ->where('transaction_type',4)
            ->sum('total_amount');
        return $opening_due_collection_amount;
    }


    public function getTodaysTotalDSOExpesne()
    {
        $todays_total_dso_exp_amount=DB::table('expenses')
            ->where('entry_date','=',date('Y-m-d'))
            ->where('type',1)
            ->sum('total_amount');
        return $todays_total_dso_exp_amount;
    }


    public function getTodaysOtherExpesne()
    {
        $todays_total_exp_amount=DB::table('expenses')
            ->where('entry_date','=',date('Y-m-d'))
            ->where('type','=',0)
            ->sum('total_amount');
        return $todays_total_exp_amount;
    }

    public function getTodaysTotalExpesne()
    {
        $todays_total_exp_amount=DB::table('expenses')
            ->where('entry_date','=',date('Y-m-d'))
            ->where('type','=',1)
            ->sum('total_amount');
        return $todays_total_exp_amount;
    }

    public function getTotalExpesne($range)
    {
        $total_exp_amount=DB::table('expenses')
            ->where('entry_date',$range,date('Y-m-d'))
            ->where(function($query){
                $query
                    ->where('type',1)
                    ->orWhere('type',0);
            })

            ->sum('total_amount');
        return $total_exp_amount;
    }


    public function getTotalFixedAssets($range)
    {
        $total_exp_amount=DB::table('expenses')
            ->where('entry_date',$range,date('Y-m-d'))
            ->where('type',30)
            ->sum('total_amount');
        return $total_exp_amount;
    }


    public function getTodaysExpesne($user)
    {
        $todays_exp_amount=DB::table('expenses')
            ->where('entry_date','=',date('Y-m-d'))
            ->where('reference_person',$user)
            //->where('type',1)
            ->sum('total_amount');
        return $todays_exp_amount;
    }

    public function getDSOTotalExpesne($user)
    {
        $todays_exp_amount=DB::table('expenses')
            ->where('entry_date','<',date('Y-m-d'))
            ->where('reference_person',$user)
            ->where('type',1)
            ->sum('total_amount');
        return $todays_exp_amount;
    }


    public function getPreviousDue($user)
    {
        $previous_given_amount=DB::table('transactions')
            ->where('entry_date','<',date('Y-m-d'))
            ->where('user_id',$user)
            ->where(function($query){
                $query->where('transaction_type',2)
                    ->orWhere('transaction_type',3);
            })
            ->sum('total_amount');

        $previous_paid_amount=DB::table('transactions')
            ->where('entry_date','<',date('Y-m-d'))
            ->where('user_id',$user)
            ->where(function($query){
                $query->where('transaction_type',5)
                    ->orWhere('transaction_type',6)
                    ->orWhere('transaction_type',7);
            })
            ->sum('total_amount');

        $previous_due_collection=DB::table('transactions')
            ->where('entry_date','<',date('Y-m-d'))
            ->where('user_id',$user)
            ->where('transaction_type',4)
            ->sum('total_amount');

//        $previous_exp_amount=DB::table('expenses')
//            ->where('entry_date','<',date('Y-m-d'))
//            ->where('reference_person',$user)
//            ->where('type',1)
//            ->sum('total_amount');
        $previous_exp_amount=0;


        if(($previous_given_amount-$previous_exp_amount)==$previous_paid_amount)
        {
            return 0;
        }

        if(($previous_given_amount-$previous_exp_amount)>$previous_paid_amount)
        {
            return $previous_given_amount-($previous_paid_amount+$previous_exp_amount);
        }
        //if()
    }


    public function newInvest(Request $request)
    {

        $rows=DB::table('investments')->where('transaction_type',$request->transaction_type)->count();

        if($rows==0)
        {
            $date=date('Y-m-d',strtotime('-1 day'));
        }else
        {
            $date=date('Y-m-d');
        }


        $id=DB::table('transactions')->insertGetId([
            'total_amount'=>$request->amount,
            'user_id'=>0,
            'transaction_type'=>$request->transaction_type,
            'entry_date'=>$date,
            'is_final'=>1,
            'is_deleted'=>0,
            'entry_user'=>Auth::user()->id,
            'edited_by'=>0,
            'created_at'=>date('Y-m-d h:i:s a'),
            'wallet_no'=>0
        ]);
        DB::table('investments')
            ->insert([
                'title'=>$request->title,
                'total_amount'=>$request->amount,
                'transaction_type'=>$request->transaction_type,
                'entry_date'=>$date,
                'entry_date_time'=>$date.date("h:i:s a"),
                'entry_user'=>Auth::user()->id,
                'reference_no'=>$id
            ]);
        return redirect()->back()->with(['success'=>'Data stored successfully !!']);
    }


    public function addNewIncome(Request $request)
    {
        if(sizeof($request->income_type_id)>=1)
        {
//            $rows=DB::table('investments')->where('transaction_type',$request->transaction_type)->count();
//
//            if($rows==0)
//            {
//                $date=date('Y-m-d',strtotime('-1 day'));
//            }else
//            {
//                $date=date('Y-m-d');
//            }

            for ($i=0;$i<sizeof($request->income_type_id);$i++)
            {
                if($request->income_type_id[$i]!='' && $request->amount[$i]!='')
                {
                    $income_id=DB::table('incomes')
                        ->insertGetId([
                            'income_type_id'=>$request->income_type_id[$i],
                            'total_amount'=>$request->amount[$i],
                            'entry_user'=>1,
                            'entry_date'=>date('Y-m-d'),
                            'transaction_type'=>$request->transaction_type
                        ]);
                    $id=$this->addAmount($request,$request->transaction_type,$request->amount[$i]); //master wallet invest
                    DB::table('investments')
                        ->insert([
                            'title'=>"",
                            'total_amount'=>$request->amount[$i],
                            'transaction_type'=>$request->transaction_type,
                            'entry_date'=>date(''),
                            'entry_date_time'=>date("Y-m-d h:i:s a"),
                            'entry_user'=>Auth::user()->id,
                            'reference_no'=>$id,
                            'income_ref_id'=>$income_id
                        ]);

                }
            }
//            if($request->post_from=='account')
//            {
            return redirect(route('accounts'))->with(
                ['tab'=>'income','success'=>'Data stored successfully!']
            );
//            }else
//            {
//                return redirect(route('exp-lists'))->with(['success','Expense added successfully !','expenses',$this->getAllExpenses()]);
//            }
        }
    }


    public function addNewIncomeType(Request $request)
    {
        DB::table('income_types')->insert([
            'type_name'=>$request->type_name,
            'is_deleted'=>0
        ]);
        return redirect()->back()->with(['income-type-success'=>'Type added !!','tab'=>'income']);
    }

    public function removeType(Request $request)
    {
        DB::table('income_types')->where('income_type_id',$request->id)->delete();
    }


    public function getAllExpenseTypes()
    {
        return $types=DB::table('expense_types')->select('id','type_name')->get();
    }

    public function giveNewPersonalTransaction(Request $request)
    {
        DB::table('personal_transactions')->insert([
            'user_id'=>$request->user_id,
            'total_amount'=>$request->total_amount,
            'entry_date'=>date('Y-m-d'),
            'entry_date_time'=>date('Y-m-d h:i:s a'),
            'title'=>$request->title,
            'entry_user'=>Auth::user()->id,
            'remarks'=>$request->note,
            'type'=>$request->type
        ]);

        DB::table('transactions')
            ->insert([
                'total_amount'=>$request->total_amount,
                'user_id'=>$request->user_id,
                'transaction_type'=>$request->transaction_type,
                'entry_date'=>date('Y-m-d'),
                'is_final'=>1,
                'is_deleted'=>0,
                'entry_user'=>Auth::user()->id,
                'edited_by'=>0,
                'created_at'=>date('Y-m-d h:i:s a'),
                'wallet_no'=>0
            ]);

        return redirect()->back()->with(['success'=>'Data stored successfully !!']);
    }


    public function receiveNewPersonalTransaction(Request $request)
    {
        DB::table('personal_transactions')->insert([
            'user_id'=>$request->user_id,
            'total_amount'=>$request->total_amount,
            'entry_date'=>date('Y-m-d'),
            'entry_date_time'=>date('Y-m-d h:i:s a'),
            'title'=>$request->title,
            'entry_user'=>Auth::user()->id,
            'remarks'=>$request->note,
            'type'=>$request->type
        ]);

        DB::table('transactions')
            ->insert([
                'total_amount'=>$request->total_amount,
                'user_id'=>$request->user_id,
                'transaction_type'=>$request->transaction_type,
                'entry_date'=>date('Y-m-d'),
                'is_final'=>1,
                'is_deleted'=>0,
                'entry_user'=>Auth::user()->id,
                'edited_by'=>0,
                'created_at'=>date('Y-m-d h:i:s a'),
                'wallet_no'=>0
            ]);

        return redirect()->back()->with(['success'=>'Data stored successfully !!']);
    }


    public function getTotalAdvancePaymentGiven($range,$date)
    {
       return  $amount=DB::table('transactions')
            ->where('entry_date',$range,$date)
            ->where(function($query){
                $query->where('transaction_type',24)
                    ->orWhere('transaction_type',25)
                    ->orWhere('transaction_type',26);
            })->sum('total_amount');
    }

    public function getTotalAdvancePaymentReceived($range,$date)
    {
        return  $amount=DB::table('transactions')
            ->where('entry_date',$range,$date)
            ->where(function($query){
                $query->where('transaction_type',27)
                    ->orWhere('transaction_type',28)
                    ->orWhere('transaction_type',29);
            })->sum('total_amount');
    }

    public function getTotalMergedAdvanceAmount($range,$date)
    {
        return  $amount=DB::table('transactions')
            ->where('entry_date',$range,$date)
            ->where('transaction_type',40)
            ->sum('total_amount');
    }



    /**==========================================================================================**/
    /**============================bank transaction==============================================**/

    public function openingTransaction(Request $request)
    {

        if($request->cash!='' && $request->b2b) {
            DB::table('cash_monitor')->insert([
                'cash'=>$request->cash,
                'b2b'=>$request->b2b,
                'bank'=>0,
                'bank_advance'=>0,
                'bank_advance_return'=>0,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>0,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);
        }else if($request->cash!='')
        {
            DB::table('cash_monitor')->insert([
                'cash'=>$request->cash,
                'b2b'=>0,
                'bank'=>0,
                'bank_advance'=>0,
                'bank_advance_return'=>0,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>0,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);

        }else if($request->b2b!='')
        {
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>$request->b2b,
                'bank'=>0,
                'bank_advance'=>0,
                'bank_advance_return'=>0,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>0,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);

        }

        if($request->bank_withdraw!='' && $request->bank_withdraw_advance) {
            $withdraw_ref_id=$this->addAmount($request, 31, $request->bank_withdraw); //opening bank withdraw
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>0,
                'bank'=>$request->bank_withdraw,
                'bank_advance'=>0,
                'bank_advance_return'=>0,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id,
            ]);

            $withdraw_ref_id1=$this->addAmount($request, 32, $request->bank_withdraw_advance); //opening bank withdraw advance
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>0,
                'bank'=>0,
                'bank_advance'=>$request->bank_withdraw_advance,
                'bank_advance_return'=>0,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id1,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);
        }
        if($request->bank_withdraw!='')
        {
            $withdraw_ref_id=$this->addAmount($request,31,$request->bank_withdraw); //opening bank withdraw
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>0,
                'bank'=>$request->bank_withdraw,
                'bank_advance'=>0,
                'bank_advance_return'=>0,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);
        }

        if($request->bank_withdraw_advance!='')
        {
            $withdraw_ref_id=$this->addAmount($request,32,$request->bank_withdraw_advance); //opening bank withdraw advance
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>0,
                'bank'=>0,
                'bank_advance'=>$request->bank_withdraw_advance,
                'bank_advance_return'=>0,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);
        }


        if($request->bank_deposit_advance!='' && $request->bank_deposit_regular!='')
        {
            $withdraw_ref_id=$this->addAmount($request,33,$request->bank_deposit_advance); //opening bank withdraw advance return
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>0,
                'bank'=>0,
                'bank_advance'=>0,
                'bank_advance_return'=>$request->bank_deposit_advance,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id,
            ]);

            $withdraw_ref_id1=$this->addAmount($request,36,$request->bank_deposit_regular); //opening bank withdraw advance return
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>0,
                'bank'=>0,
                'bank_advance'=>0,
                'bank_advance_return'=>0,
                'bank_deposit'=>$request->bank_deposit_regular,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id1,
            ]);

            return redirect()->back()->with(['success'=>'Data stored successfully !!']);
        }


        if($request->bank_deposit_advance!='')
        {
            $withdraw_ref_id=$this->addAmount($request,33,$request->bank_deposit_advance); //opening bank withdraw advance return
            DB::table('cash_monitor')->insert([
                'cash'=>$request->cash,
                'b2b'=>$request->b2b,
                'bank'=>0,
                'bank_advance'=>0,
                'bank_advance_return'=>$request->bank_deposit_advance,
                'bank_deposit'=>0,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);
        }

        if($request->bank_deposit_regular!='')
        {
            $withdraw_ref_id=$this->addAmount($request,36,$request->bank_deposit_regular); //opening bank withdraw advance return
            DB::table('cash_monitor')->insert([
                'cash'=>0,
                'b2b'=>0,
                'bank'=>0,
                'bank_advance'=>0,
                'bank_advance_return'=>0,
                'bank_deposit'=>$request->bank_deposit_regular,
                'entry_date'=>date('Y-m-d'),
                'entry_user'=>Auth::user()->id,
                'transaction_ref_id'=>$withdraw_ref_id,
            ]);
            return redirect()->back()->with(['success'=>'Data stored successfully !!']);
        }


    }


    public function getCashMonitorAmount()
    {
        $ttl=0;
        $total_amount=DB::table('cash_monitor')
            ->where('entry_date',date('Y-m-d'))
            ->select('cash','b2b','bank')
            ->get();
        if(empty($total_amount))
        {
            return 0;
        }

        foreach ($total_amount as $total)
        {
            $ttl+=$total->cash+$total->b2b;
        }

        return $ttl;
    }


    public function getBankAmount($range,$type)
    {
        $amount=DB::table('transactions')
            ->where('entry_date',$range,date('Y-m-d'))
            ->where('transaction_type',$type)
            ->sum('total_amount');
        return $amount;
    }


    public function getTotalSalaryCost($date_range)
    {
        $total_amount=DB::table('salary')
            ->where('entry_date',$date_range,date('Y-m-d'))
            ->select('basic','phone_bill','ta_da','incentive','other_incentive')
            ->get();

        if(empty($total_amount))
        {
            return 0;
        }
        $total=0;
        foreach ($total_amount as $tam)
        {
            $total+=$tam->basic+$tam->phone_bill+$tam->ta_da+$tam->incentive+$tam->other_incentive;
        }
        return $total;
    }


    public function getAllDailyOpeningTransactionsByUser(Request $request)
    {
        $lists=DB::table('transactions')
            ->where('entry_date',$request->range,$request->date)
            ->where('user_id',$request->user)
            ->where(function($query){
                $query->where('transaction_type',2)
                    ->orWhere('transaction_type',3)
                    ->orWhere('transaction_type',37);
            })
            ->select('id','total_amount','created_at','transaction_type')
            ->orderby('created_at','desc')
            ->get();
        return view('ajax-files.transactions')->with('lists',$lists);
    }


    public function getAllDailyClosingransactionsByUser(Request $request)
    {
        $lists=DB::table('transactions')
            ->where('entry_date',$request->range,$request->date)
            ->where('user_id',$request->user)
            ->where(function($query){
                $query->where('transaction_type',5)
                    ->orWhere('transaction_type',6)
                    ->orWhere('transaction_type',7)
                    ->orWhere('transaction_type',4)
                    ->orWhere('transaction_type',20)
                    ->orWhere('transaction_type',21);
            })
            ->select('id','total_amount','created_at','transaction_type')
            ->orderby('created_at','desc')
            ->get();

        $expenses=DB::table('expenses')
            ->where('entry_date',$request->range,$request->date)
            ->where('reference_person',$request->user)
            ->select('id','total_amount','entry_date','transaction_type')
            ->orderby('created_at','desc')
            ->get();
        return view('ajax-files.closing-transaction')->with(['lists'=>$lists,'expenses'=>$expenses]);
    }

    public function getallInvestmentsToday(Request $request)
    {
        $lists=DB::table('transactions')
            ->where('entry_date',$request->range,$request->date)
            ->where(function($query){
                $query->where('transaction_type',1)
                    ->orWhere('transaction_type',11)
                    ->orWhere('transaction_type',22);
            })
            ->select('id','total_amount','created_at','transaction_type')
            ->orderby('created_at','desc')
            ->get();
        return view('ajax-files.invests-edit')->with('lists',$lists);
    }

    public function getallExpenseAmountGivenToday(Request $request)
    {
        $lists=DB::table('expenses')
            ->where('expenses.entry_date',$request->range,$request->date)
            ->select('expenses.id','total_amount','created_at','expenses.type','name')
            ->join('users','users.id','expenses.reference_person')
            ->orderby('created_at','desc')
            ->get();
        return view('ajax-files.expense-edit')->with('lists',$lists);
    }

    public function allReceivedPersonalTransaction(Request $request)
    {
        $lists=DB::table('transactions')
            ->where('entry_date',$request->range,$request->date)
            ->where(function($query){
                $query->where('transaction_type',27)
                    ->orWhere('transaction_type',28)
                    ->orWhere('transaction_type',29);
            })
            ->select('id','total_amount','created_at','transaction_type')
            ->orderby('created_at','desc')
            ->get();
        return view('ajax-files.amount-received')->with('lists',$lists);
    }

    public function allGivenPersonalTransaction(Request $request)
    {
        $lists=DB::table('transactions')
            ->where('entry_date',$request->range,$request->date)
            ->where(function($query){
                $query->where('transaction_type',24)
                    ->orWhere('transaction_type',25)
                    ->orWhere('transaction_type',26);
            })
            ->select('id','total_amount','created_at','transaction_type')
            ->orderby('created_at','desc')
            ->get();
        return view('ajax-files.amount-given')->with('lists',$lists);
    }


    public function getAllOpeningsToday(Request $request)
    {
        $lists=DB::table('cash_monitor')
            ->where('entry_date',$request->range,$request->date)
            ->select('cash_id','cash','b2b','bank','bank_advance','bank_advance_return','bank_deposit')
            ->orderby('cash_id','desc')
            ->get();
        return view('ajax-files.opening-cash-list')->with('lists',$lists);
    }

    public function updateAmount(Request $request)
    {
        if(isset($request->cash_monitor))
        {
            $ref_id=DB::table('cash_monitor')->where('cash_id',$request->id)->select('transaction_ref_id')->first();

            DB::table('cash_monitor')
                ->where('cash_id',$request->id)
                ->update([$request->name=>$request->total_amount]);
            DB::table('transactions')->where('id',$ref_id->transaction_ref_id)->update(['total_amount'=>$request->total_amount]);
        }else
        {
            DB::table('transactions')->where('id',$request->id)->update(['total_amount'=>$request->total_amount]);
        }

    }

    public function deleteRow(Request $request)
    {
        if(isset($request->cash_monitor))
        {
            $ref_id=DB::table('cash_monitor')->where('cash_id',$request->id)->select('transaction_ref_id')->first();
            DB::table('cash_monitor')
                ->where('cash_id',$request->id)->delete();
            DB::table('transactions')->where('id',$ref_id->transaction_ref_id)->delete();

        }else
        {

            DB::table('transactions')->where('id',$request->id)->delete();
        }
    }


    public function getUserTransactionDueAmount(Request $request)
    {
        $given=DB::table('transactions')
            ->where('user_id',$request->id)
            ->where(function($query){
                $query->where('transaction_type',24)
                    ->orWhere('transaction_type',25)
                    ->orWhere('transaction_type',26);
            })
            ->sum('total_amount');
        $return=DB::table('transactions')
            ->where('user_id',$request->id)
            ->where(function($query){
                $query->where('transaction_type',27)
                    ->orWhere('transaction_type',28)
                    ->orWhere('transaction_type',29);
            })
            ->sum('total_amount');
        $merged_amount_with_salary=DB::table('transactions')->where('user_id',$request->id)->where('transaction_type',40)->sum('total_Amount');
        return $given-($return+$merged_amount_with_salary);
    }

    public function incomeEdit(Request $request)
    {

        $types=DB::table('income_types')->select('income_type_id','type_name')->get();
        $list=DB::table('incomes')->where('income_id',$request->id)
            ->join('income_types','income_types.income_type_id','incomes.income_type_id')
            ->select('income_id','incomes.income_type_id','type_name','total_amount')
            ->first();
        return view('accounts.income-edit')->with(['list'=>$list,'types'=>$types]);
    }

    public function updateIncome(Request $request)
    {

        DB::table('incomes')->where('income_id',$request->row_id)->update([
            'income_type_id'=>$request->income_type_id,
            'total_amount'=>$request->total_amount
        ]);
        $transaction_ref_id=DB::table('investments')
            ->where('income_ref_id',$request->row_id)
            ->select('reference_no')->first();

        DB::table('investments')
            ->where('income_ref_id',$request->row_id)
            ->update(['total_amount'=>$request->total_amount]);

        DB::table('transactions')
            ->where('id',$transaction_ref_id->reference_no)
            ->update(['total_amount'=>$request->total_amount]);


        return redirect()->back()->with('success','Data successfully updated!!');
    }

    public function deleteIncome(Request $request)
    {
        DB::table('incomes')->where('income_id',$request->income_id)->delete();
        $transaction_ref_id=DB::table('investments')->where('income_ref_id',$request->income_id)->select('reference_no')->first();
        DB::table('investments')->where('income_ref_id',$request->income_id)->delete();
        DB::table('transactions')->where('id',$transaction_ref_id->reference_no)->delete();

        return redirect(route('accounts'))->with(['success'=>'Data removed updated!!','tab'=>'income']);
    }


}

