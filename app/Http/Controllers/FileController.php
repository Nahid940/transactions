<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    //

    public function add()
    {
        $files=DB::table('files')->select('id','title','path')->get();
        return view('file.add')->with('files',$files);
    }

    public function storeFile(Request $request)
    {
        for ($i=0;$i<sizeof($request->file('files'));$i++)
        {
           $path=Storage::put('public', $request->file('files')[$i]);

            $this->addFiles($path,$request->title);
        }

        return redirect()->back()->with('success','File uploaded successfully!');
    }

    public function addFiles($file_path,$title)
    {
        DB::table('files')->insert([
            'title'=>$title,
            'path'=>$file_path,
            'entry_user'=>1
        ]);
    }

    public function deletefile(Request $request)
    {
        Storage::delete($request->file_name);
        DB::table('files')->where('id',$request->id)->delete();
    }


    public function downloadFile($file)
    {
        $files = Storage::allFiles('/');

        dd($files);



        $path = storage_path($file);

        return response()->download($path);

         $afile = storage::disk('public')->get($file);

        return response()->download($afile);
    }

}


