@extends('layouts.app')

@section('content')

    <style>
        .income-table tr td{
            border: none !important;
            padding: 2px !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="panel-body" style="padding: 0">
                <div class="tabbable tab-content-bordered">
                    <ul class="nav nav-tabs " style="background: #b53e3f;">
                        <li class="active"><a style="color: #000" href="#colored-nav-tab1" data-toggle="tab" class="" aria-expanded="true">একাউন্টস</a></li>
                        <li class=""><a style="color: #000" href="#colored-nav-tab2" data-toggle="tab" class="" aria-expanded="false">আয় হিসাব</a></li>
                        <li class=""><a style="color: #000" href="#colored-nav-tab3" data-toggle="tab" class="" aria-expanded="false">স্যালারি হিসাব</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane has-padding {{(session('tab')!='income' && session('tab')!='salary' && !isset($_GET['tab']))?'active':''}} " id="colored-nav-tab1">
                            <div class="panel panel-flat my-panel" style="background: beige">
                                <div class="panel-body my-panel-body">
                                    <legend class="text-bold my-legend">একাউন্টস</legend>
                                    <div class="ml-10 text-info-800 mb-5 text-bold " style="font-size: 18px">
                                        <i class="icon-calendar22"></i> তারিখ: @php echo date('d-m-Y') @endphp
                                    </div>
                                    <div class="table-responsive">
                                        <div class="col-md-6">
                                            @php


                                                $total_opening_cash_transaction=$total_opening_transaction_at_hand;
                                                $total_closing_cash_transaction=$total_closing_transaction_at_hand;

                                                $total_opening_b2b_transaction=$total_opening_transaction_at_b2b;
                                                $total_closing_b2b_transaction=$total_closing_transaction_at_b2b;

                                                $total_closing_bank_transaction=$total_closing_transaction_at_bank;

                                                /**======================opening personal amount transaction=================**/
                                                $personal_opening_advance_payment_given_amount=$personal_advance_payment_given_open;
                                                $personal_opening_advance_payment_received_amount=$personal_advance_payment_received_open;
                                                $opening_transaction_difference=($personal_opening_advance_payment_given_amount-($personal_opening_advance_payment_received_amount+$total_advance_merged_with_salary));
                                                /**====================================================================**/

                                                /**======================opening personal amount transaction=================**/
                                                $personal_closing_advance_payment_given_amount=$personal_advance_payment_given_close;
                                                $personal_closing_advance_payment_received_amount=$personal_advance_payment_received_close;
                                                $closing_transaction_difference=$personal_closing_advance_payment_given_amount-($personal_closing_advance_payment_received_amount+$total_todays_advance_merged_with_salary);
                                                /**====================================================================**/

                                                //Opening amount calculation
                                                if($opening_cash_invest+$total_bank_withdraw==0)
                                                {
                                                    ///if($total_opening_cash_transaction==0)
                                                    //{
                                                        $test_total_hand=($total_closing_cash_transaction)-$total_cash_salary;
                                                    //}else
                                                    //{

                                                     //   $test_total_hand=($total_opening_cash_transaction-$total_closing_cash_transaction);
                                                    //}
                                                }else
                                                {

                                                    $test_total_hand=($opening_cash_invest+$total_bank_withdraw)-(($total_opening_cash_transaction-$total_closing_cash_transaction)+$total_expense+$total_cash_salary);
                                                }

                                                if($opening_b2b_invest==0)
                                                {
                                                    //if($total_opening_b2b_transaction==0)
                                                   // {
                                                        $test_total_b2b=$total_closing_b2b_transaction-$total_b2b_salary;
                                                    //}else
                                                   // {
                                                   //     $test_total_b2b=($total_opening_b2b_transaction-$total_closing_b2b_transaction);
                                                    //}
                                                }else
                                                {
                                                    $test_total_b2b=($opening_b2b_invest-($total_opening_b2b_transaction-$total_closing_b2b_transaction))-$total_b2b_salary;
                                                }


                                                if($opening_bank_invest==0)
                                                {
                                                    $test_total_bank=$total_closing_bank_transaction-$total_bank_withdraw;
                                                }else
                                                {
                                                    $test_total_bank=($opening_bank_invest+$total_closing_bank_transaction)-$total_bank_withdraw;
                                                    //$test_total_bank=$total_closing_bank_transaction-$total_bank_withdraw;
                                                }

                                                //if($total_bank_withdraw>$total_closing_bank_transaction)
                                                //{
                                                //    $test_total_bank=$total_bank_withdraw-$total_closing_bank_transaction;
                                                //}else
                                                //{
                                                //    $test_total_bank=$total_closing_bank_transaction-$total_bank_withdraw;
                                                //}

                                                $total_investment=$opening_cash_invest+$opening_b2b_invest+$opening_bank_invest;

                                                $opening_total=$test_total_hand+$test_total_b2b+$test_total_bank;

                                                //$todays_due_collection

                                                if($todays_due_collection!='')
                                                {
                                                    $closing_due=($opening_cash+$opening_b2b+$todays_due_collection)-($closing_cash+$closing_b2b+$closing_bank+$closing_cash_salary+$closing_b2b_salary);
                                                    //$closing_due=($opening_cash+$opening_b2b)-($closing_cash+$closing_b2b+$closing_bank+$closing_cash_salary+$closing_b2b_salary);
                                                }
                                                else if($opening_cash+$opening_b2b<$closing_cash+$closing_b2b+$closing_bank)
                                                {
                                                    $closing_due=($opening_cash+$opening_b2b+$todays_due_collection)-($closing_cash+$closing_b2b+$closing_bank+$closing_cash_salary+$closing_b2b_salary);
                                                }else
                                                {
                                                   //$closing_due=(($opening_cash+$opening_b2b)-$todays_total_dso_exp_amount)-($closing_cash+$closing_b2b+$closing_bank);
                                                   $closing_due=(($opening_cash+$opening_b2b))-($closing_cash+$closing_b2b+$closing_bank+$closing_cash_salary+$closing_b2b_salary);
                                                }

                                                //dd($total_closing_cash_transaction+$total_closing_bank_transaction+$total_closing_b2b_transaction+$total_due_collection);

                                                //$mtd=((($total_opening_cash_transaction+$total_opening_b2b_transaction))-($total_closing_cash_transaction+$total_closing_bank_transaction+$total_closing_b2b_transaction));
                                                $mtd=((($total_opening_cash_transaction+$total_opening_b2b_transaction))-($total_closing_cash_transaction+$total_closing_bank_transaction+$total_closing_b2b_transaction));

                                                $total_in_hand=$test_total_hand+$test_total_b2b+$test_total_bank;

                                                $closing_salary_amount=$closing_cash_salary+$closing_b2b_salary;
                                            @endphp

                                            <div class="panel panel-flat my-panel" style="border-radius:10px;background: #daecdf">
                                                <div class="panel-body my-panel-body">
                                                    <table class="table table-bordered mytable">
                                                        <tbody>
                                                        <tr>
                                                            <td  class="text-bold text-center"  colspan="2">ওপেনিং ব্যালেন্স</td>
                                                            <td  class="text-bold text-center"  colspan="2">ক্লোজিং ব্যালেন্স</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold">ওপেনিং ইনভেস্ট (MTD)</td>
                                                            <td>{{$total_invest}}</td>
                                                            <td class="text-bold">আজকের নতুন ইনভেস্ট</td>
                                                            <td>{{$todays_invest}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold">ওপেনিং ক্যাশ @ হ্যান্ড</td>
                                                            <td>{{($test_total_hand<0?0:$test_total_hand)}} </td>
                                                            <td class="text-bold">ক্লোজিং ক্যাশ @ হ্যান্ড</td>
{{--                                                            <td>{{$closing_cash-($todays_office_expense+$todays_total_dso_expense+$todays_bank_return)}} </td>--}}
                                                            @php
                                                                $closing_amount=$closing_cash-($todays_office_expense+$todays_total_dso_expense);
                                                            @endphp
                                                            <td>{{($closing_amount>=0?$closing_amount:0)}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold">ওপেনিং বিটুবি @ মাস্টার ওয়ালেট</td>
                                                            <td>{{$test_total_b2b<0?0:$test_total_b2b}}</td>
                                                            <td class="text-bold">ক্লোজিং বিটুবি @ মাস্টার ওয়ালেট</td>
                                                            <td>{{$closing_b2b<=0?0:$closing_b2b}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold">ওপেনিং ক্যাশ @ ব্যাঙ্ক</td>
                                                            <td>{{$test_total_bank<0?0:$test_total_bank}} </td>
                                                            <td class="text-bold">ক্লোজিং ক্যাশ @ ব্যাঙ্ক</td>
                                                            <td>{{$closing_bank}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold">ওপেনিং ফিক্সড অ্যাসেট (MTD)</td>
                                                            <td>{{$total_fixed_assets}}</td>
                                                            <td class="text-bold">ক্লোজিং ফিক্সড অ্যাসেট</td>
                                                            <td>{{$todays_fixed_assets}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold">ওপেনিং মার্কেট ডিউ (MTD)</td>
                                                            <td>{{$mtd}}</td>
                                                            <td class="text-bold">ক্লোজিং  বকেয়া</td>
                                                            <td>{{$closing_due}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold " style="color: #1f4a00">অ্যাডভান্স কালেকশন (MTD)</td>
                                                            <td class="text-green-800" style="color: #1f4a00">{{$total_advance_collection}}</td>
                                                            <td class="text-bold" style="color: #1f4a00">ক্লোজিং অ্যাডভান্স কালেকশন</td>
                                                            <td class="text-green-800" style="color: #1f4a00">{{$todays_advance_collection}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold text-danger-800">মোট খরচ (MTD)</td>
                                                            <td class="text-danger-800">{{$total_expense}}</td>
                                                            <td class="text-bold text-danger-800">ক্লোজিং অফিস খরচ</td>
                                                            <td class="text-danger-800">{{$todays_office_expense}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold text-danger-800">স্যালারি খরচ</td>
                                                            <td class="text-danger-800">{{$total_salary_cost}}</td>
                                                            <td class="text-bold text-danger-800">ক্লোজিং স্যালারি খরচ</td>
                                                            <td class="text-danger-800">{{$closing_salary_amount==''?0:$closing_salary_amount}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td class="text-bold text-danger-800">ক্লোজিং ডিএসও খরচ</td>
                                                            <td class="text-danger-800">{{$todays_total_dso_expense}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold text-danger-800">ওপেনিং ব্যক্তিগত খরচ</td>
                                                            <td class="text-danger-800">{{$opening_transaction_difference}}</td>
                                                            <td class="text-bold text-danger-800">ক্লোজিং ব্যক্তিগত খরচ</td>
                                                            <td class="text-danger-800">{{$closing_transaction_difference}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td>আজকের ব্যাঙ্ক অ্যাডভান্স </td>
                                                            <td>{{$todays_total_bank_advance}}</td>
                                                            <td class="text-bold">ক্লোজিং বকেয়া আদায়</td>
                                                            <td>{{$todays_due_collection}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-bold" style="">ওপেনিং টোটাল</td>
                                                            <td class="text-bold" style="text-align: left">{{(($total_investment+$total_advance_collection)+($total_bank_advance_withdraw-$total_bank_advance_return))-($total_expense+$total_salary_cost+$opening_transaction_difference)}} </td>
                                                            <td class="text-bold">ক্লোজিং টোটাল</td>
{{--                                                            <td class="text-bold" style="text-align: left">{{(($closing_cash+$closing_bank+$closing_b2b+$todays_advance_collection+$closing_due-$todays_bank_return))}}</td>--}}
                                                            <td class="text-bold" style="text-align: left">{{(($closing_cash+$closing_bank+$closing_b2b+$closing_due+$todays_advance_collection+$todays_invest+$closing_transaction_difference+$closing_salary_amount+$todays_fixed_assets))}}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-flat my-panel" style="height: 20%;border-radius:10px;background: #daecdf">
                                                <div class="panel-body my-panel-body">
                                                    <form class="form-horizontal" id="opening_transaction_form" method="post" action="{{route('opening-transaction')}}">
                                                        <input type="hidden" id="dso_id4" name="dso_id">
                                                        <input type="hidden" id="is_final4" name="is_final" value="0">
                                                        <input type="hidden" id="wallet_no14" name="wallet_no" value="">
                                                        {{csrf_field()}}
                                                        <fieldset class=" my-content-group">
                                                            <legend class="text-bold text-green-800 my-legend">ওপেনিং হিসাব </legend>
                                                            @if(session('opening_cash_success'))
                                                                <span class="text-success text-bold">{{session('opening_cash_success')}}</span>
                                                            @endif
                                                            <div class="form-group">
                                                                <label class="control-label my-label col-lg-4">ক্যাশ </label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control my-input" name="cash" placeholder="ক্যাশ ">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label my-label col-lg-4">বিটুবি</label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control my-input" name="b2b" placeholder="বিটুবি">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label my-label col-lg-4">ব্যাঙ্ক থেকে উত্তোলন</label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control my-input" name="bank_withdraw" placeholder="ব্যাঙ্ক থেকে উত্তোলন ">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label my-label col-lg-4">ব্যাঙ্ক  থেকে উত্তোলন (অ্যাডভান্স)</label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control my-input" name="bank_withdraw_advance" placeholder="ব্যাঙ্ক  থেকে উত্তোলন (অ্যাডভান্স) ">
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                        <div class="text-right">
                                                            <button type="button" class="btn btn-primary btn-sm legitRipple opening_transaction"> <i class="icon-check"></i>সেইভ</button>
                                                        </div>
                                                    </form>
                                                    @if($cash_monitor_amount!=0)
                                                        {{--<span class="text-bold text-center">এমাউন্ট ঘাটতি : {{($total_in_hand)-($cash_monitor_amount+$total_fixed_assets)}}</span>--}}
                                                        <span class="text-bold text-center" style="margin-top: 50px">এমাউন্ট ঘাটতি : {{($total_in_hand-$cash_monitor_amount)}}</span>
                                                    @else
                                                        <span style="margin-top: 50px">&nbsp;</span>
                                                    @endif

                                                    <form class="form-horizontal" id="opening_transaction_form_deposit" method="post" action="{{route('opening-transaction')}}">
                                                        <input type="hidden" id="dso_id4" name="dso_id">
                                                        <input type="hidden" id="is_final4" name="is_final" value="0">
                                                        <input type="hidden" id="wallet_no14" name="wallet_no" value="">
                                                        {{csrf_field()}}
                                                        <fieldset class=" my-content-group">
                                                            <legend class="text-bold text-danger-800 my-legend">ক্লোজিং হিসাব </legend>
                                                            <div class="form-group">
                                                                <label class="control-label my-label col-lg-4 ">ব্যাঙ্ক জমা (রেগুলার)</label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control my-input" name="bank_deposit_regular" placeholder="ব্যাঙ্ক জমা (রেগুলার) ">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label my-label col-lg-4">ব্যাঙ্ক জমা (অ্যাডভান্স)</label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control my-input" name="bank_deposit_advance" placeholder="ব্যাঙ্ক জমা (অ্যাডভান্স) ">
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                        <div class="text-right">
                                                            <a href="javascript:void(0)" title="Edit" data-toggle="modal" class="btn btn-sm btn-info opening_amount_input" data-target="#opening_amount_input_modal"><i class="icon-database-edit2"></i> এডিট</a>
                                                            <button type="button" class="btn btn-primary btn-sm legitRipple opening_transaction_deposit"> <i class="icon-check"></i>সেইভ</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">

                                                    <div class="panel panel-flat my-panel" style="height: 20%;">
                                                        <div class="panel-body my-panel-body">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <td class="text-bold">ডিএসও নাম</td>
                                                                    <td><select name="" class="form-control" id="dso_lists">
                                                                            <option value="">ডিএসও নির্বাচন করুন</option>
                                                                            @foreach ($dsos as $dso)
                                                                                <option value="{{$dso->id}}">{{$dso->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                    <td class="text-bold">ওয়ালেট নম্বর</td>
                                                                    <td>
                                                                        <select name="wallet_no" class="form-control" id="dso_wallet">
                                                                            <option value="">ওয়ালেট নম্বর নির্বাচন করুন</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#closing_list" class="btn btn-info btn-sm mt-10 pull-right  ml-5 closing_list_button hidden"><span class="name_of_dso"></span> এর সকল ক্লোজিং লেনদেন</a>
                                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#opening_list" class="btn btn-success btn-sm mt-10 pull-right list_button hidden"><span class="name_of_dso"></span> এর সকল ওপেনিং লেনদেন</a>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-md-6">
                                                    <div class="summery"></div>
                                                </div>


                                            </div>
                                            <div class="col-md-12 mt-10 mb-10" style="">
                                                <div class="text-center text-bold">
                                                    @if($errors)
                                                        @foreach($errors->all() as $v)
                                                            <span class="text-danger-800 error-text">{{$v}}</span>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="panel panel-flat my-panel" style="background: #e1ddff">
                                                        <div class="panel-body my-panel-body">
                                                            <form class="form-horizontal" id="opening_cash_distribute" method="post" action="{{route('process-opening-cash')}}">
                                                                <input type="hidden" id="dso_id" name="dso_id">
                                                                <input type="hidden" id="is_final" name="is_final" value="0">
                                                                <input type="hidden" id="wallet_no1" name="wallet_no" value="">
                                                                {{csrf_field()}}
                                                                <fieldset class=" my-content-group">
                                                                    <legend class="text-bold my-legend">ওপেনিং ব্যালেন্স প্রদান</legend>
                                                                    @if(session('opening_cash_success'))
                                                                        <span class="text-success text-bold">{{session('opening_cash_success')}}</span>
                                                                    @endif
                                                                    <div class="opening_error_div text-danger-800"></div>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-2">ক্যাশ </label>
                                                                        <div class="col-lg-10">
                                                                            <input type="number" class="form-control my-input" name="total_amount_cash" placeholder="ক্যাশ">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-2">বিটুবি</label>
                                                                        <div class="col-lg-10">
                                                                            <input type="number" class="form-control my-input" name="total_amount_b2b" placeholder="বিটুবি ">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-2">বকেয়া আদায় </label>
                                                                        <div class="col-lg-10">
                                                                            <input type="number" class="form-control my-input" name="opening_due_collection" placeholder="আজকের বকেয়া আদায়">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-2">অ্যাডভান্স প্রদান</label>
                                                                        <div class="col-lg-10">
                                                                            <input type="number" class="form-control my-input" name="advance_payment" placeholder="অ্যাডভান্স প্রদান ">
                                                                        </div>
                                                                    </div>
                                                                    {{--<div class="pull-right text-orange-800 text-bold" style="font-size: 18px">Total: 10000/-</div>--}}
                                                                </fieldset>
                                                                <div class="text-right">
                                                                    <button type="button" class="btn btn-primary btn-sm legitRipple opening_cash_distribute mb-10"> <i class="icon-check"></i>সেইভ</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="panel panel-flat my-panel" style="background: #e1ddff">
                                                        <div class="panel-body my-panel-body">
                                                            <form class="form-horizontal" id="closing_process_submit" method="post" action="{{route('process-closing-cash')}}">
                                                                {{csrf_field()}}
                                                                <input type="hidden" id="dso_id1" name="dso_id">
                                                                <input type="hidden" id="is_final" name="is_final" value="0">
                                                                <input type="hidden" id="wallet_no2" name="wallet_no">
                                                                <fieldset class="my-content-group">
                                                                    <legend class="text-bold my-legend">ক্লোজিং ব্যালেন্স হিসাব</legend>
                                                                    @if(session('closing_cash_success'))
                                                                        <span class="text-success text-bold">{{session('closing_cash_success')}}</span>
                                                                    @endif
                                                                    <span class="closing_error_div text-danger-800"></span>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-2">ক্যাশ</label>
                                                                        <div class="col-lg-10">
                                                                            <input type="number" class="form-control my-input" name="total_amount_cash" placeholder="ক্যাশ ">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-2">বিটুবি</label>
                                                                        <div class="col-lg-10">
                                                                            <input type="number" class="form-control my-input" name="total_amount_b2b" placeholder="বিটুবি">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-2">ব্যাঙ্ক পেমেন্ট </label>
                                                                        <div class="col-lg-10">
                                                                            <input type="number" class="form-control my-input" name="closing_cheque_payment" placeholder="ব্যাঙ্ক পেমেন্ট">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label my-label col-lg-3 ">অ্যাডভান্স কালেকশন </label>
                                                                        <div class="col-lg-4">
                                                                            <select name="advance_collection_type" id="" class="form-control">
                                                                                <option value="">কালেকশন এর ধরণ</option>
                                                                                <option value="20">ক্যাশ</option>
                                                                                <option value="21"> বিটুবি </option>
                                                                                {{--<option value="23">ব্যাঙ্ক</option>--}}
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-5">
                                                                            <input type="number" class="form-control my-input mt-5" name="advance_collection_amount" placeholder="অ্যাডভান্স কালেকশন এমাউন্ট">
                                                                        </div>
                                                                    </div>
                                                                    {{--<div class="form-group">--}}
                                                                    {{--<label class="control-label my-label col-lg-2">অ্যাডভান্স স্যালারি </label>--}}
                                                                    {{--<div class="col-lg-10">--}}
                                                                    {{--<input type="text" class="form-control my-input" name="advance_salary" placeholder="অ্যাডভান্স স্যালারি ">--}}
                                                                    {{--</div>--}}
                                                                    {{--</div>--}}
                                                                </fieldset>
                                                                <div class="text-right">
                                                                    <button type="button" class="btn btn-primary btn-sm legitRipple closing_process_submit"> <i class="icon-check"></i>সেইভ</button>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-10 mb-10" style="">
                                                <div class="col-sm-6">
                                                    <div class="panel panel-flat my-panel" style="background: #b8de2169">
                                                        <div class="panel-body my-panel-body">
                                                            <span class=" text-bold">নতুন ইনভেস্টমেন্ট</span>
                                                            <form class="form-horizontal" method="post" id="invest_amount_form" action="{{route('newInvest')}}">
                                                                {{csrf_field()}}
                                                                <div class="form-group mt-20">
                                                                    <div class="col-md-12" style="margin-bottom: 35px">
                                                                        {{--<div class="form-group">--}}
                                                                        {{--<input type="text" class="form-control myplaceholder" autocomplete="off" name="title" placeholder="ইনভেস্টমেন্ট হেড " >--}}
                                                                        {{--</div>--}}
                                                                        <div class="form-group col-md-6">
                                                                            <select name="transaction_type" class="form-control" id="" required>
                                                                                <option value="">ইনভেস্ট ধরণ </option>
                                                                                <option value="1">ক্যাশ ইনভেস্ট </option>
                                                                                <option value="11">বিটুবি ইনভেস্ট </option>
                                                                                <option value="22">ব্যাঙ্ক ইনভেস্ট </option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <input type="number" class="form-control myplaceholder" autocomplete="off" name="amount" placeholder="এমাউন্ট" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="text-right mt-40">
                                                                        <a href="javascript:void(0)" title="Edit investment history" data-target="#invest_edit_list" data-toggle="modal" class="invest_edit btn btn-info btn-sm mt-20"><i class="icon-database-edit2"></i> এডিট</a>
                                                                        <button type="button" class="btn btn-primary btn-sm legitRipple mt-20 invest_amount"><i class="icon-check "></i> সেইভ </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="panel panel-flat my-panel" style="background: #b8de2169">
                                                        <div class="panel-body my-panel-body">
                                                            @if(session('reference_person_error'))
                                                                <div class="alert alert-danger myalert">{{session('reference_person_error')}}</div>
                                                            @endif
                                                            @if(session('expense_success'))
                                                                <div class="alert alert-success myalert">{{session('expense_success')}}</div>
                                                            @endif
                                                            <span class="text-bold  ">নতুন খরচ</span>
                                                            <span class="expense_error_div text-danger-800"></span>
                                                            <form action="{{route('expense-enlist')}}" class="form-horizontal" id="expense_process_submit" method="post">
                                                                {{csrf_field()}}
                                                                {{--<input type="hidden" id="reference_person" name="reference_person">--}}
                                                                <input type="hidden" name="type" value="1">
                                                                {{--<input type="hidden" id="wallet_no" name="wallet_no">--}}
                                                                <input type="hidden" name="post_from" value="account">
                                                                <div class="panel-body my-panel-body">
                                                                    {{--<div class="form-group">--}}
                                                                    {{--<div class="col-md-12">--}}
                                                                    <fieldset class="my-content-group">
                                                                        <div class="form-group">
                                                                            <div class="col-md-6">
                                                                                <select name="reference_person" class="form-control" id="">
                                                                                    <option value="">ডিএসও নির্বাচন করুন</option>
                                                                                    @foreach ($dsos as $dso)
                                                                                        <option value="{{$dso->id}}">{{$dso->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                {{--<input type="text" class="form-control my-input myplaceholder" name="title" placeholder=" টাইটেল ">--}}
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <select name="expense_type[]" id="" class="form-control " required>
                                                                                    <option value="">খরচ হেড নির্বাচন</option>
                                                                                    <option value="10000">Fixed Asset</option>
                                                                                    @foreach($expense_types  as $expense_type)
                                                                                        <option value="{{$expense_type->id}}">{{$expense_type->type_name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group mt-5">
                                                                            {{--<label class="control-label my-label col-md-2 mt-10">খরচ হেড </label>--}}
                                                                            <div class="col-md-6">
                                                                                <input type="text" placeholder="খরচ  মেমো " name="memo" class="form-control my-input myplaceholder">
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <input type="number" class="form-control my-input myplaceholder" name="amount[]" placeholder=" এমাউন্ট " required>
                                                                            </div>
                                                                            {{--<div class="col-md-4">--}}
                                                                            {{--<input type="text" name="unit" placeholder="ইউনিট" class="form-control my-input  myplaceholder">--}}
                                                                            {{--</div>--}}
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                                <div class="text-right">
                                                                    <a href="javascript:void(0)" data-target="#expense-modal" data-toggle="modal" class="btn btn btn-sm btn-info given-expense-edit"><i class="icon-database-edit2"></i> এডিট</a>
                                                                    <button type="button" class="btn btn-primary btn-sm legitRipple expense_process_submit"> <i class="icon-check"></i>সেইভ</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-10 mb-10" style="">
                                                <div class="col-md-6 mt-10">
                                                    <div class="panel panel-flat my-panel" style="background: #317aff59">
                                                        <div class="panel-body my-panel-body">
                                                            <span class=" text-bold"> ব্যক্তিগত অর্থ গ্রহণ</span>
                                                            <span class="text-danger-800 text-bold error2"></span>
                                                            <form class="form-horizontal" id="personal_amount_form1" method="post" action="{{route('receive-amount')}}">
                                                                <input type="hidden" name="type" value="1">
                                                                {{csrf_field()}}
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group col-md-6">
                                                                            <select name="user_id" class="form-control" id="user_id1" required>
                                                                                <option value="">রেফারেন্স পারসন</option>
                                                                                @foreach($users as $user)
                                                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <select name="transaction_type" class="form-control" id="receive_type" required>
                                                                                <option value=""> ধরণ </option>
                                                                                <option value="27">ক্যাশ</option>
                                                                                <option value="28">বিটুবি</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <input type="number" class="form-control myplaceholder amount1"  autocomplete="off" name="total_amount" placeholder="এমাউন্ট" required>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <input type="text" class="form-control myplaceholder" autocomplete="off" name="remarks" placeholder="নোট" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="text-right mt-10">
                                                                        <a href="javascript:void(0)" data-target="#personal_amount_receive_modal" data-toggle="modal" class="btn btn-sm btn-info personal_amount_receive mt-20"><i class="icon-database-edit2"></i> এডিট</a>
                                                                        <button type="button" class="btn btn-primary btn-sm legitRipple mt-20 personal_amount1"><i class="icon-check "></i> সেইভ </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 has-padding mt-10" >
                                                    <div class="panel panel-flat my-panel" style="background: #317aff59">
                                                        <div class="panel-body my-panel-body">

                                                            <span class=" text-bold"> ব্যক্তিগত অর্থ প্রদান</span>
                                                            <span class="text-danger-800 text-bold error1"></span>
                                                            <form class="form-horizontal" id="personal_amount_form" method="post" action="{{route('give-amount')}}">
                                                                <input type="hidden" name="type" value="0">
                                                                {{csrf_field()}}
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group col-md-6">
                                                                            <select name="user_id" class="form-control" id="user_id2" required>
                                                                                <option value="">রেফারেন্স পারসন</option>
                                                                                @foreach($users as $user)
                                                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <select name="transaction_type" class="form-control" id="given_type" required>
                                                                                <option value="">ধরণ </option>
                                                                                <option value="24">ক্যাশ</option>
                                                                                <option value="25">বিটুবি</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <input type="number" class="form-control myplaceholder amount2" autocomplete="off" name="total_amount" placeholder="এমাউন্ট" required>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <input type="text" class="form-control myplaceholder" autocomplete="off" name="remarks" placeholder="নোট" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="text-right mt-10">
                                                                        <a href="javascript:void(0)" data-target="#personal_amount_given_modal" data-toggle="modal" class="btn btn-sm btn-info personal_amount_given mt-20"><i class="icon-database-edit2"></i> এডিট</a>
                                                                        <button type="button" class="btn btn-primary btn-sm legitRipple mt-20 personal_amount"><i class="icon-check "></i> সেইভ </button>
                                                                    </div>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="panel panel-flat">--}}
                                    {{--<div class="panel-heading">--}}
                                    {{--<h5 class="panel-title">নিজস্ব  লেনদেন <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>--}}
                                    {{--</div>--}}
                                    @if(session('transaction_success'))
                                        <div class="alert alert-success">{{session('transaction_success')}}</div>
                                    @endif
                                    <div class="row mt-10" style="border-top: .5px dashed #d2b0b0">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!---==================================end of account tab=======================================-->


                        <div class="tab-pane has-padding {{(session('tab')=='income')?'active':''}} {{(isset($_GET['tab']) && $_GET['tab']=='income')?'active':''}}" id="colored-nav-tab2">
                            <div class="panel panel-flat">
                                @if(session('error'))
                                    <div class="alert alert-danger error-text">{{session('error')}}</div>
                                @endif
                                <div class="panel-heading">
                                    <h5 class="panel-title">আয় হিসাব<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-8">
                                        @if(session('success'))
                                            <div class="alert alert-success">{{session('success')}}</div>
                                        @endif
                                        <form action="{{route('add-income')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="post_from" value="accounts">
                                            <input type="hidden" name="dso_id" value="0">
                                            <input type="hidden" name="is_final" value="0">
                                            <div class="table-responsive">
                                                {{--<a href="#" class="btn btn-info btn-sm pull-right add-new-income_type" title="Add New"><i class="icon-plus2"></i></a>--}}
                                                <table class="table income-table">
                                                    <thead>
                                                    <tr>
                                                        <th>আয়ের ধরণ</th>
                                                        <th>এমাউন্ট</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="income-table-rows">
                                                    <tr>
                                                        <td>
                                                            <select name="income_type_id[]" id="" class="form-control income-type-select" >
                                                                <option value="">আয়ের ধরণ তালিকা </option>
                                                                @foreach($income_types as $income_type)
                                                                    <option value="{{$income_type->income_type_id}}">{{$income_type->type_name}}</option>
                                                                @endforeach
                                                                <option value="-1">নতুন ধরণ যুক্ত করুন </option>
                                                            </select>
                                                        </td>
                                                        <td><input type="text" placeholder="এমাউন্ট" name="amount[]" class="form-control myplaceholder"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <select name="transaction_type" class="form-control" id="" required>
                                                                <option value=""> ধরণ</option>
                                                                <option value="1">ক্যাশ  </option>
                                                                <option value="11">বিটুবি </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20"> <i class="icon-check"></i>সেভ</button>
                                                </div>
                                            </div>
                                        </form>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table mb-20 mr-20">
                                                    <form action="" method="get">
                                                        <input type="hidden" name="tab" value="income">
                                                        <tr>
                                                            <td>শুরুর তারিখ <input type="date" name="start" class="form-control" required></td>
                                                            <td>শেষ তারিখ <input type="date" name="end" class="form-control" required></td>
                                                            <td>
                                                                আয়ের ধরণ
                                                                <select name="income_type_id" id="" class="form-control" required>
                                                                    <option value="">আয়ের ধরণ তালিকা </option>
                                                                    @foreach($income_types as $income_type)
                                                                        <option value="{{$income_type->income_type_id}}">{{$income_type->type_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="text-right ">
                                                                <input type="submit" class="btn btn-info btn-sm mt-20" value="Search">
                                                                <a href="{{url('accounts?tab=income')}}" class="btn btn-success btn-sm mt-20">Reset</a>
                                                            </td>
                                                        </tr>
                                                    </form>
                                                </table>
                                            </div>

                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>তারিখ</th>
                                                        <th>আয়ের ধরণ</th>
                                                        <th>টোটাল </th>
                                                        <th>অ্যাকশন</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $total=0;
                                                @endphp
                                                @foreach($incomes as $income)
                                                    <tr>
                                                        <td>{{date('d-m-Y',strtotime($income->entry_date))}}</td>
                                                        <td>{{$income->type_name}}</td>
                                                        <td>{{$income->total_amount}}</td>
                                                        <td>
                                                            <a href="{{route('income-edit',[$income->income_id])}}" class="btn btn-info btn-sm" title="এডিট করুন "><i class="icon-database-edit2"></i></a>
                                                            <a href="javascript:void(0)" class="btn btn-danger btn-sm delete-income" data-attr="{{$income->income_id}}" title="রেকর্ড মুছে ফেলুন"><i class="icon-database-remove"></i></a>
                                                            <form method="post" action="{{route('delete-income')}}" id="income-delete{{$income->income_id}}">
                                                                <input type="hidden" name="income_id" value="{{$income->income_id}}">
                                                                {{csrf_field()}}
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $total+=$income->total_amount
                                                    @endphp
                                                @endforeach
                                                <tr>
                                                    <td colspan="9" class="text-right text-bold">সর্বমোট {{$total}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center mt-20">
                                            {{$salaries->appends(['tab' => 'income'])->render()}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 add-new-type hidden">
                                        <div class="row">
                                            <form action="{{route('addNewIncomeType')}}" method="post">
                                                <h5>নতুন আয়ের ধরণ </h5>
                                                {{csrf_field()}}
                                                <div class="panel-body">
                                                    <div class="col-md-12">
                                                        @if(session('income-type-success'))
                                                            <div class="alert alert-success">{{session('income-type-success')}}</div>
                                                        @endif
                                                        <input type="text" placeholder="Type Name" name="type_name" class="form-control myplaceholder" required>
                                                        <div class="text-right">
                                                            <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20"> <i class="icon-check"></i>সেভ</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <table class="table">
                                                <h5>আয়ের ধরণ তালিকা </h5>
                                                <thead>
                                                    <tr>
                                                        <th>ক্রমিক</th>
                                                        <th>আয়ের ধরণ</th>
                                                        <th>অ্যাকশন</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i=1;
                                                @endphp
                                                @foreach($income_types as $income_type)
                                                    <tr>
                                                        <td>{{$i++}}</td>
                                                        <td>{{$income_type->type_name}}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" class="remove_income_type" data-id="{{$income_type->income_type_id}}" title="Delete"><i class="icon-trash text-danger-700"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane has-padding {{(session('tab')=='salary')?'active':''}} {{(isset($_GET['tab']) && $_GET['tab']=='salary')?'active':''}}" id="colored-nav-tab3">
                            <div class="panel panel-flat">
                                @if(session('salary_error'))
                                    {{--@foreach(session('error') as $err)--}}
                                    <div class="alert alert-danger">{{session('error')}}</div>
                                    {{--@endforeach--}}
                                @endif

                                <div class="panel-heading">
                                    <h5 class="panel-title">স্যালারি <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                                </div>
                                <div class="panel-body">
                                    <form action="{{route('add-salary')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="post_from" value="accounts">
                                        <div class="col-md-12 mb-20">
                                            @if(session('success'))
                                                <div class="alert alert-success">{{session('success')}}</div>
                                            @endif
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody class="income-table-rows">
                                                    <tr>
                                                        <td>
                                                            <select name="user_id" id="staff_salary" class="form-control" required>
                                                                <option value="">স্টাফ তালিকা</option>
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td style="width: 100px">
                                                            <select name="transaction_type" id="" class="form-control" required>
                                                                <option value="2">বিটুবি</option>
                                                                <option value="1">ক্যাশ</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="বেসিক" name="basic" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="ফোন বিল" name="phone_bill" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="টিএ/ডিএ" name="ta_da" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="ইন্সেন্টিভ " name="incentive" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="অন্যান্য ইন্সেন্টিভ" name="other_incentive" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="hidden"  name="others" class="form-control myplaceholder others" >
                                                            <input type="text" readonly placeholder="রিমার্ক " class="form-control myplaceholder others_remark" >
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20"> <i class="icon-check"></i>সেভ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="col-md-6">

                                        <div class="text-center mt-20">
                                            {{$salaries->appends(['tab' => 'salary'])->render()}}
                                        </div>
                                    </div>

                                    <div class="col-md-12 ">
                                        <div class="col-md-8">
                                            <table class="table mb-20">
                                                <form action="" method="get">
                                                    <input type="hidden" name="tab" value="salary">
                                                    <tr>
                                                        <td>শুরুর তারিখ <input type="date" name="start" class="form-control" required></td>
                                                        <td>শেষ তারিখ <input type="date" name="end" class="form-control" required></td>
                                                        <td style="width:100px">
                                                            স্টাফ
                                                            <select name="user" id="" class="form-control" required>
                                                                <option value="">Select</option>
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="text-right ">
                                                            <input type="submit" class="btn btn-info btn-sm mt-10" value="Search">
                                                            <a href="{{url('accounts?tab=salary')}}" class="btn btn-success btn-sm mt-10">Reset</a>
                                                        </td>
                                                    </tr>
                                                </form>
                                            </table>
                                        </div>


                                        <div class="row" style="background: beige;">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>স্টাফ</th>
                                                        <th>তারিখ</th>
                                                        <th>মাস</th>
                                                        <th>বেসিক</th>
                                                        <th>ফোন বিল</th>
                                                        <th>টিএ/ডিএ</th>
                                                        <th>ইন্সেন্টিভ</th>
                                                        <th>অন্যান্য ইন্সেন্টিভ </th>
                                                        <th>টোটাল </th>
                                                        <th>অ্যাকশন </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $total=0;
                                                @endphp
                                                @foreach($salaries as $salary)
                                                    <tr>
                                                        <td>{{$salary->name}}</td>
                                                        <td>{{date('d-m-Y',strtotime($salary->entry_date))}}</td>
                                                        <td>{{date('M',strtotime($salary->entry_month))}}</td>
                                                        <td>{{$salary->basic}}</td>
                                                        <td>{{$salary->phone_bill}}</td>
                                                        <td>{{$salary->ta_da}}</td>
                                                        <td>{{$salary->incentive}}</td>
                                                        <td>{{$salary->other_incentive}}</td>
                                                        <td>
                                                            {{$salary->basic+
                                                            $salary->phone_bill
                                                            +$salary->ta_da
                                                            +$salary->incentive
                                                            +$salary->other_incentive}}
                                                        </td>
                                                        <td>
                                                            @if($salary->entry_date==date('Y-m-d'))
                                                                <a href="{{route('edit-salary',[$salary->id])}}" class="btn btn-sm btn-success" title="এডিট করুন "> <i class="icon-database-edit2"></i></a>
                                                                <a href="javascript:void(0)" data-id="{{$salary->id}}" class="btn btn-sm btn-danger delete-btn" title="রেকর্ড মুছে ফেলুন"><i class="icon-database-remove"></i></a>
                                                                <form method="post" action="{{route('delete-salary',[$salary->id])}}" id="salary-delete-form{{$salary->id}}">
                                                                    {{csrf_field()}}
                                                                </form>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $total+=$salary->basic+
                                                        $salary->phone_bill
                                                        +$salary->ta_da
                                                        +$salary->incentive
                                                        +$salary->other_incentive;
                                                    @endphp
                                                @endforeach
                                                <tr>
                                                    <td colspan="9" class="text-right text-bold">সর্বমোট {{$total}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{--</div>--}}
        </div>
    </div>

    <div id="opening_list" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"> <span class="name_of_dso"></span> এর সকল ওপেনিং লেনদেন তালিকা</h5>
                        <span class="text-green-800 success-msg1 text-bold"></span>
                    </div>
                    <div class="modal-body mb-10">
                        <div class="form-element0">

                        </div>

                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="closing_list" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><span class="name_of_dso"></span> এর সকল ক্লোজিং লেনদেন তালিকা</h5>
                        <span class="text-green-800 success-msg2 text-bold"></span>
                    </div>
                    <div class="modal-body mb-10">
                        <div class="form-element1">

                        </div>

                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="invest_edit_list" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">ইনভেস্টমেন্ট রেকর্ড এডিট </h5>
                        <span class="text-green-800 success-msg2 text-bold text-center"></span>
                    </div>
                    <div class="modal-body mb-10">
                        <div class="form-element2">

                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="opening_amount_input_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">ওপেনিং এমাউন্ট এডিট</h5>
                        <span class="text-green-800 success-msg2 text-bold text-center"></span>
                    </div>
                    <div class="modal-body mb-10">
                        <div class="form-element3">

                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="personal_amount_receive_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">ব্যক্তিগত অর্থ গ্রহণ তালিকা</h5>
                        <span class="text-green-800 success-msg2 text-bold text-center"></span>
                    </div>
                    <div class="modal-body mb-10">
                        <div class="personal_amount_receive_form1">

                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="personal_amount_given_modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">ব্যক্তিগত অর্থ প্রদান তালিকা</h5>
                        <span class="text-green-800 success-msg2 text-bold text-center"></span>
                    </div>
                    <div class="modal-body mb-10">
                        <div class="personal_amount_receive_form2">

                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="expense-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">খরচ তালিকা এডিট করুন </h5>
                        <span class="text-green-800 success-msg2 text-bold text-center"></span>
                    </div>
                    <div class="modal-body mb-10">
                        <div class="given-expense-edit-list">

                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('script')
    <script>
        $(document).ready(function () {
            setTimeout(function(){
                $('.text-success').addClass('hidden');
                $('.error-text').addClass('hidden');
            }, 4000);
        })


        var dso_name='';
        var dso_id=0;

        $('#dso_lists').on('change',function () {
            if($(this).val()!='')
            {
                dso_name=$var = jQuery("#dso_lists option:selected").text();
                $('.name_of_dso').text(dso_name)
                $('.list_button').removeClass('hidden')
                $('.closing_list_button').removeClass('hidden')

                $('#dso_id').val($('#dso_lists').val())
                $('#dso_id1').val($('#dso_lists').val())
                $('#reference_person').val($('#dso_lists').val())

                dso_id=$(this).val()
                $.ajax({
                    type:'post',
                    url:'{{route('get-wallet-no')}}',
                    data:{
                        'id':$(this).val()
                    },
                    success:function (data) {
                        $('#dso_wallet').html(data)
                    }
                })
            }
            else
            {
                $('.list_button').addClass('hidden')
                $('.closing_list_button').addClass('hidden')
            }
        })


        $('#dso_lists').on('change',function () {
            if($(this).val()!='')
            {
                $.ajax({
                    type:'post',
                    url:'{{route('payment-summery')}}',
                    data:{
                        'dso_id':$(this).val()
                    },
                    success:function (data) {
                        $('.summery').html(data)
                    }
                })
            }
        })

        var i=0;
        $('.add-new').on('click',function(){
            i++;
            $('.table-rows').append('' +

                '<tr id="remove'+i+'">' +
                '<div style="display:flex;flex-direction:row" id="remove'+i+'">'+
                '<td>' +
                '<select id="" class="form-control" name="expense_type[]">'+
                '<option value="">খরচ নির্বাচন</option>'+
                    @foreach($expense_types  as $expense_type)
                        '<option value="{{$expense_type->id}}">{{$expense_type->type_name}}</option>'+
                    @endforeach
                        '</select>'+
                '</td>'+'<td><input type="text" placeholder="এমাউন্ট" name="amount[]" class="form-control"></td><a class="btn btn-danger btn-sm">-</a></div>'+

                '</tr>');
        })

        var j=0;
        $('.add-new-income_type').on('click',function(){
            j++;
            $('.income-table-rows').append('' +
                '<tr id="remove'+j+'">' +
                '<td>' +
                '<select id="" class="form-control" name="income_type_id[]">'+
                '<option value="">আয়ের ধরণ তালিকা</option>'+
                    @foreach($income_types as $income_type)
                        '<option value="{{$income_type->income_type_id}}">{{$income_type->type_name}}</option>'+
                    @endforeach
                        '</select>'+
                '</td>'+'<td><input type="text" placeholder="এমাউন্ট" name="amount[]" class="form-control"></td>'+
                '</tr>');
        })

        $('#dso_wallet').on('change',function () {
            $('#wallet_no').val($(this).val())
            $('#wallet_no1').val($(this).val())
            $('#wallet_no2').val($(this).val())
        })


        $('.remove_income_type').on('click',function () {
            var r = confirm("Do you want to remove this type? ");
            if (r == true) {
                $.ajax({
                    type:'post',
                    url:'{{route('removeIncomeType')}}',
                    data:{
                        id:$(this).attr('data-id')
                    },
                    success:function () {
                        setTimeout(function () {
                            alert("Data removed successfully !!");
                            window.location.reload()
                        },300)
                    }
                })
            } else {
                txt = "You pressed Cancel!";
            }
        })


        $('.opening_cash_distribute').on('click',function () {

            if($('#dso_id').val()=='')
            {
                $('.opening_error_div').text("Select DSO !!");
            }else if($('#wallet_no1').val()=='')
            {
                $('.opening_error_div').text("Select Wallet No. !!");
            }else
            {
                var r = confirm("Do you want to proceed? ");
                if (r == true) {
                    $('#opening_cash_distribute').submit()
                } else {
                    txt = "You pressed Cancel!";
                }
            }

        })

        $('.closing_process_submit').on('click',function () {

            if($('#dso_id1').val()=='')
            {
                $('.closing_error_div').text("Select DSO !!");
            }else if($('#wallet_no2').val()=='')
            {
                $('.closing_error_div').text("Select Wallet No. !!");
            }else {
                var r = confirm("Do you want to proceed? ");
                if (r == true) {
                    $('#closing_process_submit').submit()
                } else {
                    txt = "You pressed Cancel!";
                }
            }


        })

        $('.expense_process_submit').on('click',function () {

            if($('#reference_person').val()=='')
            {
                $('.expense_error_div').text("Select DSO !!");
            }else if($('#wallet_no').val()=='')
            {
                $('.expense_error_div').text("Select Wallet No. !!");
            }else {

                var r = confirm("Do you want to proceed? ");
                if (r == true) {
                    $('#expense_process_submit').submit()
                } else {
                    txt = "You pressed Cancel!";
                }
            }


        })

        $('.opening_transaction').on('click',function () {
            var r = confirm("Do you want to proceed? ");
            if (r == true) {
                $('#opening_transaction_form').submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })


        $('.opening_transaction_deposit').on('click',function () {
            var r = confirm("Do you want to proceed? ");
            if (r == true) {
                $('#opening_transaction_form_deposit').submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })

        $('.personal_amount').on('click',function () {

            if($('#user_id2').val()=='')
            {
                $('.error1').text("Select Reference User!!");
            }
            else if($('#given_type').val()=='')
            {
                $('.error1').text("Select Type!");
            }
            else if($('.amount').val()=='')
            {
                $('.error1').text("Enter Amount!");
            }
            else
            {
                var r = confirm("Do you want to proceed? ");
                if (r == true) {
                    $('#personal_amount_form').submit()
                } else {
                    txt = "You pressed Cancel!";
                }
            }
        })


        $('.personal_amount1').on('click',function () {

            if($('#user_id1').val()=='')
            {
                $('.error2').text("Select Reference User!!");
            }
            else if($('#receive_type').val()=='')
            {
                $('.error2').text("Select Type!");
            }
            else if($('.amount1').val()=='')
            {
                $('.error2').text("Enter Amount!");
            }
            else
            {
                var r = confirm("Do you want to proceed? ");
                if (r == true) {
                    $('#personal_amount_form1').submit()
                } else {
                    txt = "You pressed Cancel!";
                }
            }
        })

        $('.invest_amount').on('click',function () {
            var r = confirm("Do you want to proceed? ");
            if (r == true) {
                $('#invest_amount_form').submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })

        $('.delete-btn').on('click',function () {
            var r = confirm("Do you want to remove this ? ");
            if (r == true) {
                $('#salary-delete-form'+$(this).attr('data-id')).submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })

        $('.delete-income').on('click',function () {
            var r = confirm("Do you want to remove this ? ");
            if (r == true) {
                $('#income-delete'+$(this).attr('data-attr')).submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })


        $('.list_button').on('click',function () {
            if(dso_id!='')
            {
                $.ajax({
                    type:'post',
                    url:"{{route('get-all-opening-transaction')}}",
                    data:{
                        'user':dso_id,
                        'range':'=',
                        'date':"{{date('Y-m-d')}}"
                    },
                    success:function (data) {
                        $('.form-element0').html(data)
                    }
                })
            }
        })


        $('.closing_list_button').on('click',function () {
            if(dso_id!='')
            {
                $.ajax({
                    type:'post',
                    url:"{{route('get-all-closing-transaction')}}",
                    data:{
                        'user':dso_id,
                        'range':'=',
                        'date':"{{date('Y-m-d')}}"
                    },
                    success:function (data) {
                        $('.form-element1').html(data)
                    }
                })
            }
        })

        $('.invest_edit').on('click',function () {
            $.ajax({
                type:'post',
                url:"{{route('get-all-investments-today')}}",
                data:{
                    'range':'>=',
                    'date':"{{date('Y-m-d',strtotime('-1 day'))}}"
                },
                success:function (data) {
                    $('.form-element2').html(data)
                }
            })
        })

        $('.opening_amount_input').on('click',function () {
            $.ajax({
                type:'post',
                url:"{{route('get-all-openings-today')}}",
                data:{
                    'range':'=',
                    'date':"{{date('Y-m-d')}}"
                },
                success:function (data) {
                    $('.form-element3').html(data)
                }
            })
        })


        $('.personal_amount_receive').on('click',function () {
            $.ajax({
                type:'post',
                url:"{{route('get-all-received-amounts')}}",
                data:{
                    'range':'=',
                    'date':"{{date('Y-m-d')}}"
                },
                success:function (data) {
                    $('.personal_amount_receive_form1').html(data)
                }
            })
        })

        $('.personal_amount_given').on('click',function () {
            $.ajax({
                type:'post',
                url:"{{route('get-all-given-amounts')}}",
                data:{
                    'range':'=',
                    'date':"{{date('Y-m-d')}}"
                },
                success:function (data) {
                    $('.personal_amount_receive_form2').html(data)
                }
            })
        })

        $('.given-expense-edit').on('click',function () {
            $.ajax({
                type:'post',
                url:"{{route('get-all-expense-given-today')}}",
                data:{
                    'range':'=',
                    'date':"{{date('Y-m-d')}}"
                },
                success:function (data) {
                    $('.given-expense-edit-list').html(data)
                }
            })
        })

        $('#staff_salary').on('click',function () {
            if($(this).val()!='')
            {
                $.ajax({
                    type:'post',
                    url:'{{route('get-user-transaction-due-amount')}}',
                    data:{
                        'id':$(this).val()
                    },
                    success:function (data) {
                        $('.others_remark').val(data+ " অ্যাডভান্স ")
                        $('.others').val(data)
                    }
                })
            }
        })


        $('.income-type-select').on('click',function () {
            if($(this).val()==-1)
            {
                $('.add-new-type').removeClass('hidden')
            }else
            {
                $('.add-new-type').addClass('hidden')
            }
        })

    </script>
@endsection