@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel-body" style="padding: 0">
                <div class="tabbable tab-content-bordered">
                    <!---==================================end of account tab=======================================-->
                    <div class="tab-pane has-padding active" id="colored-nav-tab3">
                        <div class="panel panel-flat">
                            @if(session('salary_error'))
                                {{--@foreach(session('error') as $err)--}}
                                <div class="alert alert-danger">{{session('error')}}</div>
                                {{--@endforeach--}}
                            @endif
                            <div class="panel-heading">
                                <h5 class="panel-title">ইনকাম ডাটা এডিট করুন <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                            </div>
                            <div class="panel-body">
                                <form action="{{route('update-income')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="row_id" value="{{$list->income_id}}">
                                    <div class="col-md-12 mb-20">
                                        @if(session('success'))
                                            <div class="alert alert-success">{{session('success')}}</div>
                                        @endif
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>ধরণ </th>
                                                        <th>টোটাল এমাউন্ট </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="income-table-rows">
                                                    <tr>
                                                        <td>
                                                            <select name="income_type_id" id="" class="form-control" required>
                                                                <option value="">ইনকাম ধরণ সিলেক্ট করুন</option>
                                                                @foreach($types as $type)
                                                                    <option value="{{$type->income_type_id}}" {{($type->income_type_id==$list->income_type_id?'selected':'')}}>{{$type->type_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="টোটাল এমাউন্ট" name="total_amount" value="{{$list->total_amount}}" class="form-control myplaceholder" required>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20"> <i class="icon-check"></i>সেভ</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{--</div>--}}
    </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection