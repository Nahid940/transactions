@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body" style="padding: 0">
                <div class="tabbable tab-content-bordered">
                        <!---==================================end of account tab=======================================-->
                        <div class="tab-pane has-padding active" id="colored-nav-tab3">
                            <div class="panel panel-flat">
                                @if(session('salary_error'))
                                    {{--@foreach(session('error') as $err)--}}
                                    <div class="alert alert-danger">{{session('error')}}</div>
                                    {{--@endforeach--}}
                                @endif
                                <div class="panel-heading">
                                    <h5 class="panel-title">স্যালারি এডিট করুন  <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                                </div>
                                <div class="panel-body">
                                    <form action="{{route('update-salary')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="row_id" value="{{$id}}">
                                        <div class="col-md-12 mb-20">
                                            @if(session('success'))
                                                <div class="alert alert-success">{{session('success')}}</div>
                                            @endif
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>স্টাফ নেইম </th>
                                                            <th>বেসিক</th>
                                                            <th>ফোন বিল</th>
                                                            <th>টিএ/ডিএ</th>
                                                            <th>ইন্সেন্টিভ</th>
                                                            <th>অন্যান্য ইন্সেন্টিভ</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="income-table-rows">
                                                    <tr>
                                                        <td>
                                                            <select name="user_id" id="staff_salary" class="form-control" required>
                                                                <option value="">স্টাফ তালিকা</option>
                                                                @foreach($users as $user)
                                                                    <option value="{{$user->id}}" {{($list->user_id==$user->id?'selected':'')}}>{{$user->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="বেসিক" name="basic" value="{{$list->basic}}" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="ফোন বিল" name="phone_bill" value="{{$list->phone_bill}}" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="টিএ/ডিএ" name="ta_da" value="{{$list->ta_da}}" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="ইন্সেন্টিভ " name="incentive" value="{{$list->incentive}}" class="form-control myplaceholder" required>
                                                        </td>
                                                        <td>
                                                            <input type="text" placeholder="অন্যান্য ইন্সেন্টিভ" value="{{$list->other_incentive}}" name="other_incentive" class="form-control myplaceholder" required>
                                                        </td>
                                                        {{--<td>--}}
                                                            {{--<input type="hidden"  name="others" class="form-control myplaceholder others" >--}}
                                                            {{--<input type="text" readonly placeholder="রিমার্ক " class="form-control myplaceholder others_remark" >--}}
                                                        {{--</td>--}}
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20"> <i class="icon-check"></i>সেভ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection