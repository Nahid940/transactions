@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-12 control-label"><strong><i class="icon-check"></i>টার্গেট তালিকা</strong></label>
                    <div class="col-md-8  col-md-offset-2">
                        <div class="row" style="margin-bottom: 10px">
                            <form action="{{route('target-list')}}" method="get">
                                <div class="col-md-6" style="padding-left: 0">
                                    <select name="month" id="" class="form-control">
                                        <option value="">মাস </option>
                                        <option value="1" {{date('m')==1?'selected':''}}>জানুয়ারী</option>
                                        <option value="2" {{date('m')==2?'selected':''}}>ফেব্রুয়ারী</option>
                                        <option value="3" {{date('m')==3?'selected':''}}>মার্চ</option>
                                        <option value="4" {{date('m')==4?'selected':''}}>এপ্রিল</option>
                                        <option value="5" {{date('m')==5?'selected':''}}>মে</option>
                                        <option value="6" {{date('m')==6?'selected':''}}>জুন</option>
                                        <option value="7" {{date('m')==7?'selected':''}}>জুলাই</option>
                                        <option value="8" {{date('m')==8?'selected':''}}>আগস্ট</option>
                                        <option value="9" {{date('m')==9?'selected':''}}>সেপ্টেম্বর</option>
                                        <option value="10" {{date('m')==10?'selected':''}}>অক্টোবর</option>
                                        <option value="11" {{date('m')==11?'selected':''}}>নভেম্বর</option>
                                        <option value="12" {{date('m')==12?'selected':''}}>ডিসেম্বর</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </div>
                            </form>
                        </div>


                        <table class="table">
                            @if(sizeof($lists)==0)
                                <div class="alert alert-danger">No Data Found For the Month {{date('F',strtotime(request('month')))}}</div>
                            @else
                                <h5>মাস: {{date('F')}}</h5>
                                @foreach($lists as $list)
                                    <tr>
                                        <td colspan="2">DSO Name : <strong>{{$list->name}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">Transaction Target:</td>
                                        <td style="text-align: right">{{$list->transaction_target}}</td>
                                    </tr >
                                    <tr>
                                        <td style="text-align: right">Registration Target:</td>
                                        <td style="text-align: right">{{$list->registration_target}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">Strike Target:</td>
                                        <td style="text-align: right">{{$list->strike_target}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">Minimum Balance:</td>
                                        <td style="text-align: right">{{$list->minimum_balance}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="background-color: #0D47A1">&nbsp;</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection