@extends('layouts.app')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-12 control-label"><strong><i class="icon-check"></i> {{date('F')}} মাসের নতুন টার্গেট সেট করুন  </strong></label>
                    <div class="col-md-8 col-md-offset-2">
                        @if(session('target-add-success'))
                            <div class="alert alert-success">
                                {{session('target-add-success')}}
                            </div>
                        @endif
                            @if(session('target-add-error'))
                            <div class="alert alert-danger">
                                {{session('target-add-error')}}
                            </div>
                        @endif
                        <form class="form-horizontal" method="post" action="{{route('kpi-add')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ডিএসও নাম </label>
                                            <select name="dso_id" class="form-control" id="dso_lists" required>
                                                <option value="">ডিএসও নির্বাচন করুন</option>
                                                @foreach ($dsos as $dso)
                                                    <option value="{{$dso->id}}">{{$dso->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ওয়ালেট নম্বর </label>
                                            <select name="wallet_no" class="form-control" id="dso_wallet" required>
                                                <option value="">ওয়ালেট নম্বর নির্বাচন করুন</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ট্রান্সাকশন টার্গেট </label>
                                            <input type="text" name="transaction_target" class="form-control" placeholder="ট্রান্সাকশন টার্গেট">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">রেজিস্ট্রেশন টার্গেট </label>
                                            <input type="text" name="registration_target" class="form-control" placeholder="রেজিস্ট্রেশন টার্গেট">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">স্ট্রাইক টার্গেট </label>
                                            <input type="text" name="strike_target" class="form-control" placeholder="স্ট্রাইক টার্গেট">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">মিনিমাম ব্যালেন্স  </label>
                                            <input type="text" name="minimum_balance" class="form-control" placeholder="মিনিমাম ব্যালেন্স ">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>

        $('#dso_lists').on('change',function () {
            if($(this).val()!='')
            {
                dso_name=$var = jQuery("#dso_lists option:selected").text();
                $('.name_of_dso').text(dso_name)
                $('.list_button').removeClass('hidden')
                $('.closing_list_button').removeClass('hidden')

                $('#dso_id').val($('#dso_lists').val())
                $('#dso_id1').val($('#dso_lists').val())
                $('#reference_person').val($('#dso_lists').val())

                dso_id=$(this).val()
                $.ajax({
                    type:'post',
                    url:'{{route('get-wallet-no')}}',
                    data:{
                        'id':$(this).val()
                    },
                    success:function (data) {
                        $('#dso_wallet').html(data)
                    }
                })
            }
            else
            {
                $('.list_button').addClass('hidden')
                $('.closing_list_button').addClass('hidden')
            }
        })

    </script>
@endsection