@extends('layouts.app')
@section('content')

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-12 control-label"><strong><i class="icon-check"></i>ডেইলি অ্যাচিভমেন্ট তালিকা</strong></label>
                    <div class="col-md-8  col-md-offset-2">
                        <div class="row" style="margin-bottom: 10px">
                            <form action="{{route('daily-target-achievement-list')}}" method="get">
                                <div class="col-md-6" style="padding-left: 0">
                                    <input type="date" value="{{date('Y-m-d')}}" name="date" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </div>
                            </form>
                        </div>

                        <table class="table">
                            @if(sizeof($lists)==0)
                                <div class="alert alert-danger">No Data Found For the Date</div>
                            @else
                                <h5>তারিখ : {{request('date')?date('d-m-Y',strtotime(request('date'))):date('d-m-Y')}}</h5>
                                @foreach($lists as $list)
                                    <tr>
                                        <td colspan="2">DSO Name : <strong>{{$list->name}}</strong><span class="pull-right">
                                                <a href="{{route('daily-target-achievement-edit',$list->achievementID)}}" class="" title="Edit"><i class="icon-pen"></i></a>
                                                <a class="delete" data-id="{{$list->achievementID}}" title="Delete" ><i class="icon-trash text-danger-700"></i></a>
                                                <form action="{{route('daily-target-achievement-delete')}}" id="del_form{{$list->achievementID}}" method="post">
                                                    <input type="hidden" name="id" value="{{$list->achievementID}}">
                                                </form>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">Transaction Target:</td>
                                        <td style="text-align: right">{{$list->transaction}}</td>
                                    </tr >
                                    <tr>
                                        <td style="text-align: right">Registration Target:</td>
                                        <td style="text-align: right">{{$list->registration}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">Strike Target:</td>
                                        <td style="text-align: right">{{$list->strike}}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right">Minimum Balance:</td>
                                        <td style="text-align: right">{{$list->minimum_balance}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="background-color: #0D47A1">&nbsp;</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('.delete').on('click',function () {
            var val=$(this).attr('data-id')

            var r = confirm("Do you want to delete? ");
            if (r == true) {
                $('#del_form'+val).submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })
    </script>
@endsection