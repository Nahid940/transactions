@extends('layouts.app')
@section('content')

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <a href="{{route('daily-target-achievement-list')}}" class="btn btn-primary btn-sm pull-right">View List</a>
                    <label class="col-lg-12 control-label"><strong><i class="icon-check"></i> ডেইলি টার্গেট অ্যাচিভমেন্ট (এডিট করুন) </strong></label>
                    <div class="col-md-8 col-md-offset-2">
                        @if(session('edit-success'))
                            <div class="alert alert-success">
                                {{session('edit-success')}}
                            </div>
                        @endif
                            <form class="form-horizontal" method="post" action="{{route('daily-target-achievement-update')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="achievementID" value="{{request('id')}}">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ডিএসও নাম </label>
                                            <select name="dso_id" class="form-control" id="dso_lists" required>
                                                <option value="">ডিএসও নির্বাচন করুন</option>
                                                @foreach ($dsos as $dso)
                                                    <option value="{{$dso->id}}" {{$list->dso_id==$dso->id?'selected':''}}>{{$dso->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ওয়ালেট নম্বর </label>
                                            <select name="wallet_no" class="form-control" id="dso_wallet" required>
                                                <option value="">ওয়ালেট নম্বর নির্বাচন করুন</option>
                                                <option value="{{$list->wallet_no}}" selected>{{$list->wallet_no}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ট্রান্সাকশন এমাউন্ট </label>
                                            <input type="text" name="transaction" value="{{$list->transaction}}" class="form-control" placeholder="ট্রান্সাকশন এমাউন্ট">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">টোটাল রেজিস্ট্রেশন </label>
                                            <input type="text" name="registration"  value="{{$list->registration}}" class="form-control" placeholder="টোটাল রেজিস্ট্রেশন ">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">টোটাল স্ট্রাইক </label>
                                            <input type="text" name="strike" value="{{$list->strike}}" class="form-control" placeholder="টোটাল স্ট্রাইক ">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">মিনিমাম ব্যালেন্স এমাউন্ট</label>
                                            <input type="text" name="minimum_balance" value="{{$list->minimum_balance}}" class="form-control" placeholder="মিনিমাম ব্যালেন্স এমাউন">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">তারিখ</label>
                                            <input type="date" name="entry_date" value="{{$list->entry_date}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $('#dso_lists').on('change',function () {
            if($(this).val()!='')
            {
                dso_name=$var = jQuery("#dso_lists option:selected").text();
                $('.name_of_dso').text(dso_name)
                $('.list_button').removeClass('hidden')
                $('.closing_list_button').removeClass('hidden')

                $('#dso_id').val($('#dso_lists').val())
                $('#dso_id1').val($('#dso_lists').val())
                $('#reference_person').val($('#dso_lists').val())

                dso_id=$(this).val()
                $.ajax({
                    type:'post',
                    url:'{{route('get-wallet-no')}}',
                    data:{
                        'id':$(this).val()
                    },
                    success:function (data) {
                        $('#dso_wallet').html(data)
                    }
                })
            }
            else
            {
                $('.list_button').addClass('hidden')
                $('.closing_list_button').addClass('hidden')
            }
        })
    </script>
@endsection