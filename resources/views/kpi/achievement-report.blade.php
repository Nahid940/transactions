@extends('layouts.app')
@section('content')
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-12 control-label"><strong><i class="icon-check"></i> অ্যাচিভমেন্ট রিপোর্ট </strong></label>
                    <div class="col-md-8 col-md-offset-2">
                        @if(session('edit-success'))
                            <div class="alert alert-success">
                                {{session('edit-success')}}
                            </div>
                        @endif
                        <form class="form-horizontal" method="get" target="_blank" action="{{route('daily-target-achievement-report')}}">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ডিএসও নাম </label>
                                            <select name="dso_id" class="form-control" id="dso_lists" required>
                                                <option value="">ডিএসও নির্বাচন করুন</option>
                                                @foreach ($dsos as $dso)
                                                    <option value="{{$dso->id}}">{{$dso->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">মাস</label>
                                            <select name="target_month" class="form-control" id="" required>
                                                <option value="">মাস নির্বাচন করুন</option>
                                                <option value="1"{{date('m')==1?'selected':''}}>January</option>
                                                <option value="2" {{date('m')==2?'selected':''}}>February</option>
                                                <option value="3" {{date('m')==3?'selected':''}}>March</option>
                                                <option value="4" {{date('m')==4?'selected':''}}>April</option>
                                                <option value="5" {{date('m')==5?'selected':''}}>May</option>
                                                <option value="6" {{date('m')==6?'selected':''}}>June</option>
                                                <option value="7" {{date('m')==7?'selected':''}}>July</option>
                                                <option value="8" {{date('m')==8?'selected':''}}>August</option>
                                                <option value="9" {{date('m')==9?'selected':''}}>September</option>
                                                <option value="10" {{date('m')==10?'selected':''}}>October</option>
                                                <option value="11" {{date('m')==11?'selected':''}}>November</option>
                                                <option value="12" {{date('m')==12?'selected':''}}>December</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        {{--$('#dso_lists').on('change',function () {--}}
            {{--if($(this).val()!='')--}}
            {{--{--}}
                {{--dso_name=$var = jQuery("#dso_lists option:selected").text();--}}
                {{--$('.name_of_dso').text(dso_name)--}}
                {{--$('.list_button').removeClass('hidden')--}}
                {{--$('.closing_list_button').removeClass('hidden')--}}

                {{--$('#dso_id').val($('#dso_lists').val())--}}
                {{--$('#dso_id1').val($('#dso_lists').val())--}}
                {{--$('#reference_person').val($('#dso_lists').val())--}}

                {{--dso_id=$(this).val()--}}
                {{--$.ajax({--}}
                    {{--type:'post',--}}
                    {{--url:'{{route('get-wallet-no')}}',--}}
                    {{--data:{--}}
                        {{--'id':$(this).val()--}}
                    {{--},--}}
                    {{--success:function (data) {--}}
                        {{--$('#dso_wallet').html(data)--}}
                    {{--}--}}
                {{--})--}}
            {{--}--}}
            {{--else--}}
            {{--{--}}
                {{--$('.list_button').addClass('hidden')--}}
                {{--$('.closing_list_button').addClass('hidden')--}}
            {{--}--}}
        {{--})--}}
    </script>
@endsection