<table class="table">
    <thead>
        <tr>
            <th>ক্যাশ</th>
            <th>বিটুবি</th>
            <th>ব্যাঙ্ক</th>
            <th>ব্যাঙ্ক (অ্যাডভান্স) </th>
            <th>ব্যাঙ্ক (অ্যাডভান্স রিটার্ন) </th>
            <th>ব্যাঙ্ক (ডিপোজিট) </th>
            <th>রিমুভ </th>
        </tr>
    </thead>
    <tbody>
    @foreach($lists as $list)
        <tr>
            <td><input type="number" class="form-control open_amount_input_field" name="cash" data-id="{{$list->cash_id}}" value="{{$list->cash}}"></td>
            <td><input type="number" class="form-control open_amount_input_field" name="b2b" data-id="{{$list->cash_id}}" value="{{$list->b2b}}"></td>
            <td><input type="number" class="form-control open_amount_input_field" name="bank" data-id="{{$list->cash_id}}" value="{{$list->bank}}"></td>
            <td><input type="number" class="form-control open_amount_input_field" name="bank_advance" data-id="{{$list->cash_id}}" value="{{$list->bank_advance}}"></td>
            <td><input type="number" class="form-control open_amount_input_field" name="bank_advance_return" data-id="{{$list->cash_id}}" value="{{$list->bank_advance_return}}"></td>
            <td><input type="number" class="form-control open_amount_input_field"  name="bank_deposit" data-id="{{$list->cash_id}}" value="{{$list->bank_deposit}}"></td>
            <td><a href="javascript:void(0)" class="btn btn-sm btn-danger remove-btn" title="Danger" data-id="{{$list->cash_id}}">X</a></td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $('.open_amount_input_field').on('keyup',function () {
        if($(this).val()!='') {
            $.ajax({
                type:'post',
                url:'{{route('update_total_opening_amount')}}',
                data:{
                    'id':$(this).attr('data-id'),
                    'total_amount':$(this).val(),
                    'cash_monitor':1,
                    'name':$(this).attr('name')
                },
                success:function () {
                    $('.success-msg2').text('Record updated !')
                }
            })
        }
    })

    $('.remove-btn').on('click',function () {
        var r = confirm("Do you want to remove this record? ");
        if (r == true) {

            $.ajax({
                type:'post',
                url:'{{route('delete_row')}}',
                data:{
                    'id':$(this).attr('data-id'),
                    'cash_monitor':1
                },
                success:function () {
                    window.location.reload()
                }
            })

        } else {
            txt = "You pressed Cancel!";
        }
    })
</script>