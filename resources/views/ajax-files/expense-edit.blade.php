<table class="table">
    <thead>
        <tr>
            <th>তারিখ</th>
            <th>ডিএসও নেইম</th>
            <th>ধরণ</th>
            <th>এমাউন্ট</th>
            <th>রিমুভ </th>
        </tr>
    </thead>
    <tbody>
    @foreach($lists as $list)
        <tr>
            <td>{{date('d-m-Y h:i:s a',strtotime($list->created_at))}}</td>
            <td>{{$list->name}}</td>
            <td>
                @if($list->type==1)
                    ক্যাশ
                @elseif($list->type==2)
                    বিটুবি
                @else
                    ব্যাঙ্ক
                @endif
            </td>
            <td><input type="number" class="form-control closing_amount_input_field" data-id="{{$list->id}}" value="{{$list->total_amount}}"></td>
            <td><a href="javascript:void(0)" class="btn btn-sm btn-danger remove-btn" title="Danger" data-id="{{$list->id}}">X</a></td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $('.closing_amount_input_field').on('keyup',function () {
        if($(this).val()!='') {
            $.ajax({
                type:'post',
                url:'{{route('updateAmount')}}',
                data:{
                    'id':$(this).attr('data-id'),
                    'total_amount':$(this).val()
                },
                success:function () {
                    $('.success-msg2').text('Record updated !')
                }
            })
        }
    })

    $('.remove-btn').on('click',function () {
        var r = confirm("Do you want to remove this record? ");
        if (r == true) {

            $.ajax({
                type:'post',
                url:'{{route('deleteAmount')}}',
                data:{
                    'id':$(this).attr('data-id'),
                },
                success:function () {
                    window.location.reload()
                }
            })

        } else {
            txt = "You pressed Cancel!";
        }
    })
</script>