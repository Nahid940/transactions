<div class="col-md-12">
    <input type="hidden" name="id" value="{{$dso->id}}">
    <div class="form-group">
        <input type="text" class="form-control myplaceholder" value="{{$dso->name}}" autocomplete="off" name="name" placeholder="ডিনাম " required>
        {{--<input type="text" class="form-control myplaceholder" value="{{$dso->username}}" autocomplete="off" name="username" placeholder="ইউজার নেইম ">--}}
    </div>
    <div class="form-group">
        <input type="text" class="form-control myplaceholder" value="{{$dso->phone}}" autocomplete="off" name="contact" placeholder="ফোন নম্বর">
    </div>
    <div class="form-group new-wallet">

        <div class="more_wallet">
            <a href="javascript:void(0)" class="btn btn-sm btn-success addmore mt-20 pull-right">+</a>
        @if(!empty($dso->wallets ) && sizeof($dso->wallets )>=1)
        @foreach($dso->wallets as $ds)
            <input type="text" class="form-control myplaceholder" autocomplete="off" value="{{$ds->wallet_no}}"  name="wallet_no[]" placeholder="Wallet No.">
        @endforeach
        @else
            <input type="text" class="form-control myplaceholder" autocomplete="off"   name="wallet_no[]" placeholder="Wallet No.">
        @endif

        </div>

    </div>
</div>

<script>
    var i=0;

    $('.addmore').on('click',function () {
        i++;
       $('.more_wallet').append('<p id="extra_remove'+i+'"><input type="text"  class="form-control" autocomplete="off"  name="wallet_no[]" placeholder="Wallet No."> <a href="javascript:void(0)" title="Remove" data-code="'+i+'" class="btn btn-danger btn-sm pull-right remove-extra-input">X</a></p>');
    })

    $(document).on('click','.remove-extra-input',function () {

        $('#extra_remove'+$(this).attr('data-code')).remove()
    })

</script>