<table class="table">
    <thead>
        <tr>
            <th>তারিখ</th>
            <th>ধরণ</th>
            <th>এমাউন্ট</th>
            <th>রিমুভ </th>
        </tr>
    </thead>
    <tbody>
    @foreach($lists as $list)
        <tr>
            <td>{{date('d-m-Y h:i:s a',strtotime($list->created_at))}}</td>
            <td>
                @if($list->transaction_type==5)
                    ক্যাশ
                @elseif($list->transaction_type==6)
                    বিটুবি
                @elseif($list->transaction_type==7)
                    ব্যাঙ্ক
                @elseif($list->transaction_type==4)
                    আজকের বকেয়া আদায়
                @elseif($list->transaction_type==20)
                        অ্যাডভান্স (ক্যাশ)
                @elseif($list->transaction_type==21)
                        অ্যাডভান্স (বিটুবি)
                @endif
            </td>
            <td><input type="number" class="form-control closing_amount_input_field" data-id="{{$list->id}}" value="{{$list->total_amount}}"></td>
            <td><a href="javascript:void(0)" class="btn btn-sm btn-danger remove-btn" title="Danger" data-id="{{$list->id}}">X</a></td>
        </tr>
    @endforeach

    @foreach($expenses as $expense)
        <tr>
            <td>{{date('d-m-Y h:i:s a',strtotime($expense->entry_date))}}</td>
            <td>
                @if($expense->transaction_type==1)
                    খরচ(ক্যাশ)
                @elseif($expense->transaction_type==2)
                    খরচ(বিটুবি)
                @endif
            </td>
            <td><input type="number" class="form-control closing_expense_amount_input_field" data-id="{{$expense->id}}" value="{{$expense->total_amount}}"></td>
            <td><a href="javascript:void(0)" class="btn btn-sm btn-danger expense_remove-btn" title="Danger" data-id="{{$expense->id}}">X</a></td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $('.closing_amount_input_field').on('keyup',function () {
        if($(this).val()!='') {
            $.ajax({
                type:'post',
                url:'{{route('update_total_opening_amount')}}',
                data:{
                    'id':$(this).attr('data-id'),
                    'total_amount':$(this).val()
                },
                success:function () {
                    $('.success-msg2').text('Record updated !')
                }
            })
        }
    })

    $('.closing_expense_amount_input_field').on('keyup',function () {
        if($(this).val()!='') {
            $.ajax({
                type:'post',
                url:'{{route('update_total_opening_expense_amount')}}',
                data:{
                    'id':$(this).attr('data-id'),
                    'total_amount':$(this).val()
                },
                success:function () {
                    $('.success-msg2').text('Record updated !')
                }
            })
        }
    })

    $('.remove-btn').on('click',function () {
        var r = confirm("Do you want to remove this record? ");
        if (r == true) {

            $.ajax({
                type:'post',
                url:'{{route('delete_row')}}',
                data:{
                    'id':$(this).attr('data-id'),
                },
                success:function () {
                    window.location.reload()
                }
            })

        } else {
            txt = "You pressed Cancel!";
        }
    })

    $('.expense_remove-btn').on('click',function () {
        var r = confirm("Do you want to remove this record? ");
        if (r == true) {
            $.ajax({
                type:'post',
                url:'{{route('delete_expense_row')}}',
                data:{
                    'id':$(this).attr('data-id'),
                },
                success:function () {
                    window.location.reload()
                }
            })

        } else {
            txt = "You pressed Cancel!";
        }
    })



</script>