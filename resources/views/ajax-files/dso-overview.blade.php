
<div class="panel panel-flat my-panel" style="height: 20%;background: #44987d47;padding-right: 5px">
    <div class="panel-body my-panel-body">

        <table class="table text-bold mytable">
            <p class="text-dark"><strong>ওভারভিউ</strong></p>
            <tbody>
                <tr>

                    @php
                        $opening_given_amount=$opening_cash_given_amount+$opening_b2b_given_amount;
                        $opening_closing_amount=$closing_b2b_amount+$closing_cash_amount;
                    @endphp

                    <td>আজকের মোট ক্যাশ প্রদান (ওপেনিং)</td>
                    <td>{{$opening_cash_given_amount}}</td>

                    <td>আজকের মোট ক্যাশ (ক্লোজিং)</td>
                    <td>{{$closing_cash_amount}}</td>
                    {{--<td> {{$opening_closing_amount}}</td>--}}
                </tr>
                <tr>
                    <td>আজকের মোট বিটুবি প্রদান (ওপেনিং)</td>
                    <td>{{$opening_b2b_given_amount}}</td>
                    <td>আজকের মোট বিটুবি (ক্লোজিং)</td>
                    <td>{{$closing_b2b_amount}}</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td>আজকের মোট ব্যাঙ্ক (ক্লোজিং)</td>
                    <td>{{$closing_bank_amount}}</td>
                </tr>
                @php
                    if($previous_due==0)
                    {
                        $due=0;
                    }else
                    {
                        //$due=($previous_due-$total_due_collection);
                        if($opening_due_collection_amount!='')
                        {
                            $due=($previous_due-$opening_due_collection_amount);
                        }else
                        {
                            $due=$previous_due;
                        }
                    }
                    if($opening_due_collection_amount!='')
                    {
                        //$current_due=($opening_given_amount-$opening_closing_amount);
                        $current_due=($opening_given_amount-$opening_closing_amount)+$opening_due_collection_amount;
                    }else if($opening_given_amount<$opening_closing_amount && $opening_due_collection_amount='')
                    {
                        $current_due=($opening_given_amount-$opening_closing_amount);

                    }else if($opening_given_amount>$opening_closing_amount)
                    {
                        //$current_due=($opening_given_amount-$opening_closing_amount)-$expense_amount;
                        $current_due=($opening_given_amount-$opening_closing_amount);
                    }
                    else
                    {
                        //$current_due=($opening_given_amount-$opening_closing_amount)-$expense_amount;
                        $current_due=($opening_given_amount-$opening_closing_amount);
                    }
                @endphp
                <tr>
                    <td>আজকের মোট বকেয়া কালেকশন</td>
                    <td>{{$opening_due_collection_amount}}</td>
                    <td>গতদিনের বকেয়া </td>
                    <td>{{$due}}</td>
                </tr>
                <tr>
                    <td>টোটাল</td>
                    <td>{{$opening_due_collection_amount+$opening_b2b_given_amount+$opening_cash_given_amount}}</td>
                    <td>আজকের বকেয়া</td>
                    <td>{{$current_due}}</td>
                </tr>
                <tr>
                    <td>আজকের খরচ</td>
                    <td>{{$expense_amount}}</td>
                    <td>বকেয়া (MTD)</td>
                    <td>{{$due+$current_due}}</td>
                </tr>
                <tr>
                    <td>অ্যাডভান্স (MTD)</td>
                    <td>{{$dso_total_advance}}</td>
                    <td> খরচ (MTD)</td>
                    <td>{{$total_expense}}</td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
