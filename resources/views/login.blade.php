@extends('layouts.app')

@section('content')
    <style>
        .form-control{
            border: 1px solid #cecece;
        }
        .form-control-feedback{
            padding: 2px !important;
        }
    </style>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="{{route('login')}}" method="post">
                {{csrf_field()}}
                <div class="panel panel-body login-form">
                    @if(session('error'))
                        <div class="alert alert-danger">{{session('error')}}</div>
                    @endif
                    <div class="text-center">
                        <h5 class="content-group">Login</h5>
                    </div>
                    <div class="form-group has-feedback has-feedback-left" >
                        <input type="text" class="form-control" name="username" placeholder="Username" required>
                        <div class="form-control-feedback">
                            <i class="icon-user-check text-muted"></i>
                        </div>
                    </div>
                    <div class="form-group has-feedback has-feedback-left mt-5"">
                        <input type="password" class="form-control" name="password" placeholder="password" required>
                        <div class="form-control-feedback">
                            <i class="icon-key text-muted"></i>
                        </div>
                    </div>
                    <button type="submit" class="btn bg-teal pull-right mt-10">Login <i class="icon-circle-right2 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection