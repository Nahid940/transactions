@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-2 control-label"><strong><i class="icon-check"></i> ব্যাক্তিগত হিসাব আপডেট </strong></label>
                    <a href="{{route('admin-trx-diary')}}" class="btn btn-success pull-right">হিসাব তালিকা </a>
                    <div class="col-lg-8">
                        @if(session('trx-update'))
                            <div class="alert alert-success">
                                {{session('trx-update')}}
                            </div>
                        @endif
                        <form class="form-horizontal" method="post" action="{{route('admin-trx-diary-update',$data->id)}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">তারিখ </label>
                                            <input value="{{$data->entry_date}}" type="date" class="form-control less-height" autocomplete="off" name="date" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ধরণ</label>
                                            <select name="type" id="" class="form-control less-height" required >
                                                <option value="">হিসাবের ধরণ</option>
                                                <option value="1" {{$data->type==1?'selected':''}}>ডেবিট (দেনা)</option>
                                                <option value="2" {{$data->type==2?'selected':''}}>ক্রেডিট (পাওনা)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">রেফারেন্স</label>
                                            <input type="text" value="{{$data->reference}}" name="reference" placeholder="রেফারেন্স" class="form-control less-height">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">রিমার্ক </label>
                                            <input type="text" value="{{$data->remark}}"  name="remark" placeholder="রিমার্ক" class="form-control less-height">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">এমাউন্ট </label>
                                            <input type="text" value="{{$data->amount}}"  name="amount" placeholder="এমাউন্ট" class="form-control less-height" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection