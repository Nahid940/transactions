@extends('layouts.app')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">ব্যাক্তিগত হিসাব তালিকা<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
        @if(session('trx-delete'))
            <div class="alert alert-danger">{{session('trx-delete')}}</div>
        @endif
        <div class="table-responsive">
            <table class="table table-xs">
                <thead>
                    <tr>
                        <th>ক্রমিক</th>
                        <th>তারিখ</th>
                        <th>রেফারেন্স </th>
                        <th>রিমার্ক</th>
                        <th>ডেবিট</th>
                        <th>ক্রেডিট</th>
                        <th>অ্যাকশন</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach($lists as $list)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{date('d-m-Y',strtotime($list->entry_date))}}</td>
                            <td>{{$list->reference}}</td>
                            <td>{{$list->remark}}</td>
                            <td>{{$list->type==1?$list->amount:"-"}}</td>
                            <td>{{$list->type==2?$list->amount:"-"}}</td>
                            <td>
                                <a href="{{route('admin-trx-delete',$list->id)}}" title="Delete"><i class="icon-trash btn btn-sm text-danger-700"></i></a>
                                <a href="{{route('admin-trx-edit',$list->id)}}" title="Edit"><i class="icon-pen btn btn-sm text-info"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection