<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Account</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	{{--<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>--}}
	<script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
	{{--<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>--}}
	{{--<script type="text/javascript" src="assets/js/plugins/ui/nicescroll.min.js"></script>--}}
	{{--<script type="text/javascript" src="assets/js/plugins/ui/drilldown.js"></script>--}}
	<!-- /core JS files -->

	<script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>
	<!-- <script type="text/javascript" src="assets/js/pages/dashboard.js"></script> -->
	{{--<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>--}}
	<!-- /theme JS files -->
</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse" style="background-color: #b53f3f">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{route('home')}}">My Accounts</a>
			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav navbar-right">
				@if(\Illuminate\Support\Facades\Auth::check())
					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user text-white"></i>
							<span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
							<i class="caret"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-right">
							{{--<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>--}}
							{{--<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>--}}
							<li><a href="{{route('reset-password')}}" id=""><i class="icon-reset"></i> Reset Password</a></li>
							<li><a href="javascript:void(0)" id="logout"><i class="icon-switch2"></i> Logout</a></li>
							<form action="{{route('logout')}}" method="post" id="logout_form">
								{{csrf_field()}}
							</form>
						</ul>
					</li>
				@endif
			</ul>
		</div>
	</div>
	<!-- /main navbar -->

	<!-- Second navbar -->
	<div class="navbar navbar-default" id="navbar-second">
		<ul class="nav navbar-nav no-border visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
		</ul>
		<div class="navbar-collapse collapse" id="navbar-second-toggle">
			<ul class="nav navbar-nav navbar-nav-material">

				@if(\Illuminate\Support\Facades\Auth::check())

{{--				<li class="{{(\Request::route()->getName()=='home'?'active':'')}}"><a href="{{route('home')}}"><i class="icon-display4 position-left"></i> ড্যাশবোর্ড</a></li>--}}

				@if(\Illuminate\Support\Facades\Auth::user()->type==1)
					<li class="{{(\Request::route()->getName()=='admin'?'active':'')}} ">
						<a href="{{route('admin')}}" class=" legitRipple" aria-expanded="false">
							<i class="icon-user position-left"></i> অ্যাডমিন
						</a>
					</li>
				@endif

				<li class="{{(\Request::route()->getName()=='accounts'?'active':'')}} ">
					<a href="{{route('accounts')}}" class="">
						<i class="icon-calculator2 position-left"></i> একাউন্টস
					</a>
				</li>
				<li class="{{(\Request::route()->getName()=='newexpense'?'active':'')}} ">
					<a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
						<i class=" icon-clipboard2 position-left"></i> খরচ <span class="caret"></span>
					</a>
					<ul class="dropdown-menu width-200">
						<li class="">
							<a href="{{route('newexpense')}}" class="dropdown-toggle">নতুন খরচ যুক্ত করুন</a>
						</li>
						<li class="">
							<a href="{{route('exp-lists')}}" class="dropdown-toggle">খরচ তালিকা</a>
						</li>
					</ul>
				</li>
				@if(\Illuminate\Support\Facades\Auth::user()->type==1)
					<li class="{{(\Request::route()->getName()=='add-file'?'active':'')}} ">
						<a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-files-empty position-left"></i>ফাইল  <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-200">
							<li class="">
								<a href="{{route('add-file')}}" class="dropdown-toggle">ফাইল ম্যানেজার</a>
							</li>
						</ul>
					</li>
				@endif
				@else
					<li class="active"><a href="{{route('login')}}"><i class="icon-key position-left"></i> লগইন</a></li>
				@endif
				{{--<li class="{{(\Request::route()->getName()=='users'?'active':'')}} ">--}}
					{{--<a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">--}}
						{{--<i class="icon-users4 position-left"></i> ইউজার্স <span class="caret"></span>--}}
					{{--</a>--}}
					{{--<ul class="dropdown-menu width-200">--}}
						{{--<li class="">--}}
							{{--<a href="{{route('users')}}" class="dropdown-toggle">সকল ইউজার্স </a>--}}
						{{--</li>--}}
					{{--</ul>--}}
				{{--</li>--}}
			</ul>
		</div>
	</div>
	<!-- /second navbar -->
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
                @yield('content')
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	<div class="footer text-muted">
	</div>
	<!-- /footer -->
	@yield('script')

	<script>
		$('#logout').on('click',function () {
			$('#logout_form').submit()
        })
	</script>
</body>
</html>