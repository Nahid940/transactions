<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Account</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	{{--<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>--}}
	<script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
	{{--<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>--}}
	{{--<script type="text/javascript" src="assets/js/plugins/ui/nicescroll.min.js"></script>--}}
	{{--<script type="text/javascript" src="assets/js/plugins/ui/drilldown.js"></script>--}}
	<!-- /core JS files -->

	<script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>
	<!-- <script type="text/javascript" src="assets/js/pages/dashboard.js"></script> -->
	{{--<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>--}}
	<!-- /theme JS files -->
</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse" style="background-color: #b53f3f">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{route('home')}}">My Accounts</a>
			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav navbar-right">
				@if(\Illuminate\Support\Facades\Auth::check())
					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user text-white"></i>
							<span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
							<i class="caret"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-right">
							{{--<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>--}}
							{{--<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>--}}
							<li><a href="{{route('reset-password')}}" id=""><i class="icon-reset"></i> Reset Password</a></li>
							<li><a href="javascript:void(0)" id="logout"><i class="icon-switch2"></i> Logout</a></li>
							<form action="{{route('logout')}}" method="post" id="logout_form">
								{{csrf_field()}}
							</form>
						</ul>
					</li>
				@endif
			</ul>
		</div>
	</div>
	<!-- /main navbar -->
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content" style="min-height:151px">

			@if(\Illuminate\Support\Facades\Auth::check())

			<div class="sidebar sidebar-main sidebar-default">

				<div class="sidebar-fixed affix-top">
					<div class="sidebar-content">
					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="sidebar-user-material">
							<div class="category-content">
								<div class="sidebar-user-material-content">
									<a href="#"><i class="icon-user" style="font-size: 80px;color: #fff"></i></a>
									<h6>{{\Illuminate\Support\Facades\Auth::user()->name}}</h6>
								</div>
							</div>
						</div>

						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">
								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li>
									<a href="{{route('admin')}}" class=" legitRipple" aria-expanded="false">
										<i class="icon-user position-left"></i> অ্যাডমিন
									</a>
								</li>
								<li>
									<a href="{{route('accounts')}}" class="">
										<i class="icon-calculator2 position-left"></i> একাউন্টস
									</a>
								</li>
								<li>
									<a href="#"><i class=" icon-pie-chart position-left"></i> <span>কেপিআই (KPI)</span></a>
									<ul>
										<li><a href="{{route('kpi-add')}}" class="dropdown-toggle">টার্গেট যুক্ত করুন</a></li>
										<li><a href="{{route('target-list')}}" class="dropdown-toggle">টার্গেট তালিকা</a></li>
										<li><a href="{{route('daily-target-achievement')}}" class="dropdown-toggle">ডেইলি টার্গেট অ্যাচিভমেন্ট</a></li>
										<li><a href="{{route('daily-target-achievement-list')}}" class="dropdown-toggle">ডেইলি টার্গেট অ্যাচিভমেন্ট(তালিকা)</a></li>
										<li><a href="{{route('achievement-report')}}" class="dropdown-toggle">অ্যাচিভমেন্ট রিপোর্ট</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class=" icon-clipboard2 position-left"></i> <span>খরচ</span></a>
									<ul>
										<li><a href="{{route('newexpense')}}" class="dropdown-toggle">নতুন খরচ যুক্ত করুন</a></li>
										<li><a href="{{route('exp-lists')}}" class="dropdown-toggle">খরচ তালিকা</a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class=" icon-chart position-left"></i> <span>রিপোর্ট </span></a>
									<ul>
										<li><a href="{{route('dso-daily-transaction-report-short')}}" class="dropdown-toggle"> ডিএসও লেনদেন (শর্ট রিপোর্ট)</a></li>
										<li><a href="{{route('dso-daily-transaction-report')}}" class="dropdown-toggle"> ডিএসও লেনদেন (ডিটেইল রিপোর্ট)</a></li>
										<li><a href="{{route('salary-report')}}" class="dropdown-toggle"> স্যালারি রিপোর্ট </a></li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-files-empty position-left"></i>ফাইল </a>
									<ul>
										<li><a href="{{route('add-file')}}" class="dropdown-toggle">ফাইল ম্যানেজার</a></li>
									</ul>
								</li>
								<!-- /main -->
							</ul>
						</div>
					</div>
					<!-- /main navigation -->
				</div>
				</div>
			</div>
			@endif
			<!-- Main content -->
			<div class="content-wrapper">
                @yield('content')
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
	<!-- Footer -->
	<div class="footer text-muted">
	</div>
	<!-- /footer -->
	@yield('script')

	<script>
		$('#logout').on('click',function () {
			$('#logout_form').submit()
        })
	</script>
</body>
</html>