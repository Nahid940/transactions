@extends('layouts.app')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">{{session('success')}}</div>
            @endif
                @if(session('error'))
                <div class="alert alert-danger">{{session('error')}}</div>
            @endif
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-2 control-label "><strong><i class="icon-user-plus"></i> নতুন ডিএসও </strong></label>
                    <div class="col-lg-3">
                        <form class="form-horizontal" method="post" action="{{route('addDso')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control myplaceholder" autocomplete="off" name="name" placeholder="ডিএসও এর নাম " required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control myplaceholder" autocomplete="off" name="contact" placeholder="ফোন নম্বর">
                                    </div>
                                    <div class="form-group new-wallet">
                                        <a type="button" class='btn btn-info btn-sm add-new-wallet mt-20' style='' title='Add New Wallet No.'>+</a>
                                        <div style="display:flex;flex-direction:row">
                                            <input type="text" class="form-control myplaceholder" autocomplete="off" required  name="wallet_no[]" placeholder="Wallet No.">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-7">
                        <label class="control-label "><strong><i class="icon-list"></i>  ডিএসও তালিকা </strong></label>
                        <table class="table mytable text-center-table">
                            <thead>
                                <tr>
                                    <th>ক্রমিক </th>
                                    <th>ডিএসও নাম </th>
                                    <th>ওয়ালেট নং</th>
                                    <th>অ্যাকশন </th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @foreach($dsos as $dso)
                                <tr>
                                    <td>{{$i++}}.</td>
                                    <td>
                                        {{$dso->name}}
                                    </td>
                                    <td>
                                        <p>
                                            @foreach($dso->wallets as $wallet)
                                                {{$wallet->wallet_no}},
                                            @endforeach
                                        </p>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" title="Delete" data-id="{{$dso->id}}" class="remove-btn"><i class="icon-trash text-danger-700"></i></a>
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default" title="Edit" class="edit_dso" data-id="{{$dso->id}}"><i class="icon-pen"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-2 control-label"><strong><i class="icon-user-plus"></i> নতুন ইউজার যুক্ত করুন </strong></label>
                    <div class="col-lg-4">
                        <form class="form-horizontal" method="post" action="{{route('add-user')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control myplaceholder" autocomplete="off" name="name" placeholder=" নাম " required>
                                    </div>
                                    <div class="form-group">
                                        <select name="type" id="" class="form-control" required>
                                            <option value="">ইউজার টাইপ</option>
                                            <option value="1">Admin</option>
                                            <option value="0">Accountant</option>
                                            <option value="2">Others</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control myplaceholder" autocomplete="off" name="contact" placeholder=" ফোন " >
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control myplaceholder" autocomplete="off" name="username" placeholder=" ইউজারনেম ">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control myplaceholder" autocomplete="off" name="password" placeholder="পাসওয়ার্ড ">
                                    </div>
                                </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <span class=" text-bold">ইউজার তালিকা </span>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>নাম </th>
                                    <th>অ্যাকশন</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td><a href="#" title="Delete" class="remove_user" data-id="{{$user->id}}"><i class="icon-trash text-danger-700"></i></a></td>
                                        <form action="{{route('remove_user')}}" method="post" id="{{$user->id}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="user" value="{{$user->id}}">
                                        </form>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-2 control-label"><strong><i class="icon-check"></i> ব্যাক্তিগত হিসাব </strong></label>
                    <a href="{{route('admin-trx-diary')}}" target="_blank" class="btn btn-success pull-right">হিসাব তালিকা </a>
                    <div class="col-lg-8">
                        @if(session('trx-admin-success'))
                            <div class="alert alert-success">
                                {{session('trx-admin-success')}}
                            </div>
                        @endif
                        <form class="form-horizontal" method="post" action="{{route('admin-trx')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">তারিখ </label>
                                            <input type="date" class="form-control less-height" autocomplete="off" name="date" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">ধরণ</label>
                                            <select name="type" id="" class="form-control less-height" required >
                                                <option value="">হিসাবের ধরণ</option>
                                                <option value="1">ডেবিট (দেনা)</option>
                                                <option value="2">ক্রেডিট (পাওনা)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">রেফারেন্স</label>
                                            <input type="text" name="reference" placeholder="রেফারেন্স" class="form-control less-height">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">রিমার্ক </label>
                                            <input type="text" name="remark" placeholder="রিমার্ক" class="form-control less-height">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">এমাউন্ট </label>
                                            <input type="text" name="amount" placeholder="এমাউন্ট" class="form-control less-height" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-6">
                <div class="row">
                    <label class="col-lg-3 control-label"><strong><i class="icon-reset"></i> ডাটাবেজ রিসেট করুন  </strong></label>
                    <div class="col-lg-4">
                        <form class="form-horizontal" id="reset_form" method="post" action="{{route('resetDB')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control myplaceholder" placeholder="পাসওয়ার্ড " required/>
                                    </div>
                                    <input type="button" value="রিসেট " class="btn btn-danger btn-sm verify mt-10 pull-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="modal_default" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route('update-dso')}}" method="post" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Edit DSO Info</h5>
                    </div>
                    <div class="modal-body mb-10">
                        {{csrf_field()}}
                        <div class="form-element">

                        </div>

                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        var i=0;
        $('.add-new-wallet').on('click',function(){
            i++;
            $('.new-wallet').append('<div style="display:flex;flex-direction:row" id="remove'+i+'"><input type="text" class="form-control" autocomplete="off" required  name="wallet_no[]" placeholder="Wallet No."><span class=" remove btn btn-danger btn-sm mr-10" data-id="'+i+'" title="Remove" style="margin: 14px">-</span></div>');
        })

        $(document).on('click','.remove',function()
        {
            var sl=$(this).attr('data-id')
            $('#remove'+sl).remove()
        })


        $('.remove-btn').on('click',function () {

            var r = confirm("Do you want to remove this DSO? ");
            if (r == true) {
                $.ajax({
                    type:'post',
                    url:'{{route('delete-dso')}}',
                    data:{
                        id:$(this).attr('data-id')
                    },
                    success:function () {
                        window.location.reload()
                    }
                })
            } else {
                txt = "You pressed Cancel!";
            }
        })

        $('.verify').on('click',function () {

            var r = confirm("Do you want to reset database? ");
            if (r == true) {
                $('#reset_form').submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })

        $('.remove_user').on('click',function () {

            var r = confirm("Do you want to delete this user?? ");
            if (r == true) {
                $('#'+$(this).attr('data-id')).submit()
            } else {
                txt = "You pressed Cancel!";
            }
        })

        $('.edit_dso').on('click',function () {
            var id=$(this).attr('data-id')
            $.ajax({
                type:'post',
                url:'{{route('get-dso')}}',
                data:{
                    'id':id
                },
                success:function (data) {
                    //console.log(data)
                    $('.form-element').html(data)
                }
            })
        })


    </script>
@endsection