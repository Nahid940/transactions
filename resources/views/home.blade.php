@extends('layouts.app')

@section('content')
    <!-- Dashboard content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Quick stats boxes -->
            <div class="row">
                <div class="col-lg-3">

                    <!-- Members online -->
                    <div class="panel bg-blue">
                        <div class="panel-body">
                            <div class="heading-elements">
                                {{--<span class="heading-text badge bg-teal-800">+53,6%</span>--}}
                            </div>

                            <h3 class="no-margin">654356</h3>
                            Total Customers
                        </div>

                        <div class="container-fluid">
                            <div id="members-online"></div>
                        </div>
                    </div>
                    <!-- /members online -->

                </div>

                <div class="col-lg-3">
                    <!-- Current server load -->
                    <div class="panel bg-blue-400">
                        <div class="panel-body">

                            <h3 class="no-margin">764567</h3>
                            Current Month Total Payment
                            <div class="text-muted text-size-small"></div>
                        </div>

                        <div id="server-load"></div>
                    </div>
                    <!-- /current server load -->
                </div>

                <div class="col-lg-3">

                    <!-- Today's revenue -->
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="no-margin">63465</h3>
                            Total Transaction Amount
                            <div class="text-muted text-size-small"></div>
                        </div>

                        <div id="today-revenue"></div>
                    </div>
                    <!-- /today's revenue -->

                </div>

                <div class="col-lg-3">

                    <!-- Today's revenue -->
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="no-margin">674567</h3>
                            Total Active Calls
                            <div class="text-muted text-size-small"></div>
                        </div>

                        <div id="today-revenue"></div>
                    </div>
                    <!-- /today's revenue -->

                </div>
                <div class="col-lg-3">

                    <!-- Today's revenue -->
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="no-margin">7657456</h3>
                            Total Trunk
                            <div class="text-muted text-size-small"></div>
                        </div>

                        <div id="today-revenue"></div>
                    </div>
                    <!-- /today's revenue -->

                </div>
                <div class="col-lg-3">
                    <!-- Today's revenue -->
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="no-margin">542345</h3>
                            Total Provider
                            <div class="text-muted text-size-small"></div>
                        </div>

                        <div id="today-revenue"></div>
                    </div>
                    <!-- /today's revenue -->

                </div>

                <div class="col-lg-3">
                    <!-- Today's revenue -->
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="no-margin">67436</h3>
                            Total Revenue
                            <div class="text-muted text-size-small"></div>
                        </div>

                        <div id="today-revenue"></div>
                    </div>
                    <!-- /today's revenue -->

                </div>

                <div class="col-lg-3">
                    <!-- Today's revenue -->
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="no-margin">35634</h3>
                            Todays Failed Calls
                            <div class="text-muted text-size-small"></div>
                        </div>

                        <div id="today-revenue"></div>
                    </div>
                    <!-- /today's revenue -->

                </div>

            </div>

            <!-- /quick stats boxes -->
        </div>
    </div>
    <!-- /dashboard content -->
@endsection
