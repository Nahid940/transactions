@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
        @endif
        <div class="panel-heading">
            <h5 class="panel-title">ডিএসও ট্রান্সাকশন রিপোর্ট (ডেইলি)<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
        <div class="panel-body">
            <div class="col-md-8">
                <form action="{{route('dso-daily-transaction-report-view')}}" target="_blank" method="get">
                    <div class="table-responsive">
                        <div class="">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label for="" class="pull-left">ডিএসও</label>
                                    <select name="dso" id="" class="form-control" required>
                                        @foreach($dsos as $dso)
                                            <option value="{{$dso->id}}">{{$dso->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="" class="pull-left">তারিখ (থেকে)</label>
                                    <input type="date" value="{{date('Y-m-d')}}" name="start_date" class="form-control">
                                </div>
                                <div class="col-md-4 hidden">
                                    <label for=""  class="pull-left">তারিখ (পর্যন্ত)</label>
                                    <input type="date" value="{{date('Y-m-d')}}"  class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-primary btn-sm legitRipple mt-10 pull-right "> <i class="icon-check"></i>সাবমিট</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

    </script>
@endsection