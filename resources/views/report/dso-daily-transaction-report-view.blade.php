<!doctype html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <title>Daily Transaction</title>
    <style type='text/css'>

        @font-face {
            font-family: SourceSansPro;
        @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro')
        }
        .clearfix:after {
            content: '';
            display: table;
            clear: both;
        }

        a {
            color: #000;
            text-decoration: none;
        }


        #logo {
            float: left;
            margin-left:50px;
            margin-top: 8px;
        }

        #logo img {
            height: 70px;
        }

        #company {
            float: right;
            margin-right: 50px;
            margin-top: 10px;
        }
        #company1 {
            float: right;

            margin-top: 10px;
        }

        #details {
            margin-bottom: 10px;
        }

        table tbody tr:last-child {
            /*border-bottom: 1px solid #000;*/
        }

        #client {
            padding-left: 6px;
            float: left;
        }

        #client .to {
            color: #fff;
        }

        h2.name {
            font-size: 15px;
            font-weight: normal;
            margin: 0;
        }

        .info{
            font-size: 14px;
            margin-bottom: 3px
        }

        #invoice {
            float: right;
            /* margin-right:15px*/
        }

        #invoice h1 {
            color: #000;
            font-size:2em;
            line-height: 1em;
            font-weight: normal;
            margin: 0 0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #000;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table th,
        table td {
            padding: 5px 3px;
            background: #fff;
            text-align: center;
            /*border: 1px solid #000;*/
            border-left: 1px solid #000;
            border-right: 1px solid #000;
        }

        table th {
            /* white-space: nowrap;*/
            font-weight: bold;
            font-size:14px
        }

        table td {
            text-align: center;
            border: 1px solid #000;
        }
        table tr td:first-child {

        }

        table td h3 {
            color: #000;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
        }

        table .no {
            color: #000;
            font-size: 13px;
            background: #fff;
        }

        table .desc {
            text-align: left;
        }

        table .unit {
            background: #fff;
        }

        table .qty {}

        table .total {
            background: #fff;
            color: #000;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table tbody tr:last-child td {
            border-bottom: 1px solid #000;
        }

        table tfoot td {
            padding: 2px 4px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 4px;
            border:none;
            /*border-bottom: 1px solid #000;*/
        }

        table tfoot tr:first-child td {
            /*border-top: 1px solid #000;*/
        }

        table tfoot tr:last-child td {
            color: #fff;
            font-size: 1.4em;
            /*border-top: 1px solid #000;*/
        }

        table tfoot tr td {

        }

        #thanks {
            font-size: 1.5em;
            margin-bottom: 50px;
            margin-top: -110px;
        }

        #notices {
            margin-top: 10px;
        }

        #notice1 {
            width:300px;
            padding-left: 3px;
            border: 1px solid #000;
            margin-top: -123px;
            font-size: 10px;
            padding-bottom:3px
        }

        #notices .notice {
            font-size: 14px;
        }

        footer {
            color: #000;
            width: 80%;
            height: 100px;
            position: absolute;
            bottom: 0;
            /*border-top: 1px solid #fff;*/
            padding: 8px 0;
            text-align: center;
        }

        .seal_signiture {
            margin-top: 60px;
        }

        .signiture {
            width: 50%;
            float: left;
            color: #000;
            font-size: 14px;
            font-weight: 600;
        }

        table thead tr {
            border-top: 1px solid #000;
        }

        table thead tr th {
            padding: 5px 2px;
            border-top:1px solid #000;
            border-bottom:1px solid #000;
            white-space: nowrap
        }

        .moreinfo {
            font-weight: 600;
            width: 70px;
        }

        h3.invoice_title {
            background: #9dbbff;
            padding: 2px 10px;
            margin-bottom: 10px;
            font-size: 18px;
            text-transform: uppercase;
            width: 350px;
            text-align: center;
            float: none;
            margin: 0 auto;
            margin-bottom: 10px;
            margin-top: 10px;
        }
        .search_info_box{
            font-size:14px
        }

        #printbtn {
            float: none;
            position: absolute;
            right: -195px;
            padding: 13px 12px;
            font-size: 16px;
            background: #b1acac;
            border: 1px solid #1918188f;
            color: #000;
            font-weight: 600;
            border-radius: 2px;
        }

        header {
            left: -50px;
            right: -50px;
            padding: 0 0 0 0

        }
        @page {
            margin: 0px;
        }


        body {
            position: relative;
            width: 18cm;
            height: 25cm;
            margin: 0 auto;
            color: #000;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
            font-family: SourceSansPro;
        }

        .text-right
        {
            text-align: right;
        }
        .text-left
        {
            text-align: left;
        }


    </style>

    <style media='print'>
        #printbtn {
            display:none;
        }
    </style>

</head>
<body>
<header class='clearfix'style='margin-bottom : 10px;'>
    {{--<div id='logo'>--}}
        {{--<img src='' style='height:50px'>--}}
    {{--</div>--}}
    <div id='company'>
        <h2 class='name'></h2>
        <div></div>
        <div></div>
        <div><a href='mailto:company@example.com'>"</a></div>
    </div>
</header>

<main>
    <div id='details' class='clearfix'>
        <h3 class='invoice_title'>DSO Daily Transaction (Summery)</h3>
        <button id='printbtn' onclick='myFunction()'>Print</button>
        <script>
            function myFunction() {

                window.print();
            }
        </script>
        <div class=''>
            <h6>
               <span class='date-text search_info_box'>
                   Date: <span class='badge badge-outline-info'>{{date('d-m-Y',strtotime($_REQUEST['start_date'])) }}</span>
               </span>
            </h6>
        </div>
        <table border='0' cellspacing='0' cellpadding='0'>

                @php

                @endphp

                @foreach($lists as $lst)

                    @php
                        $opening_cash=0;
                        $opening_b2b=0;
                        $opening_advance_return=0;

                        $closing_cash=0;
                        $closing_b2b=0;
                        $closing_bank=0;
                        $closing_advance_collection=0;

                        $opening_due_collection=0;
                    @endphp

                   @foreach($lst['transactions'] as $amount)
                        @php
                            if($amount->transaction_type==2){
                                $opening_cash+=$amount->total_amount;
                            }

                            if($amount->transaction_type==3){
                                $opening_b2b+=$amount->total_amount;
                            }

                            if($amount->transaction_type==5){
                                $closing_cash+=$amount->total_amount;
                            }

                            if($amount->transaction_type==6){
                                $closing_b2b+=$amount->total_amount;
                            }

                            if($amount->transaction_type==7){
                                $closing_bank+=$amount->total_amount;
                            }

                            if($amount->transaction_type==4){
                                $opening_due_collection+=$amount->total_amount;
                            }

                            if($amount->transaction_type==20
                            || $amount->transaction_type==21
                            || $amount->transaction_type==23){
                            $closing_advance_collection+=$amount->total_amount;
                            }

                            if($amount->transaction_type==37){
                                $opening_advance_return+=$amount->total_amount;
                            }

                        @endphp

                   @endforeach
                <tbody>
                    <tr>
                        <td colspan="4" style="text-align: left;font-weight: bold">DSO Name: {{$lst['dso_name']}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight: bold;width: 50%">Opening Transaction</td>
                        <td colspan="2" style="font-weight: bold;width: 50%">Closing Transaction</td>
                    </tr>
                    <tr>
                        <td class="text-right">Cash</td>
                        <td>{{$opening_cash}}</td>
                        <td class="text-right">Cash</td>
                        <td>{{$closing_cash}}</td>
                    </tr>
                    <tr>
                        <td class="text-right">B2B</td>
                        <td>{{$opening_b2b}}</td>
                        <td class="text-right">B2B</td>
                        <td>{{$closing_b2b}}</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-right" style="text-align: right">Bank</td>
                        <td>{{$closing_bank}}</td>
                    </tr>
                    <tr>
                        <td class="text-right"  style="font-weight: bold">Total</td>
                        <td  style="">{{$opening_cash+$opening_b2b}}</td>
                        <td class="text-right"  style="font-weight: bold">Total</td>
                        <td  style="">{{$closing_cash+$closing_b2b+$closing_bank}}</td>
                    </tr>
                    <tr>
                        <td class="text-right">Advance Return</td>
                        <td>{{$opening_advance_return}}</td>
                        <td class="text-right">Advance Collection</td>
                        <td>{{$closing_advance_collection}}</td>
                    </tr>
                    <tr>
                        <td class="text-right">Due Collection</td>
                        <td>{{$opening_due_collection}}</td>
                        <td class="text-right">Due (Today)</td>
                        <td>{{($opening_cash+$opening_b2b+$opening_due_collection)-($closing_cash+$closing_b2b+$closing_bank)}}</td>
                    </tr>
                    <tr>
                        <td class="text-right" colspan="3">Due (MTD)</td>
                        <td>{{$lst['mtd_due']}}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color: #e1e0ff">&nbsp;</td>
                    </tr>
                </tbody>
            @endforeach
        </table>

</main>
</body>
</html>