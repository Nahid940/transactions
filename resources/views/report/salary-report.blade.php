@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
        @endif
        <div class="panel-heading">
            <h5 class="panel-title">স্যালারি রিপোর্ট<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
        <div class="panel-body">
            <div class="col-md-8">
                <form action="{{route('salary-report-view')}}" target="_blank" method="get">
                    <div class="table-responsive">
                        <div class="">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label for="" class="pull-left">ডিএসও</label>
                                    <select name="u" id="" class="form-control">
                                        <option value="">All</option>
                                        @foreach($users as $dso)
                                            <option value="{{$dso->id}}">{{$dso->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="" class="pull-left">মাস</label>
                                    <select name="m" id="" class="form-control">
                                        <option value="1" {{date('m')==1?'selected':''}}>জানুয়ারী</option>
                                        <option value="2" {{date('m')==2?'selected':''}}>ফেব্রুয়ারী</option>
                                        <option value="3" {{date('m')==3?'selected':''}}>মার্চ</option>
                                        <option value="4" {{date('m')==4?'selected':''}}>এপ্রিল</option>
                                        <option value="5" {{date('m')==5?'selected':''}}>মে</option>
                                        <option value="6" {{date('m')==6?'selected':''}}>জুন</option>
                                        <option value="7" {{date('m')==7?'selected':''}}>জুলাই</option>
                                        <option value="8" {{date('m')==8?'selected':''}}>আগস্ট</option>
                                        <option value="9" {{date('m')==9?'selected':''}}>সেপ্টেম্বর</option>
                                        <option value="10" {{date('m')==10?'selected':''}}>অক্টোবর</option>
                                        <option value="11" {{date('m')==11?'selected':''}}>নভেম্বর</option>
                                        <option value="12" {{date('m')==12?'selected':''}}>ডিসেম্বর</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="" class="pull-left">বছর</label>
                                    <select name="y" id="" class="form-control">
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-sm legitRipple mt-10 pull-right "> <i class="icon-check"></i>সাবমিট</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

    </script>
@endsection