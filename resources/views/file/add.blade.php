@extends('layouts.app')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                @if(session('success'))
                    <span class="text-success text-bold">{{session('success')}}</span>
                @endif
                <h4>নতুন ফাইল যুক্ত করুন </h4>
                <form action="{{route('add-file')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="col-md-6">
                        <div class="form-group form-group-material">
                            <input type="text" name="title" class="form-control" placeholder="ফাইল নাম " required >
                        </div>
                        <div class="form-group form-group-material">
                            <label class="display-block control-label has-margin animate">Styled file input</label>
                            <div class="uploader hover">
                                <input required multiple type="file" name="files[]" class="file-styled">
                                <span class="filename" style="user-select: none;">No file selected</span>
                                <span class="action btn bg-pink-400 legitRipple" style="user-select: none;">Choose File</span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info pull-right btn-sm"><i class="icon-upload"></i> Upload</button>
                    </div>
                </form>

                <div class="col-md-6">
                    <h4>আর্কাইভ</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ক্রমিক</th>
                                <th>ফাইল নাম</th>
                                <th>অ্যাকশন</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                        $i=1;
                        @endphp
                            @foreach($files as $file)
                                @php
                                $url=\Illuminate\Support\Facades\Storage::url($file->path);
                                @endphp
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$file->title}}</td>
                                    <td>
                                        <a title="Download {{$file->title}}" href="{{ $url}}" download><i class="icon-download"></i></a>
                                        <a title="Remove" href="javascript:void(0)" class="remove_file" data-file_name="{{$file->path}}" data-id="{{$file->id}}"><i class="icon-trash text-danger-700"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('.remove_file').on('click',function () {

            var v=$(this).attr('data-file_name')

            var r = confirm("Do you want to remove this file? ");
            if (r == true) {
                $.ajax({
                    type:'post',
                    url:'{{route('delete-file')}}',
                    data:{
                        id:$(this).attr('data-id'),
                        file_name:$(this).attr('data-file_name')
                    },
                    success:function () {
                        setTimeout(function () {
                            alert("File removed successfully !!");
                            window.location.reload()
                        },300)
                    }
                })
            } else {
                txt = "You pressed Cancel!";
            }
        })
    </script>
@endsection