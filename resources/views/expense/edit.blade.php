@extends('layouts.app')
@section('content')

    <div class="panel panel-flat">
        @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
        @endif
        <div class="panel-heading">
            <h5 class="panel-title">Add New Expense<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
        <div class="panel-body">
            <div class="col-md-8">
                <div class="table-responsive">
                    <form action="{{route('expense-enlist')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="type" value="0">
                        <input type="hidden" name="post_from" value="expense">
                        {{--<a href="#" class="btn btn-info btn-sm pull-right add-new" title="Add New"><i class="icon-plus2"></i></a>--}}
                        <table class="table">

                            <tbody class="table-rows">
                            <tr>
                                <td colspan="2"><input type="text" class="form-control myplaceholder" name="title" placeholder=" টাইটেল "></td>
                            </tr>
                            <tr>
                                <td><input type="text" placeholder="খরচ  মেমো " name="memo" class="form-control myplaceholder"></td>
                                <td><input type="number" name="unit" placeholder="ইউনিট" class="form-control myplaceholder"></td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="expense_type[]" id="" class="form-control" required>
                                        <option value="">খরচ হেড</option>
                                        <option value="10000">Fixed Asset</option>
                                        @foreach($expense_types as $expense_type)
                                            <option value="{{$expense_type->id}}">{{$expense_type->type_name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input type="number" placeholder="Amount" name="amount[]" class="form-control myplaceholder"></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="col-md-12">
                            <div class="form-group">
                                <select name="reference_person" id="" class="form-control" required>
                                    <option value="">ব্যাক্তি তালিকা নির্বাচন</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--<div class="pull-right text-orange-800 text-bold" style="font-size: 18px">Total: 10000/-</div>--}}
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20"> <i class="icon-check"></i>আপডেট করুন  </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
{{--<span class=" remove btn btn-danger btn-sm mr-10" data-id="'+i+'" title="Remove" style="margin: 14px">-</span>--}}

@section('script')
    <script>

    </script>
@endsection