@extends('layouts.app')

@section('content')

    <div class="col-md-7">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">খরচ তালিকা <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            </div>
            @if(session('success'))
                <div class="alert alert-success">{{session('success')}}</div>
            @endif

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ব্যাক্তির নাম</th>
                        <th>তারিখ</th>
                        <th>খরচ হেড</th>
                        <th>এমাউন্ট</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($expenses as $expense)
                        <tr>
                            <td>1</td>
                            <td>{{$expense->name}}</td>
                            <td>{{date('d-m-Y',strtotime($expense->entry_date))}}</td>
                            <td>{{$expense->type_name}}</td>
                            <td>{{$expense->total_amount}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection

@section('script')
    <script>

    </script>
@endsection