@extends('layouts.app')
@section('content')
    <style>
        .table tr td{
            border: none !important;
            padding: 2px !important;
        }
    </style>
    <div class="panel panel-flat">
        @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
        @endif
        <div class="panel-heading">
            <h5 class="panel-title">Add New Expense<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
            <div class="panel-body">
                <div class="col-md-8">
                    <div class="table-responsive">
                        <form action="{{route('expense-enlist')}}" method="post">
                            {{csrf_field()}}
                                <input type="hidden" name="type" value="0">
                                <input type="hidden" name="post_from" value="expense">
                            {{--<a href="#" class="btn btn-info btn-sm pull-right add-new" title="Add New"><i class="icon-plus2"></i></a>--}}
                            <table class="table">

                                <tbody class="table-rows">
                                    <tr>
                                        <td>
                                            <select name="reference_person" id="" class="form-control" required>
                                                <option value="">ব্যাক্তি তালিকা নির্বাচন</option>
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input type="text" placeholder="খরচ  মেমো " name="memo" class="form-control myplaceholder"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="expense_type[]" id="" class="form-control expense-head" required>
                                                <option value="">খরচ হেড</option>
                                                <option value="10000">Fixed Asset</option>
                                                <option value="-1">Add New Head</option>
                                                @foreach($expense_types as $expense_type)
                                                    <option value="{{$expense_type->id}}">{{$expense_type->type_name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input type="number" placeholder="Amount" name="amount[]" class="form-control myplaceholder"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20"> <i class="icon-check"></i>সেভ </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>

    <div class="panel panel-flat add-new-expense-form hidden">
        @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
        @endif
        <div class="panel-heading">
            <h5 class="panel-title">Add New Expense<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <h4>নতুন খরচ হেড যুক্ত করুন</h4>
                        <form class="form-horizontal" method="post" action="{{route('addNewType')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control myplaceholder" autocomplete="off" name="type_name" placeholder="খরচ হেড" required>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple  mt-20">সেভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6 ">
                        <table class="table">
                            <h4>খরচ হেড তালিকা</h4>
                            @if(session('type-success'))
                                <div class="alert alert-success">{{session('type-success')}}</div>
                            @endif
                            @if(session('type-success-remove'))
                                <div class="alert alert-danger">{{session('type-success-remove')}}</div>
                            @endif
                            <thead>
                                <tr>
                                    <th>ক্রমিক </th>
                                    <th>খরচ হেড</th>
                                    <th>অ্যাকশন</th>
                                </tr>
                            </thead>

                            <tbody>
                            @php
                                $i=1;
                            @endphp
                            @foreach($expense_types as $expense_type)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$expense_type->type_name}}</td>
                                    <td>
                                        <a href="javascript:void(0)" class="remove_type" title="Delete" data-id="{{$expense_type->id}}"><i class="icon-trash"></i></a>
                                        <a href="javascript:void(0)" title="Edit"><i class="icon-pen"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>


@endsection
{{--<span class=" remove btn btn-danger btn-sm mr-10" data-id="'+i+'" title="Remove" style="margin: 14px">-</span>--}}

@section('script')
    <script>

        var i=0;
        $('.add-new').on('click',function(){
            i++;
            $('.table-rows').append('' +
                '<tr id="remove'+i+'">' +
                    '<td>' +
                        '<select id="" class="form-control" name="expense_type[]" required>'+
                            '<option value="">Select Expense Type</option>'+
                            '<option value="1">Bill</option>'+
                            '<option value="2">House Rent</option>'+
                            '<option value="3">Utility Bill</option>'+
                        '</select>'+
                    '</td>'+'<td><input type="text" placeholder="Amount" name="amount[]" class="form-control"></td>'+'<td><span class=" remove btn btn-danger btn-sm mr-10" data-id="'+i+'" title="Remove" style="margin: 14px">-</span></td>'+
                '</tr>');
        })

        $(document).on('click','.remove',function()
        {
            var sl=$(this).attr('data-id')
            $('#remove'+sl).remove()
        })

        $('.remove_type').on('click',function () {

            var r = confirm("Do you want to remove this expense head? ");
            if (r == true) {
                $.ajax({
                    type:'post',
                    url:'{{route('removeType')}}',
                    data:{
                        id:$(this).attr('data-id')
                    },
                    success:function () {

                        setTimeout(function () {
                            alert("Data removed successfully !!");
                            window.location.reload()
                        },300)
                    }
                })
            } else {
                txt = "You pressed Cancel!";
            }
        })

        $('.expense-head').on('change',function () {

            if($(this).val()==-1)
            {
                $('.add-new-expense-form').removeClass('hidden')
            }else
            {
                $('.add-new-expense-form').addClass('hidden')
            }

        })

    </script>
@endsection