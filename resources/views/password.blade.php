@extends('layouts.app')

@section('content')

    <div class="panel panel-flat">
        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success">{{session('success')}}</div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger">{{session('error')}}</div>
            @endif
            <div class="col-md-12">
                <div class="row">
                    <label class="col-lg-2 control-label text-success"><strong><i class="icon-reset"></i>
                            রিসেট পাসওয়ার্ড  </strong></label>
                    <div class="col-lg-3">
                        <form class="form-horizontal" method="post" action="{{route('reset-password')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" autocomplete="off" name="password" placeholder="নতুন পাসওয়ার্ড " required>
                                    </div>
                                </div>
                                <div class="text-right mt-10">
                                    <button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">সেইভ <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection
