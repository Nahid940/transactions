@extends('layouts.app')

@section('content')
<div class="panel panel-flat">
	<div class="panel-body">

		@if(session('success'))
			<div class="alert alert-success">{{session('success')}}</div>
		@endif

		<div class="row">
			<label class="col-lg-2 control-label text-success"><strong><i class="icon-user-plus"></i> নতুন ডিএসও যুক্ত করুন </strong></label>
			<div class="col-lg-4">
				<form class="form-horizontal" method="post" action="{{route('addDso')}}">
					{{csrf_field()}}
					<div class="form-group">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" autocomplete="off" name="name" placeholder="ডিএসও এর নাম " required>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" autocomplete="off" name="contact" placeholder="ফোন নম্বর">
								</div>
								<div class="form-group new-wallet">
									<a type="button" class='btn btn-info btn-sm add-new-wallet mt-20' style='' title='Add New Wallet No.'>+</a>
									<div style="display:flex;flex-direction:row">
										<input type="text" class="form-control" autocomplete="off" required  name="wallet_no[]" placeholder="Wallet No.">
									</div>
								</div>
							</div>

						<div class="text-right mt-10">
							<button type="submit" class="btn btn-primary btn-sm legitRipple mt-20">Save <i class="icon-arrow-right14 position-right"></i></button>
						</div>
					</div>

				</form>
			</div>

			<div class="col-lg-6">

				<label class="control-label text-success"><strong><i class="icon-list"></i>  ডিএসও তালিকা </strong></label>

				<table class="table">
					<thead>
						<tr>
							<th>ক্রমিক </th>
							<th>ডিএসও নাম </th>
							<th>ওয়ালেট নং</th>
							<th>অ্যাকশন </th>
						</tr>
					</thead>
					<tbody>
					@php
						$i=1;
					@endphp
					@foreach($dsos as $dso)
						<tr>
							<td>{{$i++}}.</td>
							<td>
								{{$dso->name}}
							</td>
							<td>
								<p>
								@foreach($dso->wallets as $wallet)
									{{$wallet->wallet_no}},
								@endforeach
								</p>
							</td>
							<td>
								<a href="javascript:void(0)" title="Delete" data-id="{{$dso->id}}" class="remove-btn"><i class="icon-trash text-danger-700"></i></a>
								<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default" title="Edit" class="edit_dso" data-id="{{$dso->id}}"><i class="icon-pen"></i></a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>


<div id="modal_default" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{route('update-dso')}}" method="post" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Edit DSO Info</h5>
				</div>
				<div class="modal-body mb-10">
						{{csrf_field()}}
						<div class="form-element">

						</div>

				</div>
				<div class="modal-footer ">
					<button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary btn-sm mt-10">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('script')

<script>
	var i=0;
	$('.add-new-wallet').on('click',function(){
		i++;
		$('.new-wallet').append('<div style="display:flex;flex-direction:row" id="remove'+i+'"><input type="text" class="form-control" autocomplete="off" required  name="wallet_no[]" placeholder="Wallet No."><span class=" remove btn btn-danger btn-sm mr-10" data-id="'+i+'" title="Remove" style="margin: 14px">-</span></div>');
	})

	$(document).on('click','.remove',function()
	{
		var sl=$(this).attr('data-id')
		$('#remove'+sl).remove()
	})


	$('.remove-btn').on('click',function () {

        var r = confirm("Do you want to remove this DSO? ");
        if (r == true) {
            $.ajax({
                type:'post',
                url:'{{route('delete-dso')}}',
                data:{
                    id:$(this).attr('data-id')
                },
                success:function () {
                    //window.location.reload()
                }
            })
        } else {
            txt = "You pressed Cancel!";
        }
    })

	$('.edit_dso').on('click',function () {
		var id=$(this).attr('data-id')
		$.ajax({
		    type:'post',
			url:'{{route('get-dso')}}',
			data:{
			    'id':id
			},
			success:function (data) {
			    //console.log(data)
				$('.form-element').html(data)
            }
		})
    })





	
</script>
@endsection