@extends('layouts.app')

@section('content')

<div class="panel panel-flat ">
					
    <div class="panel-body">
       <h4>DSO List</h4>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr class="bg-indigo-600">
                    <th>#</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Eugene</td>
                    <td>Kopyov</td>
                    <td>
                        <a href="" class='btn btn-info btn-sm' title="Edit"><i class='icon-database-edit2'></i></a>
                        <a href="" class='btn btn-danger btn-sm' title="Delete"><i class="icon-trash-alt"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div id="modal_default" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h5 class="modal-title">Basic modal</h5>
            </div>

            <div class="modal-body">
                <h6 class="text-semibold">Text in a modal</h6>
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

                <hr>

                <h6 class="text-semibold">Another paragraph</h6>
                <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close<span class="legitRipple-ripple" style="left: 67.53%; top: 71.0526%; transform: translate3d(-50%, -50%, 0px); width: 225.475%; opacity: 0;"></span><span class="legitRipple-ripple" style="left: 48.3519%; top: 42.1053%; transform: translate3d(-50%, -50%, 0px); width: 225.475%; opacity: 0;"></span></button>
                <button type="button" class="btn btn-primary legitRipple">Save changes</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script>

</script>
@endsection